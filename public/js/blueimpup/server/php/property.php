<?php
error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');

class CustomUploadHandler extends UploadHandler {
	var $mysql = array('delete_type' => 'POST', 'db_name' => 'sql507402_2', 'db_port' => 3306,'db_table' => 'cvi_files');
	

	public function __construct($opts){
		parent::__construct($opts);
		
		
	}

    protected function initialize() {
		if(stripos('cvi.loc', $_SERVER['HTTP_HOST']) !== FALSE)
			$this->db = new mysqli('localhost', 'root', 'qweqwe', 'Sql507402_2');
		else
			$this->db = new mysqli('62.149.150.142', 'Sql507402', 'g2o4ys7t8q', 'Sql507402_2', 3306);
			
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
    	//$file->title = @$_REQUEST['title'][$index];
    	$file->description = @$_REQUEST['description'][$index];
    	$file->rel_id = @$_REQUEST['rel_id'];
    	$file->rel_type = @$_REQUEST['rel_type'];
    }
	
	/*protected function trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range) {
        $name = 'property_'.date('Y-m-d').'T'.date('H:i:s').'-'.uniqid();
        return parent::trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range);
    }*/

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null){
		$file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);
		$file->title = $name;
        if (empty($file->error)) {
			$sql = 'INSERT INTO `'.$this->mysql['db_table']
				.'` (`rel_id`, `rel_type`, `name`, `size`, `type`, `title`, `description`)'
				.' VALUES (?, ?, ?, ?, ?, ?, ?)';
	        $query = $this->db->prepare($sql);
	        $query->bind_param(
	        	'ississs',
	        	$file->rel_id,
	        	$file->rel_type,
	        	$file->name,
	        	$file->size,
	        	$file->type,
	        	$file->title,
	        	$file->description
	        );
	        $query->execute();
	        $file->id = $this->db->insert_id;
        }
		
		// set_additional_file_properties
		$file->deleteUrl = $this->options['script_url'].'property.php'.$this->get_query_separator($this->options['script_url']).$this->get_singular_param_name().'='.rawurlencode($file->name).'&_method=DELETE';
		
        return $file;
    }
/*
    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        	$sql = 'SELECT `id`, `type`, `title`, `description` FROM `'
        		.$this->mysql['db_table'].'` WHERE `name`=?';
        	$query = $this->db->prepare($sql);
 	        $query->bind_param('s', $file->name);
	        $query->execute();
	        $query->bind_result(
	        	$id,
	        	$type,
	        	$title,
	        	$description
	        );
	        while ($query->fetch()) {
	        	$file->id = $id;
        		$file->type = $type;
        		$file->title = $title;
        		$file->description = $description;
    		}
        }
    }
*/
    public function delete($print_response = true) {
        $response = parent::delete(false);
        foreach ($response as $name => $deleted) {
        	if($deleted) {
	        	$sql = 'DELETE FROM `'
	        		.$this->mysql['db_table'].'` WHERE `name`=?';
	        	$query = $this->db->prepare($sql);
	 	        $query->bind_param('s', $name);
		        $query->execute();
        	}
        } 
        return $this->generate_response($response, $print_response);
    }

}

$upload_handler = new CustomUploadHandler(array(
					'upload_dir' => dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/files/property/',
					'upload_url' => 'http://'.$_SERVER['SERVER_NAME'].'/app/public/files/property/',
					'access_control_allow_origin' => '',
					// Defines which files can be displayed inline when downloaded:
					'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
					// Defines which files (based on their names) are accepted for upload:
					'accept_file_types' => '/.+$/i',
					// The php.ini settings upload_max_filesize and post_max_size
					// take precedence over the following max_file_size setting:
					'max_file_size' => 3 * 1024 * 1024, // 3 MiB
					'min_file_size' => 1,
					// The maximum number of files for the upload directory:
					'max_number_of_files' => null,
					// Defines which files are handled as image files:
					'image_file_types' => '/\.(gif|jpe?g|png)$/i',
					
					'image_versions' => array(
						// The empty image version key defines options for the original image:
						'' => array(
							// Automatically rotate images based on EXIF meta data:
							'auto_orient' => true
						),
						// Uncomment the following to create medium sized images:
						/*
						'medium' => array(
							'max_width' => 800,
							'max_height' => 600
						),
						*/
						'thumbnail' => array(
							// Uncomment the following to use a defined directory for the thumbnails
							// instead of a subdirectory based on the version identifier.
							// Make sure that this directory doesn't allow execution of files if you
							// don't pose any restrictions on the type of uploaded files, e.g. by
							// copying the .htaccess file from the files directory for Apache:
							//'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
							//'upload_url' => $this->get_full_url().'/thumb/',
							
							// Uncomment the following to force the max
							// dimensions and e.g. create square thumbnails:
							//'crop' => true,
							'max_width' => 150,
							'max_height' => 150
						)
					)
				));

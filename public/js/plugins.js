// old // google.maps.event.addDomListener(window, "load", init_map); // listen for gmaps
$(window).load(function(){ 

/* MAP HIGHLIGHT */
$(function() {
    $('.map').maphilight({
          strokeColor: 'FFFFFF',
          strokeOpacity: 0.7,
          strokeWidth: 2,
          fillOpacity: 0.7,
          fillColor:'fcfcfc',
          alwaysOn: false
          //groupBy:true
      });
 });    
     
/** General gdivMessage START *
es: gdivMessageAdd('messagggio', 'danger');
* * * * * * * * * * * * * * * */
$('body').on('click', ".withAlert", function(){ $(this).fadeOut('slow', function(){  } ); });
function gdivMessageAdd(message, type, options){
	var defaults = {autohide: true};
    var opts = $.extend(defaults, options);
	
	if(!message)
		message = 'Scusate per il disaggio.';
	if(!type)
		type = 'warning';
		
	var zindex = parseInt($(".withAlert").length) + 5001;
		// margin-top:'+ 45 * wCount +'px;
	var element = '<div style="z-index: '+ zindex +'" class="alert alert-'+type+' alert-dismissable withAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span class="message">'+message+'</span></div>';
	
	var eobj = $(element).clone();
	
	$(eobj).appendTo("body");
	if(opts.autohide){
		setTimeout(function(){$(eobj).hide('slow', function(){$(this).remove();});}, 4000);
	}
}
/* * * * * * * * * * * * * * * * 
* * General gdivMessage END **/

/* * * * * * * * * * * * * * * *
// function for collapse a panel START */
$(".withBox").on('click', ".showBox", function(){
    var thisBox = $(this).parent();
    $(".shBox", thisBox).show();
	$(".shSwitch", thisBox).toggle(1, function(){ 
		if($(this).hasClass('set'))
			$(this).removeClass('set');
		else
			$(this).addClass('set');
	});
});
$(".withBox").on('click', ".hideBox", function(){
    var thisBox = $(this).parent();
    $(".shBox", thisBox).hide();
	$(".shSwitch", thisBox).toggle(1, function(){ 
		if($(this).hasClass('set'))
			$(this).removeClass('set');
		else
			$(this).addClass('set');
	});
});
/* showhideBox (toggle)
 * - <withBox [collapsable]>
 * - - <showhideBox> [<shSwitch> & <shSwitch>]
 * - - - <shBox>
 * - </>
 */
$(".withBox").on('click', ".showhideBox", function(){
	var thisBox = $(this).parent();
    $(".shBox", thisBox).toggle('fast');
    $(".shSwitch", thisBox).toggle(1, function(){ 
		if($(this).hasClass('set'))
			$(this).removeClass('set');
		else
			$(this).addClass('set');
	});
});
// no close on himself click
$(".withBox.collapsable").mouseup(function(){ return false; });
/* close on document click
$(document).mouseup(function(){
    $(".shBox", $(".withBox.collapsable")).slideUp();
    var shSwitch = $(".shSwitch",  $(".withBox.collapsable"));
	if(shSwitch.hasClass('set'))
		shSwitch.toggle(1, function(){ 
		if($(this).hasClass('set'))
			$(this).removeClass('set');
		else
			$(this).addClass('set');
	});
});*/
/*  // function for collapse a panel END
* * * * * * * * * * * * * * * */ 
// $('.withTt').tooltip(); // bootstrap tooltip GO!
     
     
     

/* **  Aggiunta Eliminazione Modifica Commento Feedback ** */
$(function (){
    /* Ratings */

    /*  FeedBack zone */    
    $("#feedbackContainer form").submit( function(){
        var thisForm = $(this);
        console.info('asd');
        
        var vote = parseInt($("#gRatingVal", thisForm).val());

        if(vote < 1){
            $("#generalRating").parent().addClass('has-error bg-danger');
        } else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/hospital/addfeedback',
                data: $(this).serialize(),
                success: function(json){
                    if(json.success){
                        gdivMessageAdd(json.id, 'success', {autohide: false});
                        /*thisBox.attr('id', id+'feedback');
                        $('.feedback', thisForm).text(sendDataComment.feedback);// change textarea with commentor name, textarea delete automatically
                        $('h4', thisBox).text(sendDataComment.subject).parent().show().prev().show();

                        // delete un utilizated element
                        $("textarea", thisForm).remove();
                        $("input", thisForm).remove();
                        $("label", thisForm).remove();

                        $("#stars", thisForm).remove();
                        commentForm = null; // delete all form data*/
                    } else {
                        gdivMessageAdd(json.message, 'danger', {autohide: false});
                    }
                },
                error: function(){
                    gdivMessageAdd('Unaxpected Error', 'danger', {autohide: false});
                }
            });
        }
        return false;
    });
}); // END Feedback
     
}); // END $(window).load( function{...
<?php
if(stripos('btc.loc', $_SERVER['HTTP_HOST']) !== FALSE){
	$conf = 'development';
	ini_set('display_errors', -1);
	error_reporting(-1);
} else {
	$conf = 'production';
	ini_set('display_errors', 0); // TODO: @totest @todo
	error_reporting(0); // TODO: @totest @todo
}
//exit($conf); @todo
define('ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);         
define('DB_PREFIX', 'btc_');         

// bad performnce :( // 
set_include_path(ROOT.DS.'private'.DS.'libraries'.PS.ROOT.DS.'private'.PS.ROOT.DS.'private'.DS.'libraries'.DS.'zf1'.DS.'library');
// Autoloader
    require_once ROOT.DS.'private'.DS.'libraries'.DS.'zf1'.DS.'library'.DS.'Zend'.DS.'Loader'.DS.'Autoloader.php';
    $loader = Zend_Loader_Autoloader::getInstance();
    $loader->setFallbackAutoloader(true);
    $loader->suppressNotFoundWarnings(false);
    $loader->pushAutoloader(NULL, 'Smarty_' );    
// Load Config
    $config = new Zend_Config_Ini(ROOT.DS.'private'.DS.'configs'.DS.'application.ini', $conf);
    Zend_Registry::set('config', $config);
// Log in XML please :) http://framework.zend.com/manual/1.12/ru/zend.log.formatters.html
    $stream = new Zend_Log_Writer_Stream(ROOT.DS.'private'.DS.'data'.DS.'logs'.DS.'debug.log');
	$formatter = new Zend_Log_Formatter_Xml();
	$stream->setFormatter($formatter);
	$logger = new Zend_Log();
	$logger->addWriter($stream);
    Zend_Registry::set('logger', $logger);
/*  To Firebug @todo @totest
    $f_logger = new Zend_Log(new Zend_Log_Writer_Firebug());
    Zend_Registry::set('f_logger', $f_logger);*/
// Database
    $params = array('host' => $config->database->params->host,
                    'username' => $config->database->params->username,
                    'password' => $config->database->params->password,
                    'port' => $config->database->params->port,
                    'dbname' => $config->database->params->dbname);
    $db = Zend_Db::factory($config->database->adapter, $params);
    Zend_Registry::set('db', $db);
    Zend_Registry::get('db')->query("SET NAMES 'utf8'");
// Session Start
	Zend_Session::start();
	$session = new Zend_Session_Namespace('general');
	Zend_Registry::set('session', $session);
// setup application authentication
    $auth = Zend_Auth::getInstance();
    $auth->setStorage(new Zend_Auth_Storage_Session());

// Controllers
    $controller = Zend_Controller_Front::getInstance();
    $controller->setControllerDirectory(ROOT.DS.'private'.DS.'Controllers');
    $controller->registerPlugin(new CustomControllerAclManager($auth));

// Error handler
    $plugin = new Zend_Controller_Plugin_ErrorHandler();
    $plugin->setErrorHandlerController('Error')->setErrorHandlerAction('error');
    $controller->registerPlugin($plugin);

if($conf == 'development'){
	$controller->throwExceptions(true);
}else{
	$controller->throwExceptions(false);
}
// View to Smarty 	
    $vr = new Zend_Controller_Action_Helper_ViewRenderer();
    $vr->setView(new Templater());
    $vr->setViewSuffix('tpl');
    Zend_Controller_Action_HelperBroker::addHelper($vr);

    /** Imposto un ruote per le url dei giornali
    *	come prima cosa per l'istanza di @Zend_Controller_Router_Route ci passo @una_stringa dell'url che desidero
    *	come secondo parametro passo un @array_parametri_di_default
    *	dopo di che con getRouter()->addRoute('@route', $route) aggiungo la route al controller prima di 'dispatch'
    **/
    
	
    $lang = (isset($session->siteLang)) ? $session->siteLang : 'it' ;
    
    // change default router 
    $controller->getRouter()->addRoute('default', 
        new Zend_Controller_Router_Route( ':controller/:action/*', 
                array( 'controller' => 'index', 'action' => 'index', 'lang' => $lang ) 
        ) 
    ); 
    // add multilingual route 
    $controller->getRouter()->addRoute('default_multilingual', 
        new Zend_Controller_Router_Route( ':lang/:controller/:action/*', 
                array( 'controller' => 'index', 'action' => 'index', 'lang' => $lang ), 
                array( 'lang' => '\w{2}' ) 
        ) 
    );
	
    // SetUp route 
    $route = new Zend_Controller_Router_Route('/user/:identity', array('controller' => 'user', 'action' => 'view'));
    $controller->getRouter()->addRoute('user_view', $route);
	
	
    // SetUp route fo User View
    $route = new Zend_Controller_Router_Route(':lang/user/:identity', array('controller' => 'user', 'action' => 'view', 'lang' => $lang));
    $controller->getRouter()->addRoute('user_ml_view', $route);
    // SetUp route fo User View Review types
    $route = new Zend_Controller_Router_Route_Regex('(.{2})\/user\/(.+)\/review\/(.+)', array('controller' => 'user', 'action' => 'review'), array(1 => 'lang', 2 => 'identity', 3 => 'review'), '%s/user/%s/review/%s');
    $controller->getRouter()->addRoute('user_ml_review', $route);
    
    $controller->dispatch();
?>
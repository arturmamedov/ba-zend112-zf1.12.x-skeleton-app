<?php
class FormProcessor_UserRegistration extends FormProcessor
{
    protected $db = null;
    public $user = null;
    public $time_zone;
    public $language;
    protected $_validateOnly = false;
    protected $validateMail = null;

    public function __construct($db, $translate)
    {
        parent::__construct();

        $this->translate = $translate;

        $this->db = $db;
        $this->user = new DatabaseObject_User($db);
        $this->user->type = 'user';
    }
		
    public function validateOnly($flag, $data){
        $this->_validateOnly = (bool) $flag;
        $this->validateMail = (isset($data['mail'])) ? $data['mail'] : null;
        $this->social = $this->user->social = (isset($data['social'])) ? $data['social'] : null;
    }

    public function process(Zend_Controller_Request_Abstract $request)
    {	
        $date = new Zend_Date();
        // if request Ajax do only email validation
        if($this->_validateOnly){
            // validate the e-mail address
            $this->user_mail = $this->validateMail;
            $validator = new Zend_Validate_EmailAddress();

            if(strlen($this->user_mail) == 0 || !$validator->isValid($this->user_mail)){
                $this->addError('user_mail', $this->translate->translate('Please enter a valid E-mail address'));
            } elseif($this->user->existMail($this->user_mail)){
                if(strstr($request->getHeader('referer'), 'connecttw') !== FALSE){
                    $this->addError('user_mail', sprintf($this->translate->translate("An account with this email is already registered, <a href='%s'>login</a> or if you do not remember your password <a href='%s'>retrieve it here</a>."), '/account/login', '/account/fetch').' '.$this->translate->translate('You can link Twitter to your existing account from the general settings of your account'));
                } else {
                    $this->addError('user_mail', sprintf($this->translate->translate("An account with this email is already registered, <a href='%s'>login</a> or if you do not remember your password <a href='%s'>retrieve it here</a>."), '/account/login', '/account/fetch'));
                }
            } else {
                $this->user->user_mail = $this->user_mail;
            }
        } else {
            // validate the user's name
            $this->first_name = ucfirst($this->sanitize($request->getPost('first_name')));
            if(strlen($this->first_name) <= 2){
                $this->addError('first_name', $this->translate->translate('Please enter your name correctly'));
            } else {
                $this->user->profile->first_name = $this->first_name;
            }

            $this->last_name = ucfirst($this->sanitize($request->getPost('last_name')));
            if(strlen($this->last_name) <= 1){
                $this->addError('last_name', $this->translate->translate('Please enter your surname correctly'));
            } else {
                $this->user->profile->last_name = $this->last_name;
            }

            // validate the e-mail address
            $this->user_mail = $this->sanitize($request->getPost('user_mail'));
            $validator = new Zend_Validate_EmailAddress();

            if(strlen($this->user_mail) == 0){
                $this->addError('user_mail', $this->translate->translate('Please enter your E-mail address'));
            } elseif(!$validator->isValid($this->user_mail)){
                $this->addError('user_mail', $this->translate->translate('Please enter a valid E-mail address'));
            } elseif($this->user->existMail($this->user_mail)){
                $this->addError('user_mail', $this->translate->translate('A user with this E-mail is already registered'));
            } else {
                $this->user->user_mail = $this->user_mail;
            }

            // check if a new password has been entered and if so validate it
            $this->password = $this->sanitize($request->getPost('password'));
            $this->password_confirm = $this->sanitize($request->getPost('password_confirm'));
            
            $uppercase = preg_match('/[A-Z]+?/', $this->password);
            $number = preg_match('/[0-9]+?/', $this->password);
            $symbol = preg_match('/[@#$%]+?/', $this->password);
            
            if(strlen($this->password) < 6 || ($uppercase == 0 && $number == 0 && $symbol == 0)){
                $this->addError('password', $this->translate->translate('Enter a password that meets the security requirements, thanks'));
            } 
            if(strlen($this->password) > 31) {
                $this->addError('password', $this->translate->translate('The password is to long (use less than 31 chars please)'));
            } elseif($this->password != $this->password_confirm){
                $this->addError('password_confirm', $this->translate->translate('The two passwords do not match'));
            } else {
                $this->user->password = $this->password;
            }


            if($this->social != 'Twitter' && $this->social != 'Vk'){
                // validate reCAPTCHA
                require_once(ROOT.DS.'private'.DS.'libraries'.DS.'ReCaptcha'.DS.'recaptchalib.php');
                $this->private_key = Zend_Registry::get('config')->captcha->private_key;
                $resp = recaptcha_check_answer ($this->private_key,	$_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

                if(!$resp->is_valid){
                    $this->addError('captcha', $this->translate->translate('The security code is invalid'));
                }
            }

            $this->user->user_active = 0;
            $this->user->user_type = 'user';
            $this->user->ts_created = $date->get('YYYY-MM-dd HH:mm:ss');
            $this->user->ts_last_online = $date->get('YYYY-MM-dd HH:mm:ss');
            $this->user->ts_last_login = $date->get('YYYY-MM-dd HH:mm:ss');

            if($this->social == 'Twitter'){
                $uidentity = 't-w'.$date->get('B');
                
                $session = new Zend_Session_Namespace('login');

                if(!empty($session->tw_id_conn)){
                    $this->user->profile->tw_id_conn = $session->tw_id_conn;
                }
                if(!empty($session->tw_profile)){
                    $this->user->profile->tw_profile = $session->tw_profile;
                }
                if(!empty($session->about)){
                    $this->user->profile->about = $session->about;
                }
                if(!empty($session->location)){
                    $this->user->profile->location = $session->location;
                }
                if(!empty($session->web_site)){
                    $this->user->profile->web_site = $session->web_site;
                }
                if(!empty($session->screen_name)){
                    $this->user->profile->tw_name = $session->screen_name;
                }

                // @photo
                if(!empty($session->poster)){
                    $cdn_path = $this->user->image_cloudfront_endpoint.$this->user->bucket_image_path;
                    $bucket_path = $this->user->bucket.'/'.$this->user->bucket_image_path;
                    
                    $this->user->profile->poster_name = 'user_tw_l_'.$this->user->profile->tw_id_conn.'_'.time().'_'.uniqid().'.jpg';
            
                    $this->user->poster = $cdn_path.$this->user->profile->poster_name;
                    $this->user->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->user->profile->poster_name;
                    $this->user->profile->poster_normal = $cdn_path.'normal/'.$this->user->profile->poster_name;            

                    $this->user->getImgByUrl($session->poster, $bucket_path, $this->user->profile->poster_name);
                }

                $this->user->profile->tw_connected = 1;
                $this->user->profile->tw_autopost = 1;

                // for future call to API
                $this->user->profile->tw_access_token = $session->tw_access_token;
                $this->user->profile->tw_access_token_secret = $session->tw_access_token_secret;

                $follow_us = $this->sanitize($request->getPost('follow_us'));
                if($follow_us != NULL){
                    $twitter = $this->user->twAccesstoken('oo');
                    $twitter->post('friendships/create', array('screen_name' => 'HealthEye_it'));
                }
            } else {
                $uidentity = 'h-e'.$date->get('B');
            }
            
            $date->set('00:00:00', Zend_Date::TIMES);
            $uidentity = $date->get('U').$uidentity; // preg_replace('/([\s\.]+)/', '', microtime()); //time(); // preg_replace('/\s+/', '', 'user-'.$this->first_name.$this->last_name.time());
            if($this->user->existIdentity($uidentity)){
                $logger = Zend_Registry::get('logger');
                $logger->warn('User[Registration] - Equal identity :O {identity: '.$uidentity);
                $this->identity = $uidentity.'r'.rand(0, 1000);
            } else {
                $this->user->identity = $uidentity;
            }

            // if no errors have occurred, save the user
            if(!$this->hasError()) {                
                $this->user->save();
                
                if($this->social == 'Twitter'){
                    // initialize the options
                    $options = array(
                        array('resize' => 'fit', 'w' => 100, 'h' => 100, 'path' => 'thumbnail', 'image' => $this->user->profile->poster_name),
                        array('resize' => 'height', 'w' => 220, 'h' => 220, 'path' => 'normal', 'image' => $this->user->profile->poster_name)
                    );
                    $this->user->makeThumbs('image/jpg', $bucket_path, $options);
                }
            }

            // return true if no errors have occurred
            return !$this->hasError();
        }
    }
}
<?php
/**
 * Form processor for user account
 * @todo make something for profile to ex: website, blog, social link...
 */
class FormProcessor_UserAccount extends FormProcessor
{
    protected $db = null;
    public $user = null;

    public function __construct($db, $translate, $user_id)
    {
        parent::__construct();

        $this->translate = $translate;

        $this->db = $db;
        $this->user = new DatabaseObject_User($db);
        $this->user->load($user_id);

        $this->first_name = $this->user->profile->first_name;
        $this->last_name = $this->user->profile->last_name;
        $this->identity = $this->user->identity;
        $this->birthday_ts = $this->user->profile->birthday_ts;

        $this->description = $this->user->description;

        $this->time_zone = $this->user->time_zone;
        $this->language = $this->user->language; //$this->user->langTitleById($this->user->language);
        $this->user_locale = $this->user->user_locale;

        $this->user_mail = $this->user->user_mail;
        /*
        $this->web_site = $this->user->profile->web_site;
        $this->blog = $this->user->profile->blog;
        $this->fb_profile = $this->user->profile->fb_profile;
        $this->gp_profile = $this->user->profile->gp_profile;
        $this->tw_profile = $this->user->profile->tw_profile;
        $this->linkedin_profile = $this->user->profile->linkedin_profile;
        $this->skype_account = $this->user->profile->skype_account;
        */
    }

    public function process(Zend_Controller_Request_Abstract $request)
    {
        // validate the user's name
        $this->first_name = $this->sanitize($request->getPost('first_name'));
        if(strlen($this->first_name) < 1 || strlen($this->first_name) > 120){
            $this->addError('first_name', $this->translate->translate('Please enter your name correctly'));
        } else {
            $this->user->profile->first_name = $this->first_name;
        }
        $this->last_name = $this->sanitize($request->getPost('last_name'));
        if (strlen($this->last_name) < 1 || strlen($this->last_name) > 140){
            $this->addError('last_name', $this->translate->translate('Please enter your surname correctly'));
        } else {
            $this->user->profile->last_name = $this->last_name;
        }

        /*
        $this->identity = $this->sanitize($request->getPost('identity'));
        if(!preg_match('/^[\w_\.\-]{4,60}$/', $this->identity)){
            $this->addError('identity', $this->translate->translate('The username must contain minimum 4, maximum 60 characters and acceptable character to a url (. or -)'));
        } elseif($this->identity != $this->user->identity && $this->user->existIdentity($this->identity)){
            $this->addError('identity', $this->translate->translate('The chosen username is already used try to test other combinations, you can use symbols (. Or -)'));
        } else {
            $this->user->identity = preg_replace('/\s+/', '', $this->identity);
        }
        */

        $this->mailaffected = false;
        // validate the e-mail address
        if($request->getPost('user_mail') !== NULL){
            $this->user_mail = $this->sanitize($request->getPost('user_mail'));
            $validator = new Zend_Validate_EmailAddress();

            if(strlen($this->user_mail) == 0){
                $this->addError('user_mail', $this->translate->translate('Please enter your E-mail address'));
            } elseif(!$validator->isValid($this->user_mail)){
                $this->addError('user_mail', $this->translate->translate('Please enter a valid E-mail address'));
            } elseif($this->user->existMail($this->user_mail)){
                $this->addError('user_mail', $this->translate->translate('A user with this E-mail is already registered'));
            } else {
                $this->user->user_mail = $this->user_mail;
                $this->user->user_active = 0;
                
                // Genera l'orario e la chiave della conferma e-mail
                $this->user->profile->confirm_key_ts = time();
                $this->user->profile->confirm_key = ($this->user->getId().uniqid());
                
                $this->mailaffected = true;
            }
        }
        
        // * birthday
        $this->birthday_year = (int)$this->sanitize($request->getPost('birthdayYear'));
        $this->birthday_day = (int)$this->sanitize($request->getPost('birthdayDay'));
        $this->birthday_month = (int)$this->sanitize($request->getPost('birthdayMonth'));
        if($this->birthday_year > 0 && $this->birthday_day > 0 && $this->birthday_month > 0){
            $date = new Zend_Date();
            $date->set($this->birthday_year.'-'.$this->birthday_month.'-'.$this->birthday_day, "YYYY-MM-DD");
            // timestamp for select smarty
            $this->user->profile->birthday_ts = $date->get("U");
            $this->user->profile->birthday_year = $this->birthday_year;
            $this->user->profile->birthday_day = $this->birthday_day;
            $this->user->profile->birthday_month = $this->birthday_month;
        } else {
            unset($this->user->profile->birthday_ts);
            unset($this->user->profile->birthday_year);
            unset($this->user->profile->birthday_day);
            unset($this->user->profile->birthday_month);
        }//$this->addError('birthday', 'Per favore selezionate una data valida');	


        // validate and save description
        $this->description = $this->sanitize($request->getPost('description'));
        if(strlen($this->description) == 0){
            //$this->addError('description', 'Per favore inserite una descrizione');
            unset($this->user->description);
        } elseif(strlen($this->description) > 330){
            $this->addError('description', $this->translate->translate('Description about you are too long, sorry'));
        } else {
            $this->user->description = $this->description;
        }

        // if no errors have occurred, save the user
        if(!$this->hasError()) {
            $this->user->save();
        }

        // return true if no errors have occurred
        return !$this->hasError();
    }
}
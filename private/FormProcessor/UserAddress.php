<?php
class FormProcessor_UserAddress extends FormProcessor
{
    protected $db = null;
    public $user = null;

    public function __construct($db, $translate, $user_id)
    {
        parent::__construct();

        $this->translate = $translate;

        $this->db = $db;
        $this->user = new DatabaseObject_User($db);
        $this->user->load($user_id);

        $this->country_id = $this->user->profile->country_id;
        $this->city_id = $this->user->profile->city_id;
        $this->region_id = $this->user->profile->region_id;
        $this->province_id = $this->user->profile->province_id;
        $this->zip_code = $this->user->profile->zip_code;
        $this->address = $this->user->profile->address;
    }

    public function process(Zend_Controller_Request_Abstract $request)
    {
        /// * * *   Localita * * * ///
        // validate and save country
        $this->country_id = $this->sanitize($request->getPost('country_id'));
        if($this->country_id == 0){
            //$this->addError('country_id', 'Per favore inserite lo "Stato" della vostra residenza');
            unset($this->user->profile->country_id);
        } else {
            $this->user->profile->country_id = $this->country;
        }
        // validate and save @city_id
        $this->city_id = $this->sanitize($request->getPost('city_id'));
        if($this->city_id == 0){
            //$this->addError('city_id', 'Per favore inserite la "Citta" della vostra residenza');
            unset($this->user->profile->city_id);
        } else {
            $this->user->profile->city_id = $this->city_id;
        }
        // validate and save @region_id
        $this->region_id = $this->sanitize($request->getPost('region_id'));
        if($this->region_id == 0){
            //$this->addError('region_id', 'Per favore inserite la "Provincia" della vostra residenza');
            unset($this->user->profile->region_id);
        } else {
            $this->user->profile->region_id = $this->region_id;
        }
        // validate and save @province_id
        $this->province_id = $this->sanitize($request->getPost('province_id'));
        if($this->province_id == 0){
            //$this->addError('province_id', 'Per favore inserite la vostra province_id');
            unset($this->user->profile->province_id);
        } else {
            $this->user->profile->province_id = $this->province_id;
        }
        // validate and save @zip_code
        $this->zip_code = $this->sanitize($request->getPost('zip_code'));
        if(strlen($this->zip_code) == 0){
            //$this->addError('zip_code', 'Per favore inserite il "Codice Postale" della vostra residenza');
            unset($this->user->profile->zip_code);
        } else {
            if(!is_numeric($this->zip_code) || strlen($this->zip_code) <= 3){
                $this->addError('zip_code', 'Per favore inserite un codice postale valido');
            } else {
                $this->user->profile->zip_code = $this->zip_code;
            }
        }
        // validate and save @address
        $this->address = $this->sanitize($request->getPost('address'));
        if(strlen($this->address) == 0){
            //$this->addError('address', 'Per favore inserite "l\'indirizzo" della vostra residenza');
            unset($this->user->profile->address);
        } else {
            $this->user->profile->address = $this->address;
        }

        // if no errors have occurred, save the user
        if(!$this->hasError()) {
            $this->user->save();
        }

        // return true if no errors have occurred
        return !$this->hasError();

    }
}
<?php
class FormProcessor_UserPassword extends FormProcessor
{
    protected $db = null;
    public $user = null;

    public function __construct($db, $translate, $user_id)
    {
        parent::__construct();

        $this->translate = $translate;

        $this->db = $db;
        $this->user = new DatabaseObject_User($db);
        $this->user->load($user_id);
    }

    public function process(Zend_Controller_Request_Abstract $request)
    {
        // if OLD Password , check if a new password has been entered and if so validate it
        $old_pass = $request->getPost('old_password');
        $this->password = $this->sanitize($request->getPost('change_password'));
        $this->password_confirm = $this->sanitize($request->getPost('password_confirm'));
        
        $uppercase = preg_match('/[A-Z]+?/', $this->password);
        $number = preg_match('/[0-9]+?/', $this->password);
        $symbol = preg_match('/[@#$%]+?/', $this->password);
        
        if(strlen($old_pass) > 0 || $this->user->password === NULL){
            if (strlen($this->password) == 0){
                $this->addError('change_password', $this->translate->translate('To change the password you must enter a new password'));
            } 
            if(strlen($this->password) < 6 || ($uppercase == 0 && $number == 0 && $symbol == 0)){
                $this->addError('change_password', $this->translate->translate('Enter a password that meets the security requirements, thanks'));
            } elseif ($this->password != $this->password_confirm){
                $this->addError('password_confirm', $this->translate->translate('The two passwords do not match'));
            } else {
                if($this->user->password === NULL || $this->user->camparePassword($old_pass, $this->user->user_id)){
                    $this->user->password = $this->password;
                } else {
                    $this->addError('old_password', $this->translate->translate('The password is not the one you are currently using'));
                }
            }
        } else {
            $this->addError('old_password', $this->translate->translate('To change password, you must know your current password'));
        }            
        
        // if no errors have occurred, save the user
        if(!$this->hasError()) {
            $this->user->save();
        }

        // return true if no errors have occurred
        return !$this->hasError();
    }
}
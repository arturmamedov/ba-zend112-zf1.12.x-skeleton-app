<?php
class FormProcessor_About extends FormProcessor
{
    protected $db = null;
    public $item = null;

    public function __construct($translate)
    {
        parent::__construct();
        $this->translate = $translate;
    }

    public function process(Zend_Controller_Request_Abstract $request)
    {
        if(strlen($this->mail) == 0){
            $this->mail = $this->sanitize($request->getPost('mail'));
        }
        if(strlen($this->name) == 0){
            $this->name = $this->sanitize($request->getPost('name'));
        }
		$this->text = $this->sanitize($request->getPost('text'));
		
		// validate the e-mail address
		$validator = new Zend_Validate_EmailAddress();
		if(strlen($this->mail) == 0){
			$this->addError('mail', $this->translate->translate('Per favore inserite il vostro indirizzo e-mail'));
        }
		elseif(!$validator->isValid($this->mail)){
			$this->addError('mail', $this->translate->translate('Per favore inserite un indirizzo e-mail valido'));
        }
        
		if(strlen($this->name) == 0){
			$this->addError('name', $this->translate->translate('Per favore inserite il vostro nome'));
        }
        
		// text
        $lenght = strlen($this->text);
        if($lenght < 15){
            $this->addError('text', 'Troppo poco testo, sei sicuro sia importante?');
        }elseif($lenght > 10000 ){
            $this->addError('text', 'Troppo testo per una sola volta, non dici :)');
        }
        
        return !$this->hasError();
    }
}
?>
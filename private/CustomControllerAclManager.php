<?php
class CustomControllerAclManager extends Zend_Controller_Plugin_Abstract
{
    // default user role if not logged or (or invalid role found)
    private $_defaultRole = 'guest';

    // the action to dispatch if a user doesn't have sufficient privileges
    private $_authController = array('controller' => 'account', 'action' => 'login');

    public function __construct(Zend_Auth $auth)
    {
        $this->auth = $auth;
        $this->acl = new Zend_Acl();

        // add the different user roles
        $this->acl->addRole(new Zend_Acl_Role($this->_defaultRole));
        $this->acl->addRole(new Zend_Acl_Role('user'), $this->_defaultRole);
        $this->acl->addRole(new Zend_Acl_Role('puser'), 'user'); // user that pay
        $this->acl->addRole(new Zend_Acl_Role('master'), 'puser');
        
		
		
        // add the resources we want to have control over
        $this->acl->add(new Zend_Acl_Resource('index'));
        
        $this->acl->add(new Zend_Acl_Resource('account'));
        $this->acl->add(new Zend_Acl_Resource('user'));
		
        // allow access to everything for all users by default (so users can access free resource index etc.)
        $this->acl->allow();
		
        // deny resource for all (so after allow resource only for right user)
        $this->acl->deny(null, 'index');
        $this->acl->deny(null, 'account');
        $this->acl->deny(null, 'user');
		
		/// INDEX
        $this->acl->allow('guest', 'index', array('index', 'about', 'terms'));
        $this->acl->allow('master', 'index');
        
		
        // ACCOUNT    
        $this->acl->allow('guest', 'account', array('login', 'logout', 'signup', 'registered', 'fetch'));
        $this->acl->allow('user', 'account', array('index', 'address', 'password', 'biupload'));
        
        /// USER
		$this->acl->allow('guest', 'user', array('view'));
        $this->acl->allow('master', 'user', array('delete', 'manage', 'managedata'));
		
	}

    /**
     * preDispatch
     *
     * Before an action is dispatched, check if the current user
     * has sufficient privileges. If not, dispatch the default
     * action instead
     *
     * @param Zend_Controller_Request_Abstract $request
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        // check if a user is logged in and has a valid role,
        // otherwise, assign them the default role (guest)
        if($this->auth->hasIdentity())
            $role = $this->auth->getIdentity()->user_type;
        else
            $role = $this->_defaultRole;

        if(!$this->acl->hasRole($role))
            $role = $this->_defaultRole;

        // the ACL resource is the requested controller name
        $resource = $request->controller;

        // the ACL privilege is the requested action name
        $privilege = $request->action;

        // if we haven't explicitly added the resource, check
        // the default global permissions
        if(!$this->acl->has($resource))
            $resource = null;

        // access denied - reroute the request to the default action handler
        if(!$this->acl->isAllowed($role, $resource, $privilege)) {
            $request->setControllerName($this->_authController['controller']);
            $request->setActionName($this->_authController['action']);
        }
    }
}
?>
{include file='layout/head.tpl'}
</head>

<body id="about">
	{include file='layout/navbar.tpl'}
    {include file='layout/breadcrumbs.tpl'}
    
<!-- Main row .container -->
<div class="container">

    <div class="row">
        <div class="page-header text-center">
            <h2>Regioni</h2>
        </div>

        <!-- Right side wrapper col-->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center-block" >
            <!-- <hr class="hr-sm"> -->

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left" style="padding-top:30px;">
                    {include file='widget/map.tpl'}
                    
                    <div class="text-center">
                        {foreach from=$regions item=region}
                            <a href="/place/region/id/{$region->id}" class="" style="text-decoration: underline; margin:0 4px;">#{$region->name}</a>
                        {/foreach}
                    </div>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right">
                <!-- SLIDER -->
                     <div class="flexslider">
                      <ul class="slides">
                            <li data-thumb="/public/files/structure/images/rimini_ospedale.jpg">
                                <img src="/public/files/structure/images/rimini_ospedale.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/ospedale-rimini1.jpg">
                                <img src="/public/files/structure/images/ospedale-rimini1.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/italy2.jpg">
                                <img src="/public/files/structure/images/italy2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london2.jpg">
                                <img src="/public/files/structure/images/london2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london2.jpg">
                                <img src="/public/files/structure/images/london2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london3.jpg">
                                <img src="/public/files/structure/images/london3.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/italy2.jpg">
                                <img src="/public/files/structure/images/italy2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london2.jpg">
                                <img src="/public/files/structure/images/london2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london2.jpg">
                                <img src="/public/files/structure/images/london2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london3.jpg">
                                <img src="/public/files/structure/images/london3.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london2.jpg">
                                <img src="/public/files/structure/images/london2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london2.jpg">
                                <img src="/public/files/structure/images/london2.jpg" />
                            </li>
                            <li data-thumb="/public/files/structure/images/london3.jpg">
                                <img src="/public/files/structure/images/london3.jpg" />
                            </li>
                      </ul>
                    </div>
                <!-- /SLIDER -->
            </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p>Qui non so cosa ci si puo' scrivere, in tema alla pagina delle regioni? Le regioni Italiane ... hanno strutture sanitarie ... boh La struttura dell'ospedale Infermi è costituita da un blocco centrale e da diversi padiglioni ai quali si accede tramite percorsi esterni: Poliambulatori, Ovidio, Flaminio. Ha 3 ingressi: quello principale è in Via Settembrini, gli altri da Via Flaminia e da Via Ovidio (una traversa di Via Flaminia). Per le urgenze ha un accesso privilegiato che porta direttamente al Pronto Soccorso in Via Settembrini. Sul piano rialzato (R) sono presenti diversi servizi aggiuntivi: Sportello bancario e bancomat: scala A; Chiesa: scala B; Bar interno: scala C; Edicola: scala C.</p>
                        <hr>

                    </div>
                </div>
        </div>
        <!-- /Right side wrapper col-->
    </div>
    <!-- / .row -->

<div class="page-header text-center">
    <h2><i class="icon-building text-black"></i> Strutture e Recensioni <i class="icon-eye-open text-black"></i></h2>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {include file='place/lib/example-structure.tpl'}
        </div>
    </div> <!-- /.structure cont -->
    
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {include file='place/lib/example-feedback.tpl'}
        </div>
    </div> <!-- /.feed cont -->
</div> <!-- /.row -->
</div> <!-- /.container -->

{*}
<div class="container">

  <div class="row">
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-building icon-4x text-info"></i>
      <h4>Hotels</h4>
      <p>Search more than 300'000 hotels around the World</p>
      <p><a class="btn btn-info" href="#">All hotels  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-group icon-4x text-info"></i>
      <h4>Hostels</h4>
      <p>Search more than 300'000 hostels around the World</p>
      <p><a class="btn btn-info" href="#">All hotels  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-ticket icon-4x text-info"></i>
      <h4>Tickets</h4>
      <p>Search more than 300'000 hostels around the World</p>
      <p><a class="btn btn-info" href="#">Buy tickets  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->



    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-sun icon-4x text-info"></i>
      <h4>Resorts</h4>
      <p>Search more than 300'000 resorts around the World</p>
      <p><a class="btn btn-info" href="#">All resorts  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->



  </div><!-- /.row -->    

  <div class="row">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 text-center">
                    <p class="lead ">
                        <i class="icon-gift text-info"></i> Subscribe to newsletter and get special offers to you email every week!
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 text-center">
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                        </div>
                          <button type="submit" class="btn btn-info">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  
</div><!-- /.container -->
{/*}
    
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl' hide=true}



  
<!-- gMap PLUGIN -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/public/js/jquery.gmap.js"></script>
	
<!-- FLEXSLIDER INIT SCRIPT-->
<!-- Optional FlexSlider Additions -->
<script src="/public/js/jquery.easing.js"></script>
<script src="/public/js/jquery.mousewheel.js"></script>
<script src="/public/js/jquery.flexslider-min.js"></script>
    
<!-- INIT SCRIPT - show gMap onclick -->
<script>
{literal}
          
$(window).load(function(){
    $('.flexslider').flexslider({
        animation: "fade",
        controlNav: "thumbnails",
        start: function(slider){
            $('body').removeClass('loading');
        }
    });
});
    
$(document).ready(function(){

    $('.toggler').click(function(){
    jQuery('.gmappanel').removeClass('hide-map').addClass('show-map').css('height', '600');
    $('.toggler').css('display', 'none');
    $('.toggler-hide').css('display', 'block');

            var $map = $('#map');
            google.maps.event.addDomListener(window, 'resize', function() {
                map.setCenter(homeLatlng);
            });
            if( $map.length ) {

                $map.gMap({
                    address: 'Viale Luigi Settembrini, 2, 47923 Rimini',
                    zoom: 14,
                    markers: [
                        { 'address' : 'Rimini ospedale',}
                    ]
                });

            }


    });
    $('.toggler-hide').click(function(){
        jQuery('.gmappanel').removeClass('show-map').addClass('hide-map').css('height', '0');
        $('.toggler').css('display', 'block');
        $('.toggler-hide').css('display', 'none');
    });
});
{/literal}
  </script>
  
</body>
</html>
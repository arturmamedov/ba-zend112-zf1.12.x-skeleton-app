{include file='layout/head.tpl'}
</head>

<body id="about">
	{include file='layout/navbar.tpl'}
    {include file='layout/breadcrumbs.tpl'}
    
<!-- Main row .container -->
<div class="container">

<div class="row">
    <!-- Right side wrapper col-->
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" >
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" >
                <h2>{$structure->name}</h2>
                <p><span class="text-rose"><i class="icon-eye-open"></i> 54 Recensioni</span> | <span class="text-danger"><i class="glyphicon glyphicon-user"></i> 132 contributori</span></p>
                <small><i class="glyphicon glyphicon-road"></i> Parcheggio: A pagamento. Alcuni posti gratuiti nella strada chiusa che costeggia il parcheggio per i dipendenti
</small>

            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                <a  class="btn btn-info btn-block btn-lg toggler" href="#" style="margin:30px 0px 10px 0px;"> <i class="icon-map-marker"></i> Mostra Mappa </a> <a  class="btn btn-info btn-lg btn-block toggler-hide" href="#" style="margin:30px 0px 10px 0px;"><i class="icon-map-marker"></i> Mostra Mappa </a>
            </div>

        </div>

            <hr class="hr-sm">

            <div class="block-wrapper">
            <!-- SLIDER -->
            <div class="flexslider">
                <ul class="slides">
                    <li data-thumb="/public/files/structure/images/{$structure->poster}">
                        <img src="/public/files/structure/images/{$structure->poster}" />
                    </li>
                    <li data-thumb="/public/files/structure/images/ospedale-rimini1.jpg">
                        <img src="/public/files/structure/images/ospedale-rimini1.jpg" />
                    </li>
                </ul>
            </div>
            <!-- /SLIDER -->



            <!-- GMAP-->

                    <div id="map" class="gmappanel hide-map"></div>

            <!-- GMAP-->
        </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p>{$structure->description}</p>
                    <hr>

                    <!-- DESCRIPTION TABS-->
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#home" data-toggle="tab">Caratteristiche</a></li>
                        <li><a href="#profile" data-toggle="tab">Servizi</a></li>
                        {*}<li class="dropdown">
                          <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
                            <li><a href="#dropdown1" tabindex="-1" data-toggle="tab">@fat</a></li>
                            <li><a href="#dropdown2" tabindex="-1" data-toggle="tab">@mdo</a></li>
                          </ul>
                        </li>{/*}
                      </ul>

                      <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="home">
                            <p>Nome</p>
                            <p>Indirizzo</p>
                            <p>Parcheggio</p>
                            <p>Sede di Pronto soccorso</p>
                            <p>Farmacia</p>
                        </div>
                        <div class="tab-pane fade" id="profile">
                          <p>I 23 servizi SI/NO</p>
                        </div>
                        {*}<div class="tab-pane fade" id="dropdown1">
                          <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                        </div>
                        <div class="tab-pane fade" id="dropdown2">
                          <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
                        </div>{/*}
                      </div>
                    <!-- / DESCRIPTION TABS-->
                </div>
            </div>
    </div>
    <!-- /Right side wrapper col-->
    
    <!-- Left side wrapper col  -->
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        {*}<a class="" href="#"><i class="icon-angle-left"></i> <strong>Torna ai risultati di ricerca</strong></a>
        <hr class="hr-sm">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>La tua ricerca</h4>
            </div>
            <div class="panel-body">

                <ul class="list-unstyled">
                    <li><a href="#" title="">Rimini <small>(Ospedali)</small></a><li>
                </ul>
            </div>
            <div class="panel-footer">
                <a class="btn btn-block btn-sm btn-info">Nuova ricerca</a>
            </div>
        </div>{/*}

            <div class="panel panel-primary">

                <div class="panel-body">

                <h4>Correlati</h4>
                <hr class="hr-sm">
            
            {for $foo=1 to 5}
                <!-- RELATED ITEM -->
                <div class="alert" style="position:relative">	
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true" style="position:absolute; top:0px; right:0px;">&times;</a>
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="thumbnail"><img class="img-responsive" src="/public/files/structure/images/london2.jpg" alt="" ></div>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <a href="#" title="">Rimini AUSL</a>
                        </div>
                    </div>
                </div>
                <!-- RELATED ITEM -->
            {/for}
            </div>
        </div>
    </div><!-- / left side -->
</div>
<!-- / .row -->

<hr>

<!-- HOTEL ITEM -->
<div class="structure-feedback row margin-top-10" id="feed5" style="border: 1px solid #cccccc; border-radius: 7px; padding:10px;">
    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
        <div class="thumbnail"><img class="img-responsive" src="/public/img/site/theme/7.jpg" alt="" ></div>
    </div>
    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
        <h4><a href="/structure/view#feed5" title="" >Titolo del Feedback</a></h4>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:0px;">
            <p class="">
                <i class="icon-eye-open text-rose"></i>
                <i class="icon-eye-open text-rose"></i> 
                <i class="icon-eye-close text-gray"></i>
                <i class="icon-eye-close text-gray"></i> 
                <i class="icon-eye-close text-gray"></i>
            </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <p class="lead text-info text-right"><small>Data:</small> <strong>10/05/2014</strong></p>
        </div>

        <p>Sono stata all'ospedale il 04/09/2013 e il 05/09/2013 e a dire il vero mi è piaciuto.Mi avete curato benissimo(a dire il vero ho ancora il mal di gola),è di fuori nel corridoio delle scale A,B,C,D,E,F,G(se esistono le scale E ed F)che mi inquieta,mi fa paura.</p>
        
        <p>
            <span style="text-decoration: underline;">Motivo visita: Famiglia </span>
            |
            <span style="text-decoration: underline;">Data visita: 24/11/2013 </span>
            |
            <span style="text-decoration: underline;">Feedback: Scarso </span>
        </p>
        
        <a href="javascript:;" title="Vota per l'utilità di questa recensione"><i class="icon-thumbs-up"></i> La recensione è utile!</a>
        <a href="javascript:;" title="Espandi la recensione per vedere le valutazioni del utente" class="pull-right"><i class="icon-thumbs-up"></i> Di più! <span class="caret"></span></a>
      </div>
</div>

<div class="structure-feedback row margin-top-10" id="feed5" style="border: 1px solid #cccccc; border-radius: 7px; padding:10px;">
    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
        <div class="thumbnail"><img class="img-responsive" src="/public/img/site/theme/italy3.jpg" alt="" ></div>
    </div>
    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
        <h4><a href="/structure/view#feed5" title="" >Titolo del Feedback</a></h4>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:0px;">
            <p class="">
                <i class="icon-eye-open text-rose"></i>
                <i class="icon-eye-close text-gray"></i> 
                <i class="icon-eye-close text-gray"></i>
                <i class="icon-eye-close text-gray"></i> 
                <i class="icon-eye-close text-gray"></i>
            </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <p class="lead text-info text-right"><small>Data:</small> <strong>10/05/2014</strong></p>
        </div>

        <p>Sono stata all'ospedale il 04/09/2013 e il 05/09/2013 e a dire il vero mi è piaciuto.Mi avete curato benissimo(a dire il vero ho ancora il mal di gola),è di fuori nel corridoio delle scale A,B,C,D,E,F,G(se esistono le scale E ed F)che mi inquieta,mi fa paura.</p>
        
        <p>
            <span style="text-decoration: underline;">Motivo visita: Amici </span>
            |
            <span style="text-decoration: underline;">Data visita: 24/11/2013 </span>
            |
            <span style="text-decoration: underline;">Feedback: Pessimo </span>
        </p>
        
        <a href="javascript:;" title="Vota per l'utilità di questa recensione"><i class="icon-thumbs-up"></i> La recensione è utile!</a>
        <a href="javascript:;" title="Espandi la recensione per vedere le valutazioni del utente" class="pull-right"><i class="icon-thumbs-up"></i> Di più! <span class="caret"></span></a>
      </div>
</div>

{for $foo=1 to 4}
<div class="structure-feedback row margin-top-10" style="border: 1px solid #cccccc; border-radius: 7px; padding:10px;">
    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
        <div class="thumbnail"><img class="img-responsive" src="/public/img/site/theme/55.jpg" alt="" ></div>
    </div>
    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
        <h4><a href="/structure/view#feed5" title="" >Titolo della recensione</a></h4>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:0px;">
            <p class="">
                <i class="icon-eye-open text-rose"></i>
                <i class="icon-eye-open text-rose"></i> 
                <i class="icon-eye-open text-rose"></i>
                <i class="icon-eye-open text-rose"></i> 
                <i class="icon-eye-open text-rose"></i>
            </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <p class="lead text-info text-right"><small>Data:</small> <strong>10/05/2014</strong></p>
        </div>

        <p>Sono stata all'ospedale il 04/09/2013 e il 05/09/2013 e a dire il vero mi è piaciuto.Mi avete curato benissimo(a dire il vero ho ancora il mal di gola),è di fuori nel corridoio delle scale A,B,C,D,E,F,G(se esistono le scale E ed F)che mi inquieta,mi fa paura.</p>
        
        <p>
            <span style="text-decoration: underline;">Motivo visita: Bambini </span>
            |
            <span style="text-decoration: underline;">Data visita: 01/01/2014 </span>
            |
            <span style="text-decoration: underline;">Feedback: Ecellente </span>
        </p>
        
        <a href="javascript:;" title="Vota per l'utilità di questa recensione"><i class="icon-thumbs-up"></i> La recensione è utile!</a>
        <a href="javascript:;" title="Espandi la recensione per vedere le valutazioni del utente" class="pull-right"><i class="icon-thumbs-up"></i> Di più! <span class="caret"></span></a>
      </div>
</div>
<!-- /feed ITEM -->
{/for}
</div>
<!-- /.container-->

{*}
<div class="container">

  <div class="row">
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-building icon-4x text-info"></i>
      <h4>Hotels</h4>
      <p>Search more than 300'000 hotels around the World</p>
      <p><a class="btn btn-info" href="#">All hotels  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-group icon-4x text-info"></i>
      <h4>Hostels</h4>
      <p>Search more than 300'000 hostels around the World</p>
      <p><a class="btn btn-info" href="#">All hotels  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-ticket icon-4x text-info"></i>
      <h4>Tickets</h4>
      <p>Search more than 300'000 hostels around the World</p>
      <p><a class="btn btn-info" href="#">Buy tickets  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->



    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center">
     <i class="icon-sun icon-4x text-info"></i>
      <h4>Resorts</h4>
      <p>Search more than 300'000 resorts around the World</p>
      <p><a class="btn btn-info" href="#">All resorts  &raquo;</a></p>
    </div><!-- /.col-lg-3 -->



  </div><!-- /.row -->    

  <div class="row">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 text-center">
                    <p class="lead ">
                        <i class="icon-gift text-info"></i> Subscribe to newsletter and get special offers to you email every week!
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 text-center">
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                        </div>
                          <button type="submit" class="btn btn-info">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  
</div><!-- /.container -->
{/*}
    
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl' hide=true}



  
<!-- gMap PLUGIN -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/public/js/jquery.gmap.js"></script>
	
<!-- FLEXSLIDER INIT SCRIPT-->
<!-- Optional FlexSlider Additions -->
<script src="/public/js/jquery.easing.js"></script>
<script src="/public/js/jquery.mousewheel.js"></script>
<script src="/public/js/jquery.flexslider-min.js"></script>
    
<!-- INIT SCRIPT - show gMap onclick -->
<script>
{literal}
          
$(window).load(function(){
    $('.flexslider').flexslider({
        animation: "fade",
        controlNav: "thumbnails",
        start: function(slider){
            $('body').removeClass('loading');
        }
    });
});
    
$(document).ready(function(){

    $('.toggler').click(function(){
    jQuery('.gmappanel').removeClass('hide-map').addClass('show-map').css('height', '600');
    $('.toggler').css('display', 'none');
    $('.toggler-hide').css('display', 'block');

            var $map = $('#map');
            google.maps.event.addDomListener(window, 'resize', function() {
                map.setCenter(homeLatlng);
            });
            if( $map.length ) {

                $map.gMap({
                    address: 'Viale Luigi Settembrini, 2, 47923 Rimini',
                    zoom: 14,
                    //scrollwheel: false,
                    markers: [
                        { 'address' : 'Rimini ospedale',}
                    ]
                });

            }


    });
    $('.toggler-hide').click(function(){
        jQuery('.gmappanel').removeClass('show-map').addClass('hide-map').css('height', '0');
        $('.toggler').css('display', 'block');
        $('.toggler-hide').css('display', 'none');
    });
});
{/literal}
  </script>
  
</body>
</html>
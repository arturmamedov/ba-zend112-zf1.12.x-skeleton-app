{include file='mail/layout/head.tpl'}
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
            {include file='mail/layout/header.tpl'}
                <tr>
                    <td align="center" valign="top">
                        <!-- BEGIN BODY // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                            <tr>
                                <td valign="top" class="bodyContent">
                                    <h1 align="center">{$title}</h1>
                                    {$content}
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="bodyContent featuredContent">
                                    <h3>{$translate->translate("dellei_Your login data")}</h3>
                                    
                                    <p>Mail: {$user->user_mail}</p>
                                    
                                    <p>Password: {$translate->translate("for safety reasons, we don't send it by email")}  {$translate->translate("dellei_(if you forget, you can <a href='%s' target='_blank'>retrieve it</a>)")|sprintf:$fetch_password_url}</p>
                                    
                                    <p>{$translate->translate("dellei_You can login <a href='%s' target='_blank'>here</a>")|sprintf:$login_url}</a></p>
                                </td>
                            </tr>
                            
                            {if $user->user_active == 0}
                            <tr>
                                <td valign="top" class="bodyContent">
                                    <a href="{$site}account/managerregistered?action=confirm&id={$user->getId()}&key={$user->profile->confirm_key}" target="_blank" style="color:#FFFFFF; text-decoration:none;">
                                        <table border="0" cellpadding="0" cellspacing="0" style="background-color:#0073ba; border-radius:5px; text-align:center;" align="center">
                                            <tr>
                                                <td align="center" valign="middle" style="color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; letter-spacing:-.5px; line-height:150%; padding-top:15px; padding-right:30px; padding-bottom:15px; padding-left:30px;">
                                                    {$translate->translate('dellei_Confirm your email')}
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyContent" style="text-decoration:none; text-align:center;">
                                    <a href="{$site}account/managerregistered?action=confirm&id={$user->getId()}&key={$user->profile->confirm_key}" target="_blank" style="text-decoration:none; text-align:center;">{$site}account/managerregistered?action=confirm&id={$user->getId()}&key={$user->profile->confirm_key}</a>
                                    <br>({$translate->translate('dellei_click it or copy and paste in the address bar')})
                                </td>
                            </tr>
                            {/if}
                        </table>
                        <!-- // END BODY -->
                    </td>
                </tr>
                {include file='mail/layout/footer.tpl'}
            </table>
            <!-- // END TEMPLATE -->
        </td>
    </tr>
</table>
</center>
</body>
</html>
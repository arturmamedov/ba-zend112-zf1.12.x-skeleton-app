{include file='mail/layout/head.tpl'}
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
            {include file='mail/layout/header.tpl'}
                <tr>
                    <td align="center" valign="top">
                        <!-- BEGIN BODY // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                            <tr>
                                <td valign="top" class="bodyContent">
                                    <h1 align="center">{$title}</h1>
                                    {$content}
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="bodyContent">
                                    <a href="{$site}{$feedback_url}" target="_blank" style="color:#FFFFFF; text-decoration:none;">
                                        <table border="0" cellpadding="0" cellspacing="0" style="background-color:#0073ba; border-radius:5px;" align="center">
                                            <tr>
                                                <td align="center" valign="middle" style="color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; letter-spacing:-.5px; line-height:150%; padding-top:15px; padding-right:30px; padding-bottom:15px; padding-left:30px;">
                                                    {$translate->translate('Write a review')}
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyContent">
                                    <h2>{$translate->translate('Alone we are nobody, together we are HealthEye!')}</h2>
                                </td>
                            </tr>
                        </table>
                        <!-- // END BODY -->
                    </td>
                </tr>
                {include file='mail/layout/footer.tpl'}
            </table>
            <!-- // END TEMPLATE -->
        </td>
    </tr>
</table>
</center>
</body>
</html>
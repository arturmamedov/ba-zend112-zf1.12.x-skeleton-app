{include file='mail/text_layout/header.tpl'}

** {$title}
------------------------------------------------------------

{$content}

{$site}account/registered?action=confirm&id={$user->getId()}&key={$user->profile->confirm_key}
({$translate->translate('click it or copy and paste in the address bar')})
***

{$translate->translate('Alone we are nobody, together we are HealthEye!')}

{include file='mail/text_layout/footer.tpl'}
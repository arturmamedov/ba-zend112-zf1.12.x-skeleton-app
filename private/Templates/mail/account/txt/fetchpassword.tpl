{include file='mail/text_layout/header.tpl'}

** {$title}
------------------------------------------------------------

{$content}

{$translate->translate('Change your password')}
{$site}account/fetch?action=confirm&id={$user->getId()}&key={$user->profile->new_password_key}

{$translate->translate("Didn't request this change? Let us know immediately.")}

{include file='mail/text_layout/footer.tpl'}
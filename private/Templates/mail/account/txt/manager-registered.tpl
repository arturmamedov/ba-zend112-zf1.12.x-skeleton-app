{include file='mail/text_layout/header.tpl'}

** {$title}
------------------------------------------------------------

{$content}

***
{$translate->translate("dellei_Your login data")}
                                    
Mail: {$user->user_mail}
    
Password: {$translate->translate("for safety reasons, we don't send it by email")}

{$site}account/registered?action=confirm&id={$user->getId()}&key={$user->profile->confirm_key}
({$translate->translate('dellei_click it or copy and paste in the address bar')})
***

{include file='mail/text_layout/footer.tpl'}
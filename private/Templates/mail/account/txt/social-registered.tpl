{include file='mail/text_layout/header.tpl'}

** {$title}
------------------------------------------------------------

{$content}

***
{$translate->translate('Write a review')}
{$site}{$feedback_url}
***

{$translate->translate('Alone we are nobody, together we are HealthEye!')}

{include file='mail/text_layout/footer.tpl'}
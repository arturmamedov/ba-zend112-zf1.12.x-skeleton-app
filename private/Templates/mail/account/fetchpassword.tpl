{include file='mail/layout/head.tpl'}
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
            {include file='mail/layout/header.tpl'}
                <tr>
                    <td align="center" valign="top">
                        <!-- BEGIN BODY // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                            <tr>
                                <td valign="top" class="bodyContent" mc:edit="body_content">
                                    <h1 align="center">{$title}</h1>
                                    {$content}
                                </td>
                            </tr>
                            {if strlen($user->profile->new_password_key) > 0}
                            <tr>
                                <td valign="top" class="bodyContent">
                                    <a href="{$site}account/fetch?action=confirm&id={$user->getId()}&key={$user->profile->new_password_key}" target="_blank" style="color:#FFFFFF; text-decoration:none;">
                                        <table border="0" cellpadding="0" cellspacing="0" style="background-color:#0073ba; border-radius:5px; text-align:center;" align="center">
                                            <tr>
                                                <td align="center" valign="middle" style="color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; letter-spacing:-.5px; line-height:150%; padding-top:15px; padding-right:30px; padding-bottom:15px; padding-left:30px;">
                                                    {$translate->translate('Change your password')}
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyContent" style="text-decoration:none; text-align:center;">
                                    <a href="{$site}account/fetch?action=confirm&id={$user->getId()}&key={$user->profile->new_password_key}" target="_blank" style="text-decoration:none; text-align:center;">{$site}account/fetch?action=confirm&id={$user->getId()}&key={$user->profile->new_password_key}</a>
                                    <br>({$translate->translate('click it or copy and paste in the address bar')})
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyContent" style="text-decoration:none; text-align:center;">
                                    <h3>{$translate->translate("Didn't request this change? <a href='%s'>Let us know immediately.</a>")|sprintf:$letusknow_url}</h3>
                                </td>
                            </tr>
                            {/if}
                        </table>
                        <!-- // END BODY -->
                    </td>
                </tr>
                {include file='mail/layout/footer.tpl'}
            </table>
            <!-- // END TEMPLATE -->
        </td>
    </tr>
</table>
</center>
</body>
</html>
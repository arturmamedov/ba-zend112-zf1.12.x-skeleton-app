{include file='mail/layout/head.tpl'}
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
            {include file='mail/layout/header.tpl'}
                <tr>
                    <td align="center" valign="top">
                        <!-- BEGIN BODY // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                            <tr>
                                <td valign="top" class="bodyContent" mc:edit="body_content">
                                    <h1 align="center">{$title}</h1>
                                    {$content}
                                </td>
                            </tr>
                        </table>
                        <!-- // END BODY -->
                    </td>
                </tr>
                {include file='mail/layout/footer.tpl'}
            </table>
            <!-- // END TEMPLATE -->
        </td>
    </tr>
</table>
</center>
</body>
</html>
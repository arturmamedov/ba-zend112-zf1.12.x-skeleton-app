{include file='layout/head.tpl'}
</head>

<body>
	{include file='layout/navbar.tpl'}
	{include file='layout/breadcrumbs.tpl'}
   
<div class="container">
    <header class="page-header text-center no-margin">
        <h1 class="no-margin">{$title}</h1>
        <p>{$description}</p>
    </header>

    <div class="padding center-block padding border-radius bg-warning margin-top-10">
        {if $identity->user_id == $user->getId()}
            <div class="alert alert-danger">
                <p>
                    Stai inviando l'email a te stesso :)<br>
                    Per inviarla a qualcuno devi selezionare "Invia mail" dalla <a href="/admin/usermanage">gestione utenti</a>, oppure manualmente aggiungere nel indirizzo http:/.../mail/send/id/[id_utente]
                </p>
            </div>
        {/if}

        <form action="" method="POST" class="form-horizontal" role="form">
            <div class="col-sm-7">
                <h4 class="text-center text-muted">A</h4>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-2 control-label" for="to_name">Nome:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="text" class="form-control" value="{$user->profile->first_name} {$user->profile->last_name}"  name="to_name" id="to_name" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-2 control-label" for="to_email">Email:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="text" class="form-control" value="{$user->user_mail}" name="to_email" id="to_email" disabled>
                    </div>
                </div>
                    
                <p class="text-center">
                    <a href="javascript:;" class="btn btn-primary" title="Tutti gli utenti" role="button" data-toggle="modal" data-target=".bs-users-edit">{$translate->translate('Users')} <i class="icon-user"></i></a>
                </p>
            </div>
            
            <div class="col-sm-5">
                <h4 class="text-center text-muted">Da</h4>
                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-2 control-label" for="from_name">Nome:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="text" class="form-control" value="{$from_name}" name="from_name" id="from_name" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-md-3 col-lg-2 control-label" for="from_mail">Email:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="text" class="form-control" value="{$from_mail}" name="from_mail" id="from_mail" disabled>
                    </div>
                </div>
            </div>
               
            <div class="clearfix"></div>
            <hr>
            <div class="clearfix"></div>
            
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="control-label" for="title">Titolo:</label>
                    <div class="">
                        <input type="text" class="form-control" value="{$title_mail|default:''}" name="title" id="title" required>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label class="control-label" for="teaser">Brevissima descrizione:</label>
                    <div class="">
                        <input type="text" class="form-control" value="{$teaser_mail|default:''}" name="teaser" id="teaser" required>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label class="control-label" for="content">Email <small>(non esagerare con le funzioni di stile, le email non leggono tutto)</small>:</label>
                    <textarea class="form-control" name="content" id="content" required>{$content|default:''}</textarea>
                </div>
                    
                <div class="form-group text-center">
                    <input type="submit" name="sendmail" value="{$title}" class="btn btn-success">
                    <input type="submit" name="testmail" value="Invia email di test a ({$identity->user_mail})" class="btn btn-warning">
                </div>
            </div>
        </form>
        
        <div class="clearfix"></div>
    </div>
</div>
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl' ga=false}
<script src="/public/js/manage-plugins.js" type="text/javascript"></script>

<div class="modal fade bs-users-edit" tabindex="-1" role="dialog" id="usersModal" aria-labelledby="usersModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Make sure the path to CKEditor is correct. -->
<script src="/public/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
{literal}
    // Replace the <textarea id="text"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace( 'content', {
		toolbar: [
                { name: 'clipboard', items: [ 'Source','-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                { name: 'editing',     items: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'basicstyles', items: [ 'Bold', 'Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
                { name: 'links', items : [ 'Link','Unlink' ] },
                '/',
                { name: 'styles', items : [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                { name: 'colors', items : [ 'TextColor','BGColor'] },
                { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
            ],
		language: 'it',
		uiColor: '#FFFFFF',
		height: 100,
		resize_dir: 'vertical',
		toolbarCanCollapse: true
	});
{/literal}
</script>

</body>
</html>
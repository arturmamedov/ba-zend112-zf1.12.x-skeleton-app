<tr>
    <td align="center" valign="top">
        <!-- BEGIN PREHEADER // -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
            <tr>
                <td valign="top" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;" mc:edit="preheader_content00">
                    {$teaser}
                </td>
                {if $browser_url}
                <!-- *|IFNOT:ARCHIVE_PAGE|* -->
                <td valign="top" width="180" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:0;" mc:edit="preheader_content01">
                    {$translate->translate('Email not displaying correctly?')}<br /><a href="{$browser_url}" target="_blank">{$translate->translate('View it in your browser')}</a>.
                </td>
                <!-- *|END:IF|* -->
                {/if}
            </tr>
        </table>
        <!-- // END PREHEADER -->
    </td>
</tr>
<tr>
    <td align="center" valign="top">
        <!-- BEGIN HEADER // -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
            <tr>
                <td valign="top" class="headerContent">
                    <a href="{$site}" target="_blank">
                        <img src="http://d3s70323kj2e8m.cloudfront.net/images/for/mail_header.png" style="max-width:600px;" id="headerImage" />
                    </a>
                </td>
            </tr>
        </table>
        <!-- // END HEADER -->
    </td>
</tr>
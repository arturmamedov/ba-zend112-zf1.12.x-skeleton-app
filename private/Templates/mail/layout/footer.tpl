<tr>
    <td align="center" valign="top">
        <!-- BEGIN FOOTER // -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
            <tr>
                <td valign="top" class="footerContent">
                    {if isset($twitter_url)}<a href="{$twitter_url}">{$translate->translate('Follow on Twitter')}</a>&nbsp;&nbsp;&nbsp;{/if}
                    {if isset($facebook_url)}<a href="{$facebook_url}">{$translate->translate('Like on Facebook')}</a>&nbsp;&nbsp;&nbsp;{/if}
                </td>
            </tr>
            <tr>
                <td valign="top" class="footerContent" style="padding-top:0;">
                    <em>Copyright &copy; {$smarty.now|date_format:'%Y'} <a href="{$site}">HealthEye</a>, All rights reserved.</em>
                </td>
            </tr>
            <tr>
                <td valign="top" class="footerContent" style="padding-top:0; padding-bottom:40px;">
                    <p>{$translate->translate('You are receiving this email for notification from HealthEye.it')}</p>
                    <p>{$translate->translate('For info:')} <a href="mailto:{$info_mail}">{$info_mail}</a></p>
                    <p align="right">Via Garibaldi, Rocca d’Arazzo 14030 (AT) Piemonte - Italia.</p>
                </td>
            </tr>
        </table>
        <!-- // END FOOTER -->
    </td>
</tr>
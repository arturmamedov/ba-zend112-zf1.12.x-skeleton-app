{include file='layout/head.tpl'}
<script type="text/javascript">
    {literal}var RecaptchaOptions = {theme:'clean'};{/literal}
</script>
</head>

<body id="signup">
	{include file='layout/navbar.tpl' signup=true}
<div class="container">
        
<div class="row">
    
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 center-block bg-default padding border-radius">
        
        <div class="page-header text-center">
            <h1 class="text-danger">{$title}</h1>
            <p>{$description}</p>
        </div>            
            
        <p class="text-center">
            <a href="{$logout_url}" class="btn btn-lg btn-warning">Logout</a>
        </p>
    </div>
</div><!-- row -->  

	
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}

<script>{literal}
$( document ).ready(function(){
// Ajax mail validator 	
$("#forEmail").focusout(function(){
    var input = $(this), mail = input.val();

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/account/signup',
        data: {mail: mail},
        success: function(json) {
            if(!json.errors.user_mail){
                input.next().html("");
            } else {
                input.next().html('<div class="alert alert-danger">'+json.errors.user_mail+'</div>');
            }
        }
    });
    return false;
}); 
// Ajax valid mail END   
});    
{/literal}</script>
</body>
</html>
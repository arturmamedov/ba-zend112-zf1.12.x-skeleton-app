{include file='layout/head.tpl'}
</head>

<body id="about">
	{include file='layout/navbar.tpl'}
    
<div class="container">
    
    <div class="divider hidden-xs"></div>
    <div class="col-xs-12 bg-warning padding border-radius col-sm-8 col-md-6 col-lg-6 center-block">
        <div class="page-header text-center">
            <h1>{$title} <span class="text-muted"> {$tsn}</span></h1>
            <p>{$description}</p>
        </div>
        
        {include file='lib/error.tpl' error=$errors.confirm|default:''}
        {if $errors|count == 0 || $fp->getErrors()|count > 0}
            {include file='account/libs/changepassword.tpl'}
        {else}
            <p class="lead text-center">
                <a class="text-danger" href="/account/fetch">{$translate->translate('Please try again to retrieve your password')}</a>
            </p>
        {/if}
    </div>
</div>
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}

</body>
</html>
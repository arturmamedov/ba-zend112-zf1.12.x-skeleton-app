<form method="POST" id="userDataForm" action="/account/fetchchange" class="row form-horizontal" role="form" autocomplete="off">
    <input id="user_id" name="user_id" value="{$fp->user->getId()}" type="hidden" />

    <div class="padding">
        <div class="page-header no-margin">
            <h4 class="text-center text-muted">{$translate->translate('Change Password')}  <i class="icon-lock"></i></h4>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 center-block margin-top-10">
            <input name="id" type="hidden" value="{$fp->user->getId()}" />
            <input name="key" type="hidden" value="{$fp->key}" />
            
            <div class="form-group {if $fp->getError('change_password')}has-error{/if}">
                <label class="col-md-4 control-label" for="newPassword">{$translate->translate('New password')}</label>
                <div class="col-md-8">
                    <input id="newPassword" class="form-control" name="change_password" type="password" value="{$fp->password}" placeholder="{$translate->translate('New password')}" required />
                    <span class="help-block">{$translate->translate('The password must be 6 or more characters HAVE a number, a special or a capital')}</span>
                    {include file='lib/error.tpl' error=$fp->getError('change_password')}
                </div>
            </div>

            <div class="form-group {if $fp->getError('password_confirm')}has-error{/if}">
                <label class="col-md-4 control-label" for="formPasswordConfirm">{$translate->translate('Repeat new password')}</label>
                <div class="col-md-8">
                    <input id="formPasswordConfirm" class="form-control" name="password_confirm" type="password" value="{$fp->password_confirm}" placeholder="{$translate->translate('Repeat new password')}" required />
                    {include file='lib/error.tpl' error=$fp->getError('password_confirm')}
                </div>
            </div>
        </div>

    <div class="form-group text-center">
        <input class="btn btn-success btn-lg" type="submit" id="save" value="{$translate->translate('Change Password')}">
    </div>
    </div><!-- bg-white -->
</form>
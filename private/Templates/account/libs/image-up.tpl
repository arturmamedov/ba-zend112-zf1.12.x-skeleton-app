<div class="dropzone bg-white">
    <h4>
        <span class="withTt glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="La foto che compare nelle pagine di ricerca del sito e come immagine princiapale nella pagina degli annunci (max 3mb)"></span>
        Carica immagine
        <span class="withTt" title="La foto che compare nelle pagine di ricerca del sito e come immagine princiapale nella pagina degli annunci (max 3mb)" data-toggle="tooltip" data-placement="right">[?]</span>
    </h4>
    
    <p class="">Trascina l'immagine qui</p>

<!-- The file upload form used as target for the file upload widget -->
    <form class="fileupload" action="/account/biupload" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript>Devi abilitare i Javascript nel Browser per poter caricare immagini (oppure riprova con un altro Borwser es: Chrome, FireFox, Opera)</noscript>

        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="text-center">

                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Carica immagine...</span>
                    <input type="file" name="files[]">
                </span>

                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>

            <!-- The global progress state -->
            <div class="col-xs-12 col-sm-12 col-md-12 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>

        <!-- The table listing the files available for upload/download -->
        <div role="presentation" class="table files">
        {if $user->poster != null}
            <div class="">
                <span class="preview">
                    <a data-gallery="none" download="{$user->poster}" title="Immagine {$user->poster}" href="{$user->poster}">
						<img src="{$user->profile->poster_normal}" class="img-responsive">
					</a>
                </span>
            </div>
            <div class="">
                <button data-url="/account/biupload?file={$user->poster}&rel_id={$user->getId()}&_method=DELETE" data-type="DELETE" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Elimina</span>
                </button>
            </div>
        {/if}
        </div>
    </form>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
    <figure class="thumbnail col-xs-12 col-sm-6 col-md-12 col-lg-12 text-center">
        <a title="{$translate->translate('Your profile page!')}" href="/u/{$identity->identity}">
            <img class="img-thumbnail img-responsive center-block" src="{$user->profile->poster_normal|default:$user->poster_default}" alt="{$translate->translate('Profile Photo')} {$user->profile->first_name} {$user->profile->last_name}" />
        </a>
        
        <div class="divider clearfix"></div>
        <h3 class="no-margin"><a href="/user/{$user->identity}" title="{$translate->translate('Your profile page!')}">{$user->profile->first_name} {$user->profile->last_name}</a></h3>
        <p><a href="/user/{$user->identity}" class="text-muted" title="{$translate->translate('Your profile page!')}"><small>http://healtheye.it/user/{$user->identity}</small></a></p>
    </figure>

    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12 bg-info padding-tb margin-top-10">
        <ul class="nav nav-pills nav-stacked">
            <h4 class="text-center text-black">Menu</h4>
            <hr>
            <li {if $page == 'account'}class="active"{/if}>
                <a href="/account/index">Account</a>
            </li>
            <li {if $page == 'address'}class="active"{/if}>
                <a href="/account/address">{$translate->translate('Address')}</a>
            </li>
            <li {if $page == 'password'}class="active"{/if}>
                <a href="/account/password">{$translate->translate('Change Password')}</a>
            </li>
            {*<li>
                <a href="/account/notifications">{$translate->translate('Notifications Settings')}</a>
            </li>*}
        </ul>
    </div> {*bg-info*}
</div>
<form action="javascript:;" id="addmailForm" role="form" class="form-inline text-center">
    <div class="form-group">
        <label class="withTt" for="user_mail" title="{$translate->translate('The E-mail is used to access the site and therefore can not be miss')}" data-toggle="tooltip" data-placement="right">E-mail</label>
        <input id="user_mail" class="form-control" name="user_mail" value="" type="email" required />
    </div>        
    <input class="btn btn-success" type="submit" id="save" value="{$translate->translate('Save')}">
    <span class="help-block bg-danger padding border-radius">
        {$translate->translate('You still have to add your email.')}
        <i class="icon-question-sign pull-right withTt" data-toggle="tooltip" data-placement="left" title="{$translate->translate("We're respect your privacy and will not rent or sell your e-mail address. Its only for send you notifications.")}"></i>
    </span>
</form>
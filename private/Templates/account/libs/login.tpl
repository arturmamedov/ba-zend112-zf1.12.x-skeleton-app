<form action="/account/login" method="POST" class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 control-label" for="username">Email</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" id="username" class="form-control" name="user_name_mail" value="{$user->user_mail|default:''}" placeholder="email@mail.com" required />
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-12 col-sm-4  control-label" for="password">Password</label>
        <div class="col-xs-12 col-sm-8">
        <input type="password" id="password"  class="form-control" name="password"  placeholder="password" required /><br />
        </div>	
    </div>

    <div class="form-group">
        <div class="text-center">
            <input type="submit" class="btn btn-primary btn-lg" value="{$translate->translate("Login")}">
        </div>
    </div>
</form>
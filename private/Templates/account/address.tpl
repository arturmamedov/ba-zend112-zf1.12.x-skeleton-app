{include file='layout/head.tpl'}
</head>

<body id="settings">
	{include file='layout/navbar.tpl'}
    
<div class="container">
    {if $fp->getErrors()|count > 0}
        {include file='lib/pop_error.tpl' error=$fp->getErrors()}
    {/if}

    <div class="row">        
        {include file='account/libs/menu.tpl'}
			
{*} Address {/*}
		<form method="POST" id="userDataForm" action="/account/address" class="col-xs-12 col-sm-12 col-md-7 col-lg-7 form-horizontal" role="form">
            <input id="user_id" name="user_id" value="{$fp->user->getId()}" type="hidden" />
            <div class="bg-info padding">
                <div class="page-header no-margin">
                    <h4 class="text-center text-muted">{$translate->translate('Address Settings')}  <span class="glyphicon glyphicon-map-marker"></span></h4>
                    <p class="text-center"><small>{$translate->translate('The address is not mandatory, therefore saving with empty fields will eliminate it with no errors')}</small></p>
                </div>
			
                <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 center-block">
                    <div class="form-group margin-top-10 {if $fp->getError('country')}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="select_country">Stato</label>

                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <select id="country_id" class="form-control" disabled="" name="select_country">
                                <option value="105" selected>{$translate->translate('Italy')}</option>
                                {foreach from=$countries item=country}
                                    <option value="{$country->getId()}">{$translate->translate($country->name)}</option>
                                {/foreach}
                            </select>
                            {include file='lib/error.tpl' error=$fp->getError('country')}
                        </div>
                    </div>
                        
                    <div class="form-group {if $fp->getError("region_id")}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="select_region">Regione</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <select name="region_id" id="select_region" class="form-control" >
                                {if $fp->region_id > 0}
                                    <option value="{$regions[$fp->region_id]->getId()}" selected>{$regions[$fp->region_id]->name}</option>
                                {/if}
                                <option value="0">{$translate->translate('Select region')}</option>
                                {foreach from=$regions item=region}
                                    <option value="{$region->getId()}">{$region->name}</option>
                                {/foreach}
                            </select>
                        </div>
                        {if $fp->getError('region_id')}<span class="help-block">{include file='lib/error.tpl' error=$fp->getError('state')}</span>{/if}
                    </div>
                    
                    <div class="form-group {if $fp->getError("province_id")}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 control-label col-lg-3" for="select_province">{$translate->translate('Province')}</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <select name="province_id" id="select_province" class="form-control" >
                                {if $fp->province_id > 0}
                                    <option value="{$provinces[$fp->province_id]->getId()}" selected>{$provinces[$fp->province_id]->name} - ({$provinces[$fp->province_id]->code_2})</option>
                                {/if}
                                <option value="0">{$translate->translate('Select region before')}</option>
                            </select>
                            <span class="help-block">{include file="lib/error.tpl" error=$fp->getError("province_id")}</span>
                        </div>
                    </div>
                
                    <div class="form-group {if $fp->getError("city_id")}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 control-label col-lg-3" for="select_city">{$translate->translate('City')}</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <select name="city_id" id="select_city" class="form-control" >
                                {if $fp->city_id > 0}
                                    <option value="{$cities[$fp->city_id]->getId()}" selected>{$cities[$fp->city_id]->name}</option>
                                {/if}
                                <option value="0">{$translate->translate('Select province before')}</option>
                            </select>
                            <span class="help-block">{include file="lib/error.tpl" error=$fp->getError("city_id")}</span>
                        </div>
                    </div>
                    
                    <div class="form-group {if $fp->getError("address")}has-error{/if} margin-top-10">
                        <label class="col-xs-12 col-sm-3 col-md-3 control-label col-lg-3" for="address">{$translate->translate('Address')}, {$translate->translate('Address_num')}</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <input id="address" class="form-control" name="address" value="{$fp->address|escape}" type="text" placeholder="{$translate->translate('Address_example')}" /><span class="help-block">{include file="lib/error.tpl" error=$fp->getError("address")}</span>
                        </div>
                    </div>
                    
                    <div class="form-group {if $fp->getError('zip_code')}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="zip_code" >CAP</label>

                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <input type="text" name="zip_code" class="form-control" id="zip_code" value="{$fp->zip_code}">
                            <span class="help-block">{include file='lib/error.tpl' error=$fp->getError('zip_code')}</span>
                        </div>
                    </div>

                    <div class="form-group text-center">
                            <input class="btn btn-success btn-lg" type="submit" id="save" value="{$translate->translate('Save')}">
                    </div>
                    <div class="clearfix"></div>
                </div><!-- center block -->
            </div><!-- bg-white -->
        </form>
    </div><!-- row -->
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}
</body>
</html>
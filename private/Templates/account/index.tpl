{include file='layout/head.tpl'}
</head>

<body id="settings">
	{include file='layout/navbar.tpl'}
    
<div class="container">
    {if $fp->getErrors()|count > 0}
        {include file='lib/pop_error.tpl' error=$fp->getErrors()}
    {/if}

    <div class="row">

        <h1 class="text-center">
            <a class="text-user" href="/u/{$identity->identity}" title="{$translate->translate('Your profile page!')}">{$title}</a>
        </h1>

        {include file='account/libs/menu.tpl'}

        <form method="POST" id="userDataForm" action="/account/index" class="col-xs-12 col-sm-12 col-md-7 col-lg-7 bg-info form-horizontal" role="form">
            <input id="user_id" name="user_id" value="{$fp->user->getId()}" type="hidden" />
            <div class="padding">

                <div class="page-header no-margin">
                    <h4 class="text-center text-muted">{$translate->translate('Account Settings')}  <span class="withTt glyphicon glyphicon-cog" data-toggle="tooltip" data-placement="top" title="{$translate->translate('General settings of your account')}"></span></h4>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 center-block">
                    
                    {*<div class="form-group margin-top-10 {if $fp->getError('identity')}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="puIdentity">Username</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <input id="puIdentity" class="form-control" name="identity" required value="{$fp->identity|escape}" type="text" placeholder="Username e Vanity Url" />
                            {include file='lib/error.tpl' error=$fp->getError('identity')}
                        </div>
                    </div>*}

                    
                    {if strlen($user->user_mail) > 0}
                        <div class="form-group margin-top-10 {if $fp->getError('user_mail')}has-error{/if}">
                            <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label withTt" for="user_mail" title="{$translate->translate('The E-mail is used to access the site and therefore can not be miss')}" data-toggle="tooltip" data-placement="right">E-mail</label>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                <input id="user_mail" class="form-control" name="user_mail" disabled value="{$fp->user_mail|escape}" type="email" />
                                {include file='lib/error.tpl' error=$fp->getError('user_mail')}
                                {if $user->user_active == 0}
                                    <span class="help-block bg-warning padding border-radius">
                                        {$translate->translate('You still have to confirm your email.')} 
                                        <a href="/account/reconfirm?action=firstmail">{$translate->translate('Resend the confirmation email')}</a>
                                    </span>
                                {/if}
                            </div>
                        </div>
                    {else}
                        <div class="form-group margin-top-10 {if $fp->getError('user_mail')}has-error{/if}">
                            <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label withTt" for="user_mail" title="{$translate->translate('The E-mail is used to access the site and therefore can not be miss')}" data-toggle="tooltip" data-placement="right">E-mail</label>
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                <input id="user_mail" class="form-control" name="user_mail" value="{$fp->user_mail|escape}" type="email" required />
                                {include file='lib/error.tpl' error=$fp->getError('user_mail')}
                                <span class="help-block bg-danger padding border-radius">
                                    {$translate->translate('You still have to add your email.')}
                                    <i class="icon-question-sign pull-right withTt" data-toggle="tooltip" data-placement="left" title="{$translate->translate("We're respect your privacy and will not rent or sell your e-mail address. Its only for send you notifications.")}"></i>
                                </span>
                            </div>
                        </div>
                    {/if}

                    <div class="form-group {if $fp->getError('first_name')}has-error{/if}">            
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="firstName">{$translate->translate('Name')}</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <input id="firstName" class="form-control" name="first_name" required value="{$fp->first_name|escape}" type="text" placeholder="{$translate->translate('Name')}" />
                            {include file='lib/error.tpl' error=$fp->getError('first_name')}
                        </div>
                    </div>

                    <div class="form-group {if $fp->getError('last_name')}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="lastName">{$translate->translate('Surname')}</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <input id="lastName" class="form-control" name="last_name" required value="{$fp->last_name|escape}" type="text" placeholder="{$translate->translate('Surname')}" />
                            {include file='lib/error.tpl' error=$fp->getError('last_name')}
                        </div>
                    </div>

                    <div class="form-group {if $fp->getError('birthday')}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="birthday">{$translate->translate('Birthday')}</label>

                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            {html_select_date 
                                prefix='birthday' end_year=1900 reverse_years=true 
                                time=$fp->birthday_ts
                                year_empty="Selezionare o digitare anno"
                                day_empty="Selezionare o digitare giorno"
                                month_empty="Selezionare o digitare mese"
                                month_format="%B"
                                all_extra="class='form-control pull-left' style='width: 30%; margin-right:1.3333%;'" 
                            }
                            <span class="help-block">{include file='lib/error.tpl' error=$fp->getError('birthday')}</span>
                        </div>
                    </div>

                    <div class="form-group {if $fp->getError('description')}has-error{/if}">
                        <label class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label" for="description">{$translate->translate('About you')}</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <textarea id="description"  class="form-control" placeholder="..." name="description" rows="5">{$fp->description|escape}</textarea>
                            {include file='lib/error.tpl' error=$fp->getError('description')}
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <input class="btn btn-success btn-lg" type="submit" id="save" value="{$translate->translate('Save')}">
                    </div>
                    <div class="clearfix"></div>
                    
                    {*
                    <div class="page-header">
                        <h2>Social Account</h2>
                    </div>
                    
                    {if $user->profile->gp_connected == 1}
                        <p>Sei connesso con Google <a href="/account/revokegp">Disconnetti</a></p>
                    {/if}
                    
                    {if $user->profile->fb_connected == 1}
                        <p>Sei connesso con Facebook <a href="#">Disconnetti</a></p>
                    {/if}
                    *}
                </div>
            </div><!-- bg-white -->
        </form>
    </div><!-- row -->
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}


{*}		
<script>
	$('.fileupload').on('fileuploadsubmit', function (e, data) {
		 data.formData = {rel_id: $("#user_id").val(), rel_type: 'user'};
	});
</script>



<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/public/js/blueimpup/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/public/js/blueimpup/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-ui.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
	<script src="/public/js/blueimpup/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
			
			
{literal}
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <a class="preview"></a>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <a class="add preview" href="file.url">
        {% if (file.url) { %}
            <img class="img-responsive add" src="{%=file.url%}">
        {% } %}
    </a>
    <div class="clearfix"></div>
    {% if (file.error) { %}
    <div class="error"><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}

    {% if (file.deleteUrl) { %}
    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
        <i class="glyphicon glyphicon-trash"></i>
        <span>Elimina</span>
    </button>
    {% } %}
{% } %}
</script>
{include file='widget/bi_gallery.tpl'}
{/*}
</body>
</html>
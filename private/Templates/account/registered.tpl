{include file='layout/head.tpl'}

<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="/public/js/blueimpup/css/jquery.fileupload.css">
<link rel="stylesheet" href="/public/js/blueimpup/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="/public/js/blueimpup/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="/public/js/blueimpup/css/jquery.fileupload-ui-noscript.css"></noscript>
</head>

<body id="account">
	{include file='layout/navbar.tpl'}
<div class="container">
	
	{include file='lib/pop_message.tpl'}
<div class="row">
    <div class="col-sm-6 center-block padding bg-info">
    
        <div class="page-header no-margin text-center">
            <h1 class="">{$title}</h1>
        </div>

    
        {*}{include file='account/libs/menu.tpl'}{/*}
        <div class="center-block">
            {if $action == 'sendMail'}
                <p>Grazie {$user->profile->first_name}, per aver dimostrato interesse.</p>
                <p>Per concludere la registrazione <strong>devi attivare il tuo account cliccando il link di conferma che ti è stato spedito sulla tua eMail</strong>.</p>	
                <p>Dopo di che potrai accedere con eMail e password appena forniti.</p>
            {/if}

            {if $action == 'confirm'}
                {if $errors|@count == 0}
                    <p>Hai confermato la tua eMail ed il tuo account è attivo! Puoi ora eseguire il <a href="/account/login"><strong>Login</strong></a> con le credenziali che hai fornito.</p>
                    
                    <div class="well well-sm">
                        <p>Pagina del tuo profilo: <a href="/user/{$identity->identity}" title="Vai al tuo Profilo!">{$site}u/{$identity->identity}</a></p>
                        <p>Pagina del gestionale: <a href="/user/{$identity->identity}" title="Vai al tuo Profilo!">{$site}dashboard</a></p>
                        <p>Pagina impostazioni account: <a href="/user/{$identity->identity}" title="Vai al tuo Profilo!">{$site}account/index</a></p>
                    </div>
                    
                    <p class="pull-right"><strong>{$user->profile->first_name}, grazie ancora!</strong></p>
                {else}
                    <p class="alert alert-danger">{$errors['confirm']|default: ''}</p>
                    <br>
                    <p>Se non è cosi, per favore <a href="/support/new?tag=registrazione&title=Non rieso a confermare la mail" title="Scrivi ora">scrivi nel supporto descrivendo i tuoi passi</a>. Grazie per la comprensione.</p>
                {/if}
            {/if}
        </div>
        
        <div class="clearfix" style="height:50px;"></div>
        
        {if $authenticated}
        <div class="center-block">
            <h2 class="center-text">Immagine di profilo :)</h2>
            {include file='account/libs/image-up.tpl'}
        </div>
        {/if}
    
		<div class="clearfix" style="height:50px;"></div>
    </div><!-- col-sm-6 -->
</div> <!-- row -->
    
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}




{literal}		
<script>
	$('.fileupload').on('fileuploadsubmit', function (e, data) {
		 data.formData = {rel_id: $("#user_id").val(), rel_type: 'user'};
	});
</script>
{/literal}


<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/public/js/blueimpup/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/public/js/blueimpup/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/public/js/blueimpup/js/jquery.fileupload-ui.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
	<script src="/public/js/blueimpup/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
			
			
{literal}
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <a class="preview"></a>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <a class="add preview" href="file.url">
        {% if (file.url) { %}
            <img class="img-responsive add" src="{%=file.url%}">
        {% } %}
    </a>
    <div class="clearfix"></div>
    {% if (file.error) { %}
    <div class="error"><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}

    {% if (file.deleteUrl) { %}
    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
        <i class="glyphicon glyphicon-trash"></i>
        <span>Elimina</span>
    </button>
    {% } %}
{% } %}
</script>
{/literal}
{if $authenticated}
{include file='widget/bi_gallery.tpl'}
{/if}
</body>
</html>
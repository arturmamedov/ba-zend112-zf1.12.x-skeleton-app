{include file='layout/head.tpl'}
</head>

<body id="about">
	{include file='layout/navbar.tpl'}
    
<div class="container">
    
    <div class="divider hidden-xs"></div>
    <div class="col-xs-12 bg-info padding col-sm-8 col-md-6 col-lg-6 center-block">
        <div class="page-header text-center">
            <h1>{$title} <span class="text-muted"> {$tsn}</span></h1>
            <p>{$description}</p>
        </div>
        
        {if $action == 'confirm'}
            {include file='lib/error.tpl' error=$errors.confirm|default:''}
            {if $errors|count == 0 || $fp->getErrors()|count > 0}
                {include file='account/libs/changepassword.tpl'}
            {else}
                <p class="lead text-center">
                    <a class="text-danger" href="/account/fetch">{$translate->translate('Please try again to retrieve your password')}</a>
                </p>
            {/if}
        {elseif $action == 'know'}
            <h3 class="text-center">{$translate->translate("Thanks for letting us know")}</h3>
            <p class="text-center">{$translate->translate("You can log into your account with your current password, and you don't need to do anything else.")}</p>
            {include file='account/libs/login.tpl'}
        {elseif $action == 'complete'}
            <p class="lead text-success text-center">{$translate->translate('We have sent you an email with instructions on how to recover password. Check your e-mail and follow the instructions, see you soon!')}</p>
        {else}
            <form method="post" action="/account/fetch" role="form">
                <div class="form-group">
                    <label class="control-label lead withTt" data-toggle="tooltip" data-placement="right" for="user_mail" title="{$translate->translate('Email that you used during registration')}">Email</label>
                    <div>
                        <input class="form-control input-lg" id="user_mail" autofocus="autofocus" name="user_mail" required="required" value="" type="email" placeholder="{$translate->translate('Email that you used during registration')}" />
                        {include file='lib/error.tpl' error=$errors.user_mail|default:''}
                    </div>
                </div>

                <div class="form-group text-center">
                    <input class="btn btn-primary btn-lg" type="submit" name="send" value="{$translate->translate('Recover Password')}" />
                </div>

                <p class="text-right">
                    {$translate->translate('OR')}
                    <a href="/account/login" class="text-muted"> {$translate->translate('Login')}</a>
                    {$translate->translate('or')}
                    <a href="/account/signup" class="text-muted">{$translate->translate('Sign Up')}</a>
                </p>
                <div class="clearfix"></div>
            </form>
        {/if}
    </div>
</div>
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}

</body>
</html>
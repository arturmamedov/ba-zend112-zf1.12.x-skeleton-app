     {include file='layout/head.tpl'}
</head>

<body id="connect">
	{include file='layout/navbar.tpl'}
    
<div class="container">

<div class="row">
    
    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 center-block bg-acqua padding border-radius">
        
        <div class="page-header text-center">
            <img src="/public/img/site/icons/twitter-logo.png" alt="Twitter Login" class="center-block col-xs-1">
            <h1 class="">{$title}<span class="text-muted"> - HealthEye</span></h1>
        </div>
            
        <div class="clearfix"></div>
            
        <form class="form-horizontal col-xs-12 col-sm-12 col-md-10 col-lg-10 center-block" method="POST" action="/account/connecttw" id="regForm" autocomplete="on" role="form">
            <div class="col-xs-12">
                <figure class="col-xs-4">
                    <img src="{$fp->poster}" class="img-circle pull-right margin-top-10 user-75 img-responsive">
                </figure>
                <div class="col-xs-8">
                    <h3 class="no-margin">@{$fp->screen_name}</h3>
                    <p>{$fp->description}</p>
                </div>
            </div>
                
            <div class="clearfix"></div>
            <div class="divider"></div>

            <h4 class="text-center">{$translate->translate('One last step for complete, <strong>fill in the missing fields</strong>')}</h4>
            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="firstName">{$translate->translate('Name')}</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="firstName" autofocus="autofocus" name="first_name" required="required" value="{$fp->first_name|escape}" type="text" placeholder="Nome" />
                    {include file='lib/error.tpl' error=$fp->getError('first_name')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="last_name">{$translate->translate('Surname')}</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="lastName" name="last_name" required="required" value="{$fp->last_name|escape}" type="text" placeholder="Cognome" />
                    {include file='lib/error.tpl' error=$fp->getError('last_name')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="forEmail">Email</label>
                <div class="col-xs-12 col-sm-8">
                    <input type="email" class="form-control" id="forEmail" name="user_mail" required="required" value="{$fp->user_mail|escape}" placeholder="{$translate->translate('Confirmation e-mail')}" />
                    <div class="help-block"></div>
                    {include file='lib/error.tpl' error=$fp->getError('user_mail')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="password">Password</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="password" name="password" required="required" type="password" value="{$fp->password|escape}" placeholder="Password" autocomplete="false">
                    <span class="help-block">{$translate->translate('The password must be 6 or more characters HAVE a number, a special or a capital')}</span>
                    {include file='lib/error.tpl' error=$fp->getError('password')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="formPasswordConfirm">{$translate->translate('Confirm your password')}</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="formPasswordConfirm" name="password_confirm" required="required" type="password" value="{$fp->password_confirm|escape}" placeholder="{$translate->translate('Repeat password')}" autocomplete="false" />
                    {include file='lib/error.tpl' error=$fp->getError('password_confirm')}
                </div>
            </div>

            <div class="clearfix"></div>

            {if $fp->vsocial == 'Twitter'}
                <p class="text-center">{$translate->translate('Follow on Twitter')} <input type="checkbox" id="tw_autopost" name="follow_us" checked /></p>
            {/if}

            <div class="col-xs-12 col-sm-12 text-center margin-top-10">
                <input class="btn btn-lg btn-success" type="submit" value="{$translate->translate('Sign Up')}"/>
                <a href="/account/logout" class="btnt btn-link">{$translate->translate('Cancel')}</a>
                <p class="margin-top-10"><small>{$translate->translate('By clicking SIGN UP you accept the')} <a href="/index/terms" target="_blank"> {$translate->translate('Terms and Conditions')}</a></small></p>
            </div>

            <div class="clearfix"></div>
        </form>
    </div>
</div>

{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}

</body>
</html>
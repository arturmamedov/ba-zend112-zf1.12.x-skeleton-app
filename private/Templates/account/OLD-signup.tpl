{include file='layout/head.tpl'}
<script type="text/javascript">
    {literal}var RecaptchaOptions = {theme:'clean'};{/literal}
</script>
</head>

<body id="signup">
	{include file='layout/navbar.tpl'}
<div class="container">
	
	{include file='lib/pop_message.tpl'}
        
<div class="row">
    
    <div class="col-sm-8 center-block bg-white padding border-radius">
        
        <div class="page-header text-center">
            <h1 class="">{$title}<small>{$tsn}</small></h1>
        </div>
        
    <form class="form-horizontal" method="POST" action="/account/signup" id="regForm" autocomplete="on" role="form">
        {*}
        <div class="">
            <a href="/account/connect?conn=fb" class="" title="">Facebook</a>
            <a href="/account/connect?conn=vk">VK</a>
            <a href="/account/connect?conn=tw">Twitter</a>
            <a href="/account/connect?conn=in">Linkedin</a>
            <a href="/account/connect?conn=gp">Google</a>
        </div>
        {/*}
        
        <div class="form-group">
            <label class="col-sm-4 control-label" for="firstName">Nome</label>
            <div class="col-sm-8">
                <input class="form-control" id="firstName" autofocus="autofocus" name="first_name" required="required" value="{$fp->first_name|escape}" type="text" placeholder="Nome (persona/agenzia)" />
                {include file='lib/error.tpl' error=$fp->getError('first_name')}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="last_name">Cognome</label>
            <div class="col-sm-8">
                <input class="form-control" id="lastName" name="last_name" required="required" value="{$fp->last_name|escape}" type="text" placeholder="Cognome (persona)" />
                {include file='lib/error.tpl' error=$fp->getError('last_name')}
            </div>
        </div>
        {*}
        <div class="form-group">
            <label class="col-sm-4 control-label" for="username">Username</label>
            <div class="col-sm-8">
                <input tabindex="1" id="firstName" autofocus="autofocus" name="username" required="required" value="{$fp->first_name|escape}" type="text" placeholder="Nome utente sul sito" />
                {include file='lib/error.tpl' error=$fp->getError('username')}
            </div>
        </div>
        {/*}    
        <div class="form-group">
            <label class="col-sm-4 control-label" for="formEmail">Mail</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" id="formEmail" name="user_mail" required="required" value="{$fp->user_mail|escape}" placeholder="Mail per confermare identità" />
                {include file='lib/error.tpl' error=$fp->getError('user_mail')}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="password">Password</label>
            <div class="col-sm-8">
                <input class="form-control" id="password" name="password" required="required" type="password" value="{$fp->password|escape}" placeholder="Password">
                <span class="help-block">La password deve contenere 6 o piú caratteri, AVERE un numero, un simbolo speciale o una maiuscola</span>
                {include file='lib/error.tpl' error=$fp->getError('password')}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="formPasswordConfirm">Conferma password</label>
            <div class="col-sm-8">
                <input class="form-control" id="formPasswordConfirm" name="password_confirm" required="required" type="password" value="{$fp->password_confirm|escape}" placeholder="Ripeti la password" />
                {include file='lib/error.tpl' error=$fp->getError('password_confirm')}
            </div>
        </div>
        
        <div class="form-group text-center">
            <label class="col-sm-4 control-label">Codice di sicurezza</label>
            <div class="col-sm-8">{$recaptcha}
                {include file='lib/error.tpl' error=$fp->getError('captcha')}
            </div>
        </div>
        
        <div class="col-sm-12 text-center">
            <input class="btn btn-lg btn-success" type="submit" value="Registrati"/>
            <p><small>Cliccando REGISTRATI si accettano i <a href="/index/terms" target="_blank"> Termini e Condizioni d'Uso</a></small></p>
        </div>
        
        
        <a class="pull-right btn btn-default" href="/account/login" title="Hai già un account? Esegui il login :)">Login</a>
        
        <div class="clearfix"></div>
    </form>
    </div>
</div><!-- row -->  

	
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl' hide=true}

</body>
</html>
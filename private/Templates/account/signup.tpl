{include file='layout/head.tpl'}
<script type="text/javascript">
    {literal}var RecaptchaOptions = {theme:'clean'};{/literal}
</script>
</head>

<body id="signup">
	{include file='layout/navbar.tpl' signup=true}
<div class="container">
        
<div class="row">
    <div class="col-sm-10 col-md-8 col-lg-8 bg-info padding">        
        <div class="page-header text-center">
            <h1 class="">{$title}<span class="text-muted"> {$tsn}</span></h1>
            
            <h4 class="text-black">{$translate->translate('Login')} {$translate->translate('via your favorite social network')}</h4>
            <p>
                <a href="#/account/connectfb" title="{$translate->translate('Sign Up')} {$translate->translate('or')} {$translate->translate('Login')} {$translate->translate('with')} Facebook" class="withTt btn btn-primary" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-facebook text-white icon-large"></i> Facebook</a>
                <a href="#/account/connectgp" title="{$translate->translate('Sign Up')} {$translate->translate('or')} {$translate->translate('Login')} {$translate->translate('with')} Google" class="withTt btn btn-danger" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-google-plus text-white icon-large"></i> Google+</a>
            </p>
        </div>
        
    
        <form class="form-horizontal col-xs-12 col-sm-12 col-md-10 col-lg-10 center-block" method="POST" action="/account/signup" id="regForm" autocomplete="on" role="form">
            <h4 class="text-black text-center">{$translate->translate('Or fill out the form below')}</h4>
            <div class="clearfix"></div>
            
            <div class="form-group margin-top-10">
                <label class="col-xs-12 col-sm-4 control-label" for="firstName">{$translate->translate('Name')}</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="firstName" autofocus="autofocus" name="first_name" required="required" value="{$fp->first_name|escape}" type="text" placeholder="Nome" />
                    {include file='lib/error.tpl' error=$fp->getError('first_name')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="last_name">{$translate->translate('Surname')}</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="lastName" name="last_name" required="required" value="{$fp->last_name|escape}" type="text" placeholder="{$translate->translate('Surname')}" />
                    {include file='lib/error.tpl' error=$fp->getError('last_name')}
                </div>
            </div>
            {*}<!--
            <div class="form-group">
                <label class="col-sm-4 control-label" for="username">Username</label>
                <div class="col-sm-8">
                    <input tabindex="1" id="firstName" autofocus="autofocus" name="username" required="required" value="{$fp->first_name|escape}" type="text" placeholder="Nome utente sul sito" />
                    {include file='lib/error.tpl' error=$fp->getError('username')}
                </div>
            </div>
           -->{/*}    
            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="forEmail">Email</label>
                <div class="col-xs-12 col-sm-8">
                    <input type="email" class="form-control" id="forEmail" name="user_mail" required="required" value="{$fp->user_mail|escape}" placeholder="{$translate->translate('Confirmation e-mail')}" />
                    <div class="help-block"></div>
                    {include file='lib/error.tpl' error=$fp->getError('user_mail')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="password">Password</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="password" name="password" required="required" type="password" value="{$fp->password|escape}" placeholder="Password" autocomplete="false">
                    <span class="help-block">{$translate->translate('The password must be 6 or more characters HAVE a number, a special or a capital')}</span>
                    {include file='lib/error.tpl' error=$fp->getError('password')}
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label" for="formPasswordConfirm">{$translate->translate('Confirm your password')}</label>
                <div class="col-xs-12 col-sm-8">
                    <input class="form-control" id="formPasswordConfirm" name="password_confirm" required="required" type="password" value="{$fp->password_confirm|escape}" placeholder="{$translate->translate('Repeat password')}" autocomplete="false" />
                    {include file='lib/error.tpl' error=$fp->getError('password_confirm')}
                </div>
            </div>

            <div class="form-group text-center">
                <label class="col-xs-12 col-sm-4 control-label">{$translate->translate('Security code')}</label>
                <div class="col-xs-12 col-sm-8">{$recaptcha}
                    {include file='lib/error.tpl' error=$fp->getError('captcha')}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 text-center margin-top-10">
                <input class="btn btn-lg btn-success" type="submit" value="{$translate->translate('Sign Up')}"/>
                <p class="margin-top-10"><small>{$translate->translate('By clicking SIGN UP you accept the')} <a href="/index/terms" target="_blank"> {$translate->translate('Terms and Conditions')}</a></small></p>
            </div>
            
            <div class="clearfix"></div>
        </form>
        <a class="pull-right text-black" href="/account/login">{$translate->translate('Already a member? Log in')}</a>
        <div class="clearfix"></div>
    </div>
	
	<div class="hidden-sm hidden-xs col-md-4 col-lg-4 padding">
		<div class="col-xs-12 bg-info padding">	
		
			<h1 class="text-right">Registrarsi e semplice</h1>
			<ul>
				<li>Registrazione Gratuita</li>
				<li>Pannello per gestire le transizioni</li>
				<li>Altro ancora</li>
			</ul>
		
		</div>
	</div>
	</div><!-- row -->  

	
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}

</body>
</html>
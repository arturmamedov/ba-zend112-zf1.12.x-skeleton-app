{include file='layout/head.tpl'}
</head>
<body id="login">
    {include file='layout/navbar.tpl' login=true}
<div class="container">
        
    <div class="divider-100 hidden-xs"></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 center-block bg-info padding border-radius">
			
            <h1 class="text-center">{$title}<span class="text-muted"> {$tsn}</span></h1>
            
            <p class="text-center lead">{$translate->translate('via your favorite social network')}</p>
            
            <p class="text-center">
                <a href="/account/connectfb" title="{$translate->translate('Sign Up')} {$translate->translate('or')} {$translate->translate('Login')} {$translate->translate('with')} Facebook" class="withTt btn btn-primary" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-facebook text-white icon-large"></i> Facebook</a>
                <a href="/account/connectgp" title="{$translate->translate('Sign Up')} {$translate->translate('or')} {$translate->translate('Login')} {$translate->translate('with')} Google" class="withTt btn btn-danger" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-google-plus text-white icon-large"></i> Google+</a>
            </p>
            
            <hr>
            {if $errors|count > 0}{include file='lib/error.tpl' error=$errors.general}{/if}
				
            <p class="text-center lead">{$translate->translate('with your credentials')}</p>
            {include file='account/libs/login.tpl'}

            <a href="/account/fetch" class="pull-right text-muted">{$translate->translate('Forgot your password?')}</a>
            <a href="/account/signup" class="pull-left text-info">{$translate->translate('Sign Up')}</a>
            
            <div class="clearfix"></div>
		</div> <!-- sm 5 -->
</div> <!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}
</body>
</html>
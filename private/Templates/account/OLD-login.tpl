{include file='layout/head.tpl'}
</head>
<body id="login">
    {include file='layout/navbar.tpl'}
<div class="container">
	<div class="row">
        
		<div class="col-sm-6 center-block bg-white padding border-radius">
			
			<h1 class="text-center">Login</h1>
			<p class="text-center">Esegui il login per accedere</p>
				
			{if $errors|count > 0}{include file='lib/error.tpl' error=$errors.general}{/if}
				
			<form action="/account/login" method="POST" class="form-horizontal" role="form">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="username">Username</label>
					<div class="col-sm-9">
						<input type="text" id="username" class="form-control" name="user_name_mail" value="{$user_name_mail|default:''}" autofocus="autofocus" placeholder="username / @email" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3  control-label" for="password">Password</label>
					<div class="col-sm-9">
						<input type="password" id="password"  class="form-control" name="password"  placeholder="password"/><br />
					</div>	
				</div>
				
				<div class="form-group">
					<div class="text-center">
						<input type="submit" class="btn btn-primary btn-lg" value="Log in">
					</div>
				</div>
			</form>

			<a href="/account/fetch" class="pull-right text-muted">Recupero password</a>
			<a href="/account/signup" class="pull-left text-info">Registrazione</a>
            
            <div class="clearfix"></div>
		</div> <!-- sm 5 -->
        
        
	</div> <!-- row -->
</div> <!-- container -->  
        
{include file='layout/foot.tpl'}
</body>
</html>
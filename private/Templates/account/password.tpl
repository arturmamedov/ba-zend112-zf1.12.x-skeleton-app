{include file='layout/head.tpl'}
</head>

<body id="settings">
	{include file='layout/navbar.tpl'}
    
<div class="container">
    {if $fp->getErrors()|count > 0}
        {include file='lib/pop_error.tpl' error=$fp->getErrors()}
    {/if}

    <div class="row">        
        {include file='account/libs/menu.tpl'}
			
{*} Address {/*}
		<form method="POST" id="userDataForm" action="/account/password" class="col-xs-12 col-sm-12 col-md-7 col-lg-7 form-horizontal" role="form" autocomplete="off">
            <input id="user_id" name="user_id" value="{$fp->user->getId()}" type="hidden" />
            
            <div class="bg-info padding">
                <div class="page-header no-margin">
                    <h4 class="text-center text-muted">{$translate->translate('Change Password')}  <i class="icon-lock"></i></h4>
                </div>
				
                {if strlen($user->user_mail) < 2}
                    <div class="alert alert-danger">
                        <p>{$translate->translate('You still have to add your email.')} <a href="/account/index">{$translate->translate('Settings')}</a></p>
                    </div>
                {else}
				<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 center-block margin-top-10">
                    {if $user->password === NULL}
                        <div class="alert alert-danger">
                            <p>{$translate->translate('You have not yet a password to access, create it now!')} <a href="/account/index">{$translate->translate('Settings')}</a></p>
                        </div>
                    {/if}
                    
					<div class="form-group {if $fp->getError('change_password')}has-error{/if}">
						<label class="col-md-4 control-label" for="newPassword">{$translate->translate('New password')}</label>
						<div class="col-md-8">
							<input id="newPassword" class="form-control" name="change_password" type="password" value="{$fp->password}" placeholder="{$translate->translate('New password')}" required />
                            <span class="help-block">{$translate->translate('The password must be 6 or more characters HAVE a number, a special or a capital')}</span>
							{include file='lib/error.tpl' error=$fp->getError('change_password')}
						</div>
					</div>

					<div class="form-group {if $fp->getError('password_confirm')}has-error{/if}">
						<label class="col-md-4 control-label" for="formPasswordConfirm">{$translate->translate('Repeat new password')}</label>
						<div class="col-md-8">
							<input id="formPasswordConfirm" class="form-control" name="password_confirm" type="password" value="{$fp->password_confirm}" placeholder="{$translate->translate('Repeat new password')}" required />
							{include file='lib/error.tpl' error=$fp->getError('password_confirm')}
						</div>
					</div>

                    {if $user->password !== NULL}
                        <div class="form-group {if $fp->getError('old_password')}has-error{/if}">
                            <label class="col-md-4 control-label" for="oldPassword">{$translate->translate('Current password')}</label>
                            <div class="col-md-8">
                                <input id="oldPassword" class="form-control" name="old_password" type="password" value="" placeholder="{$translate->translate('Current password')}" required />
                                {include file='lib/error.tpl' error=$fp->getError('old_password')}
                            </div>
                        </div>
                    {/if}    
				</div>
                
                <div class="form-group text-center">
                    <input class="btn btn-success btn-lg" type="submit" id="save" value="{$translate->translate('Change Password')}">
                </div>
                {/if}
            </div><!-- bg-white -->
        </form>
    </div><!-- row -->
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}
</body>
</html>
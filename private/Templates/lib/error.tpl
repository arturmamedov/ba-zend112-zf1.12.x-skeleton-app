{if $error|@is_array || $error|strlen > 0}
    {assign var=hasError value=true}
{else}
    {assign var=hasError value=false}
{/if}

<div class="alert alert-danger alert-dismissable" {if !$hasError} style="display:none"{/if}>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {if $error|@is_array}
        <ul class="list-unstyled">
            {foreach from=$error item=str}
                <li>
					<span class="message">{$str}</span>
				</li>
            {/foreach}
        </ul>
    {else}
		<span class="message">{$error}</span>
    {/if}
</div>
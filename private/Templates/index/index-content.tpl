<!-- Main blog .container -->
	<div class="container">
		<div class="row">
		<!-- Main blocks right side wrapper col  -->
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<!-- inner .row -->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>I più popolari</h3>
							</div>
						</div>
					</div>
				</div>
					
              <!-- FEED ITEM -->
              {for $foo=1 to 4}
                <div class="row margin-top-10" style="border: 1px solid #cccccc; border-radius: 7px; padding:10px;">
                    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
                        <div class="thumbnail"><img class="img-responsive" src="/public/img/site/theme/55.jpg" alt="" ></div>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
                        <h4><a href="/structure/view#feed5" title="" >Title della recensione fatta</a></h4>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:0px;">
                            <p class="text-warning"><i class="icon-eye-open"></i><i class="icon-eye-open"></i> <i class="icon-eye-open"></i><i class="icon-eye-close"></i> <i class="icon-eye-close"></i> </p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <p class="lead text-info text-right"><small>Recensioni:</small> <strong>10</strong></p>
                            </div>

                        <p>Struttura description paragraph.Struttura description paragraph.</p>
                        <a href="javascript:;" title="Vota per l'utilità di questa recensione"><i class="icon-thumbs-up"></i> La recensione è utile!</a>
                    </div>
                </div>
                <!-- /FEED ITEM -->
                {/for}
                
                <div class="row margin-top-10">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a class="btn btn-default btn-block" href="#" title=""><i class="icon-chevron-sign-right text-info"></i> Viasualizza di più</a>
                    </div>
                </div>
				<!-- /inner .row -->
				
				
				<!-- NEXT CITY BLOCK 
				=============================== -->
				
				<!-- inner .row -->
				<div class="row margin-top-10">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Ultimi recensionati </h3>
							</div>
						</div>
					</div>
				</div>	
					
                {for $foo=1 to 2}
                <div class="row margin-top-10" style="border: 1px solid #cccccc; border-radius: 7px; padding:10px;">
                    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
                        <div class="thumbnail"><img class="img-responsive" src="/public/img/site/theme/55.jpg" alt="" ></div>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
                        <h4><a href="#" title="" >Recensione della struttura</a></h4>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:0px;">
                            <p class="text-warning"><i class="icon-eye-open"></i><i class="icon-eye-open"></i> <i class="icon-eye-open"></i><i class="icon-eye-open"></i> <i class="icon-eye-close"></i> </p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <p class="lead text-info text-right"><small>Recensioni:</small> <strong>100</strong></p>
                            </div>

                        <p>Struttura description paragraph.Struttura description paragraph.Struttura description paragraph.Struttura description paragraph.Struttura description paragraph.</p>
                        <a href="javascript:;" title="Vota per l'utilità di questa recensione"><i class="icon-thumbs-up"></i> La recensione è utile!</a>
                    </div>
                </div>
                <!-- /FEED ITEM -->
                {/for}
                
                {for $foo=1 to 2}
                <div class="row margin-top-10" style="border: 1px solid #cccccc; border-radius: 7px; padding:10px;">
                    <div class="col-xs-2 col-sm-4 col-md-4 col-lg-4">
                        <div class="thumbnail"><img class="img-responsive" src="/public/img/site/theme/55.jpg" alt="" ></div>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8 col-lg-8">
                        <h4><a href="#" title="" >Titolo Recensione</a></h4>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:0px;">
                            <p class="">
                                <i class="icon-eye-open text-rose"></i>
                                <i class="icon-eye-open text-rose"></i> 
                                <i class="icon-eye-open text-rose"></i>
                                <i class="icon-eye-open text-gray"></i> 
                                <i class="icon-eye-open text-gray"></i>
                            </p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <p class="lead text-info text-right"><small>Recensioni:</small> <strong>140</strong></p>
                            </div>

                        <p>Struttura description paragraph. Struttura description paragraph. Struttura description paragraph. Struttura description paragraph.</p>
                        <a href="javascript:;" title="Vota per l'utilità di questa recensione"><i class="icon-thumbs-up"></i> La recensione è utile!</a>
                    </div>
                </div>
                <!-- /FEED ITEM -->
                {/for}
                
                <div class="row margin-top-10">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a class="btn btn-default btn-block" href="#" title=""><i class="icon-chevron-sign-right text-info"></i> Visualizza altro</a>
                    </div>
                </div>
				
				<!-- /inner .row -->
				
					<hr>
				<div class="row margin-top-10">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="well">
								<div class="row">
									<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
										<i class="icon-comments icon-3x text-info"></i>
									</div>
									<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
										<p class="lead">Leggete più di $_totale_ recensioni da utenti reali! <a class="btn btn-link" href="#" title="">Leggere</a></p>
									</div>
								</div>
							</div>
						</div>
				</div>
				
			</div>
		<!-- /Main blocks left side -->
		
			<!-- Teasers right side wrapper col-->
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 gradient-bg" >
						<div class="panel panel-default margin-top-10">
							<div class="panel-heading">
								<h3>Struture</h3>
							</div>
						</div>
			
				<div class="row margin-top-10">
                    {foreach from=$structures item=item}
                                
                            
                    <!-- COUNTRY ITEM -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="thumbnail">
                            <img class="img-responsive" src="/public/img/site/theme/rome9.jpg" alt="" >
                            <div class="caption alt"><p class="lead">{$item->city_id}</p></div>
                            <div class="caption">
                                <h4><a href="#" title=""><i class="icon-eye-open text-rose"></i> {$item->profile->vote}</a></h4>
                                <p>{$item->name}</p>
                                <p>
                                    <a href="/{$item->type}/view/id/{$item->id}/{$item->url}" class="" style="text-decoration: underline; margin:0 4px;">1 Recensione</a>
                                </p>
                            </div>

                        </div>

                    </div>
                    <!-- /COUNTRY ITEM -->
                    {/foreach}
                </div>

			
			<!-- /Teasers right side-->
            <div class="panel panel-default margin-top-10">
                <div class="panel-heading">
                    <h3>Regioni</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 center-block">
                        <div class="text-center">
                            {foreach from=$regions item=region}
                                <a href="/place/region/id/{$region->id}" class="" style="text-decoration: underline; margin:0 4px;">#{$region->name}</a>
                            {/foreach}
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-default btn-sm btn-block" href="#" title=""><i class="icon-building"></i> All countries list</a>
                </div>
            </div>						
		</div>
	</div>
	<!-- /Main blog .container -->
    
    
       <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
<hr>
<div class="panel panel-primary">
<div class="panel-body">
	<div class="container">
		<div class="row ">
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 text-center">
				<p class="lead ">
					<i class="icon-gift text-info"></i> Iscrivitevi solo con mail per ricevere newslatter e non perdervi gli aggiornamenti principali!
				</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 text-center">
				<form class="form-inline" role="form">
				  <div class="form-group">
					<label class="sr-only" for="exampleInputEmail2">Indirizzo eMail</label>
					<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Inserire eMail">
				  </div>
				  <button type="submit" class="btn btn-info">Iscriviti</button>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
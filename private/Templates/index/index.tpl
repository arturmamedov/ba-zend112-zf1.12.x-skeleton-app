{include file='layout/head.tpl'}
</head>

<body id="index" class="gradient" ng-app="main" id="ng-app">
    {include file='layout/navbar.tpl'}

<div class="container" ng-controller="IndexCtrl">    

    <div class="col-xs-10 center-block bg-info border-radius padding">
        <h1 class="text-center text-danger">Hello World - @withArtur</h1>
        
        <p class="text-muted">Applicazione base con ZendFramework 1.12.x , pattern MVC con Smarty come Tamplaterperil View. Composto dal Controller User e Account per creare una registrazione e una parte interna al utente. Dal IndexController per le pagine principali del sito, home page, contatti, termini.</p>
    </div>

    
</div>
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl'}
</body>
</html>
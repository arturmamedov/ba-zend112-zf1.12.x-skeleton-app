{include file='layout/head.tpl'}
</head>

<body id="about">
	{include file='layout/navbar.tpl'}
<div class="container" id="container">
	
	{include file='lib/pop_message.tpl'}
	
	

	<div class="page-header text-center">
		<h1>Informazioni di contatto <small>{$tsn}</small></h1>
	</div>
	
	<div class="row">
		<figure class="col-md-3">
			<img src="/public/img/site/logo.jpg" alt="Logo {$tsn}" class="img-responsive">
		</figure>
		
		<div class="col-md-4">
			<address>
				<strong>{$tsn}</strong>,<br>
				P.iva: .......<br>
				<label>emailexample@gmail.com</label><br>
				<abbr title="FAX">fax:</abbr> (+39) 123 123 123
			</address>
		</div>
		
		<div class="col-md-4">
			<p>
				a e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenev
			</p>
			<p>
				a e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenev
			</p>
			<p>
				a e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenev
			</p>
		</div>
	</div>
	
	<div class="row">
		{if isset($fp->text) && !$fp->hasError()}
			<h2>Ti risponderemo il più presto possibile</h2>
			<p>{$fp->name} ci hai scritto:</p> 
			<p>{$fp->text}</p>
			<p>Ti ricontatteremo su {$fp->mail}</p>
			
		{else}
		<div class="col-md-6">
			<h2>Contattaci</h2>
			{*}<p>Per problemmatiche al sito puoi anche cercare e rivolgerti alla sezione supporto.</p>{/*}
			
			<form method="POST" action="/index/about">
				<div class="form-group">
					<label for="mail">Vostra e-Mail</label>
					{if $authenticated}
						<p class="form-control-static">{$fp->mail}</p>
					{else}
						<input type="text" id="mail" name='mail' class="form-control" value="{$fp->mail}" required placeholder="email@example.com" required>
					{/if}
                    {include file='lib/error.tpl' error=$fp->getError('mail')}
				</div>
				
				<div class="form-group">
					<label for="name">Nome</label>
					{if $authenticated}
						<p class="form-control-static">{$fp->name}</p>
					{else}
						<input type="text" name='name' id="name" class="form-control" value="{$fp->name}" required placeholder="Emanuele" required>
					{/if}
                    {include file='lib/error.tpl' error=$fp->getError('name')}
				</div>
				
				<div class="form-group">
					<label for="text">Cosa volete comunicarci</label>
					<textarea id="text" name='text' class="form-control" required>{$fp->text}</textarea>
                    {include file='lib/error.tpl' error=$fp->getError('text')}
				</div>
				
				<div class="row center-text">
					<input type="submit" class="btn btn-info btn-lg" value="Invia" title="Iviaci il tuo messaggio"> 
				</div>
			</form>
		</div>
		{/if}
		
		
	</div>


	{include file='layout/footer.tpl'}
</div>

{include file='layout/foot.tpl' hide=true}

</body>
</html>
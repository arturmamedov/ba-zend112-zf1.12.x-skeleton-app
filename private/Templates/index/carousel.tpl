<!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide">
     
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="/public/img/site/Carousel/HospitalBig.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>SocialEye</h1>
              <p>Recensioni sulle strutture ......</p>
              <p><a class="btn btn-large btn-primary" href="#">Registrati Ora!</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/public/img/site/Carousel/HospitalHeart.jpg"  alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Lavoraiamo nel vostro Interesse</h1>
              <p>Approviamo recensioni con cura .....</p>
              <p><a class="btn btn-large btn-primary" href="#">Learn more</a></p>
            </div>
          </div>
        </div>
          <div class="item">
          <img src="/public/img/site/Carousel/wallpaper-occhio.jpg"  alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>L'occhio sulla salute</h1>
              <p>Solo la verità, per esssere informati bene e prevenire al posto di curare .....</p>
              <p><a class="btn btn-large btn-primary" href="#">Learn more</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="icon-prev"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="icon-next"></span></a>
    </div>
	<!-- /.carousel -->
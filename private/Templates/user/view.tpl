{include file='layout/head.tpl'}
</head>

<body id="about">
	{include file='layout/navbar.tpl'}
<div class="container" id="container">
	
	{include file='lib/pop_message.tpl'}

    <div class="row">

        <div class="col-md-10">

            <div class="page-header" itemscope itemtype="http://schema.org/Person">
                <h1 itemprop="name">{$user->profile->first_name} {$user->profile->last_name}</h1>

                {*}<div class="btn-group">
                    {if !$is_user}<button type="button" class="btn btn-danger btn-sm {if !$authenticated}nlog{/if}" id="writeDialog">
                        Scrivi
                        <span class="glyphicon glyphicon-envelope"></span>
                    </button>{/if}
                    <button type="button" class="btn btn-info btn-sm">
                        Segui
                        <span class="glyphicon glyphicon-plus-sign"></span>
                    </button>
                    {include file='user/libs/followu.tpl' followu=$followu}
                </div>{/*}
            </div>

            <div class="row">
                <figure class="col-md-3">
                    <img itemprop="image" src="/public/files/user/image/{$user->poster|default:'default.png'}" alt="Logo Generalbrokers srl" title="Logo Generalbrokers srl" style="width:100%;" class="img-rounded">
                </figure>

                <div class="col-md-4">
                    <address>
                      <strong>{$user->profile->first_name} {$user->profile->last_name}</strong><br>
                      {$user->profile->indirizzo}<br>
                      {$user->profile->localita} 
                      {if $user->profile->provincia != ''}({$user->profile->provincia}),{/if} {$user->profile->regione}<br>
                      {if $user->profile->telefono_pubblico != ''}<abbr title="Telefono">Tel:</abbr> (+39) {$user->profile->telefono_pubblico}{/if}
                    </address>

                    <address>
                        {if $user->sito_web != ""}
                            <strong><a href="{$user->profile->sito_web}" target="_blank">{$user->profile->sito_web}</a></strong><br>
                        {/if}
                        {if $user->email_pubblica != ""}
                            <a href="mailto:{$user->profile->email_pubblica}">{$user->profile->email_pubblica}</a>
                        {/if}
                    </address>
                    
                    
                    {*}<dl>
                        <dt>Membro da:</dt>
                        <dd>{DatabaseObject_Calendar::tsReadableDiff($user->ts_created, null, 2)}</dd>
                    </dl>{/*}
                </div>

                <div class="col-md-5">
                    <blockquote class="blockquote-reverse">
                        <p>{$user->description|default:'Nessuna descrizione ...'}</p>
                    </blockquote>
                </div>    
            </div>

        <div class="clearfix"></div>

            <div class="page-header">
                <h2>Annunci immobiliari pubblicati ({$items_count})</h2>
            </div>

        <!-- filters -->
                <!--<span class="left">Risultati 1-15 di 15 </span>
                <form action="/agenzia.php?id=11999&amp;page=1" method="post" name="orderform">
                    Ordina per: 
                    <input type="radio" name="prezzoc" onchange="document.orderform.submit()"> Prezzo cresente
                    <input type="radio" name="prezzod" onchange="document.orderform.submit()"> Prezzo decrescente
                    <input type="radio" name="mqc" onchange="document.orderform.submit()"> MQ cresente
                    <input type="radio" name="mqd" onchange="document.orderform.submit()"> MQ decresente
                </form>

            <div class="btn-group btn-group-justified col-md-12">
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Prezzo cresente</button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Prezzo decresente</button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default">MQ cresente</button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default">MQ decresente</button>
                </div>
            </div> -->
        <!-- filters -->
            
            <!-- pagination -->
            <div class="text-center">
                <ul class="pagination">
                    
                    {$pagination}
                    
                </ul>
            </div> <!-- pagination END -->
            
            <div class="clearfix" style="margin: 10px 0 ;"></div>
            <div class="col-md-12">
                {* include file='property/items/user_page.tpl'}{/*}
            </div>

            
            <!-- pagination -->
            <div class="text-center">
                <ul class="pagination">
                    
                    {$pagination}
                    
                </ul>
            </div> <!-- pagination END -->
            
        {*}
        <!-- Propertys -->
            <div class="res-box">
                <div class="pic">
                    <a class="dettagli" href="/annuncio.php?id=70">
                    <img src="/image.php?w=80&amp;h=80&amp;f=administrator/public/resized-286568714_img_1044.jpg">
                    </a>
                </div>
                <div class="text">
                    <span class="title"><a class="dettagli" href="/annuncio.php?id=70">BILOCALE - Vendita</a></span>
                    <span class="richiesta">
                        <a class="rich" href="javascript:;" data-reveal-id="myModal70">invia richiesta</a>
                        <a class="dettagli" href="/annuncio.php?id=70">dettagli annuncio</a>
                    </span>
                    <div class="det">
                        <span class="cat">Categoria: <strong>Residenziale</strong></span>
                        <span class="mq">MQ: <strong>70</strong></span>
                        <span class="loc">Località: <strong>Cernusco sul Naviglio</strong></span>
                    </div>
                    Cernusco s/N - zona centro - vendesi appartamento di 2 locali mq. 70 composto da: ingresso, soggiorno, cucin....
                </div>
                <div class="price">138.000,00 EURO</div>
            </div>
        <!-- Dialog
            <div id="myModal70" class="reveal-modal">	
                <h3>Richiesta veloce per l'immobile BILOCALE - Vendita</h3>

                <div class="esito"></div>

                <form action="" id="fastform" class="70" method="post">
                <input type="hidden" id="email_dest70" name="email_dest" value="info@generalbrokers.com">
                <p>il tuo nome</p>
                <p><input type="text" id="nome70" name="nome"></p>
                <p>la tua e-mail</p>
                <p><input type="text" id="email70" name="email"></p>
                <p>oggetto</p>
                <p><input type="text" id="ogg70" name="ogg"></p>
                <p>messaggio</p>
                <p><textarea rows="10" id="mess70" name="mess"></textarea></p>
                <p><input type="submit" value="invia richiesta" name="invia">
                </p></form>
            </div>
        -->
        {/*}

            <!--pagination
            <ul class="pagination">
                <li class="disabled"><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
            <!--pagination-->

        </div> <!-- 10 -->

        <!--column left-->
        <div class="col-md-2">
            
            <div class="clearfix" style="margin: 10px 0 ;"></div>
            Barra sinistra
        </div><!--column left-->

    </div> <!-- row -->

	
</div><!-- container -->
{include file='layout/footer.tpl'}
{include file='layout/foot.tpl' hide=true}

</body>
</html>
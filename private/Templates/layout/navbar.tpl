<!-- NAVBAR
================================================== -->
  <body>
   <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
       <div class="container">
	  <!-- Brand and toggle get grouped for better mobile display -->
	  <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		  <span class="sr-only">Menu Navigazione</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
          <a class="navbar-brand" href="/">
              <img src="/public/img/site/logo-obiv.jpg" class="img-responsive">
          </a>
	  </div>

	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
		  <li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">withArtur <b class="caret"></b></a>
			<ul class="dropdown-menu">
                            <li><a href="#">Artur Mamedov <i class="fa fa-linkedin text-primary"></i></a></li>
			  <li><a href="http://www.obiv.it/" target="_blank">[OBIV].it</a></li>
			  <li><a href="#">@withArtur <i class="fa fa-youtube-play text-danger"></i></a></li>
			  <li><a href="#">@arturmamedov <i class="fa fa-youtube-play text-danger"></i></a></li>
			  <li><a href="#">@videogiornaleOBIV <i class="fa fa-youtube-playtext-danger"></i></a></li>
			</ul>
		  </li>
		  <li><a href="#">#PHP <i class="fa fa-youtube-play text-danger"></i></a></li>
		  <li><a href="#">OOP con PHP <i class="fa fa-youtube-play text-danger"></i></a></li>
		  <li><a href="#">MVC con PHP <i class="fa fa-youtube-play text-danger"></i></a></li>
		  <li><a href="https://it-it.facebook.com/withArtur" target="_blank">Facebook <i class="fa fa-facebook-official fa-ls text-primary"></i></a></li>
		  
		</ul>
          
        {if !$authenticated}
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-share"></i></a>
                    <ul class="dropdown-menu" style="min-width:40px;">
                        <li style="font-size: 11px;" class="text-center">Condividi</li>
                        <li><a href="#" title=""><i class="icon-twitter"></i></a></li>
                        <li><a href="#" title=""><i class="icon-facebook"></i></a></li>
                        <li><a href="#" title=""><i class="icon-google-plus"></i></a></li>
                        <li><a href="#" title=""><i class="icon-youtube"></i></a></li>	
                        <li><a href="#" title=""><i class="icon-dribbble"></i></a></li>
                    </ul>
                </li>
                    <li><a href="/account/login" title="">Accedi</a></li>
                    <li><a href="/account/signup" title="">Registrati</a></li>
                    <li style="border-right: none;"><span style=" color: #DDDDDD;display: block;padding: 13px 3px;">Connettiti con:</span></li>
                    <li><a href="#" title=""><i class="icon-twitter"></i></a></li>
                    <li><a href="#" title=""><i class="icon-facebook"></i></a></li>
                    <li><a href="#" title=""><i class="icon-google-plus"></i></a></li>
            </ul>
        {else}
            <ul class="nav navbar-nav navbar-right">
                <li style="border-right: none;"><span style=" color: #DDDDDD;display: block;padding: 13px 8px;">Ciao {$identity->first_name}!</span></li>
                <li><a href="/account/index" title="">Impostazioni</a></li>
                <li><a href="/account/logout" title="">Esci</a></li>
            </ul>
        {/if}
	</div><!-- /.navbar-collapse --> 
	</div>
</nav>
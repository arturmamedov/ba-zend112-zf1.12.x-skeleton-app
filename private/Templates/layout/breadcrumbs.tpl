<!-- breadcrumbs-->	
<div class="row">
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md12 col-lg-12">
        <ol class="breadcrumb">
            {breadcrumbs trail=$breadcrumbs->getTrail()}
        </ol>
    </div>
</div>
</div>
<!-- /breadcrumbs-->
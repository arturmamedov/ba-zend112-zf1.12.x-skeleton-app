<footer class="row footer-site bg-white" role="contentinfo">
  <div class="container text-center">
    <p class="pull-right"><a class="btn btn-sm btn-default" href="#">Torna su</a></p>
    <p>Copyright 2010-2015 Artur Mamedov &copy; <a href="http://obiv.it/">{$tsn}</a></p>
    
    <p>Telephone: +39 333 35 44990 | <a href="/index/about" title="Contattaci, inviando un messaggio dalla pagina delle informazioni">withArtur@gmail.com</a></p>
    <ul class="list-unstyled list-inline">
      <li>Benvenuto {$identity->first_name|default:'sul sito'}</li>
      <li>·</li>
      <li><a href="/">Home</a></li>
      <li>·</li>
      <li><a href="/index/about">Informazioni/Contattaci</a></li>
      <li>·</li>
      <li><a href="/account">Account</a></li>
      <li>·</li>
      <li><a href="/dashboard">Gestione</a></li>
	  
	  
	  <div class="clearfix"></div>
	  <li class="pull-left margin-top-10">
        {if $lang == 'it'}
            <strong class="text-underline1" title="{$translate->translate("Italian")}"><i class="flag flag-it"></i> ITA</strong> | 
            <a href="{$translate_uri}" title="{$translate->translate("View in %s")|sprintf:$translate->translate("English")}"><i class="flag flag-gb"></i> EN</a></li>
        {else}
            <a href="{$translate_uri}" title="{$translate->translate("View in %s")|sprintf:$translate->translate("Italian")}"><i class="flag flag-it"></i> ITA</a> | 
            <strong class="text-underline1" title="{$translate->translate("English")}"><i class="flag flag-gb"></i> EN</strong></li>
        {/if}
    </ul>
  </div>
</footer>
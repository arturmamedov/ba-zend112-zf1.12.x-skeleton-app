<?php
class Breadcrumbs
{
    private $_trail = array();

    public function addStep($name, $link = '', $title = '')
    {
        $this->_trail[] = array(
            'name' => $name,
            'link'  => $link,
            'title'  => $title
        );
    }

    public function getTrail()
    {
        return $this->_trail;
    }

    public function getTitle()
    {
        if (count($this->_trail) == 0){
            return null;
        }
        
        return $this->_trail[count($this->_trail) - 1]['title'];
    }
}
<?php

/*
 * File with translation of all database category in italian language
 * the aray are associative and go to keep here value from a 'name' value in Db
 */

class Languages_itIT {

    public function categories(){
            $categories = array(
                'MATH' => 'Matematica',
                'PHYSICS' => 'Psicologia',
                'LANGUAGE' => 'Lingue',
                'ECONOMY' => 'Economia',
                'LITERATURE' => 'Letteratura',
                'PHILOSOPHY ' => 'Filosofia',
                'HOBBY' => 'Hobby',
                'MEDICINE' => 'Medicina',
                'COOKING' => 'Cucina',
                'DESIGN' => 'Design',
                'HISTORY' => 'Storia',
                'GEOGRAPHY' => 'Geografia',
                'MUSIC' => 'Musica',
                'CHEMISTRY' => 'Chimica',
                'BIOLOGY' => 'Biologia',
                'FASHION' => 'Moda',
                'PROGRAMMING' => 'Programmazione',
                'SOFTWARE' => 'Software',
                'ELETRONICS' => 'Elettronica',
                'PSYCOLOGY' => 'Psicologia',
                'HISTORY' => 'Storia',
                'GEOGRAPHY' => 'Geografia',
                'DIRECTION' => 'Regia',
                'PHOTOPGRAPHY' => 'Fotografia',
                'AGRICOLTURE' => 'Agricoltura',
                'MECHANICS' => 'Meccanica',
                'ANTHROPOLOGY' => 'Antropologia',
                'HOWTO' => 'Fai da te',                     // o 'Come fare'
                'MARKETING' => 'Marketing',
                'COMUNICATION' => 'Comunicazione',
                'LAW' => 'Legge'
            );
            
            return $categories;
    }

}
?>

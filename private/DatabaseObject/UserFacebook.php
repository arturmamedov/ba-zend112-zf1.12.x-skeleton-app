<?php
/**
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 * @version 1.1
 * 
 * Class of method for work with _user_ , _p-user_ and his data
 * method for register, connect, getInformation, getUsers, setData, searchFor _user_/s
 */
class DatabaseObject_UserFacebook extends DatabaseObject_User
{    
    // define all need variables
    var $app_id = "749627421745480";
    var $app_secret = "f3563436149d6a9a54f4030235fabf5b";
    
    public function __construct($db){
        parent::__construct($db);
        
        $this->social = 'Facebook';
        $this->user_logger('facebook');
    }

    protected function preInsert(){
        parent::preInsert();
        return true;
        
    }

    protected function postLoad(){
        parent::postLoad();
        return true;
    }

    protected function postInsert(){
        parent::postInsert();
        return true;
    }

    protected function postUpdate(){
        parent::postUpdate();
        return true;
    }

    protected function preDelete(){
        parent::preDelete();
        return true;
    }
    
    
    /**
     * Get Facebook access_token and expire data, set him how $sess->'session'
     * 
     * @param type $redirect (the function nedd a redirection the url must be the callAction)
     * 
     * @return bool (true or false)
     */
    public function fbAccesstoken($redirect)
    {
        $sess = Zend_Registry::get('session');
        $sess->redirect_end = ($redirect != 'NO') ? $redirect : $sess->redirect_end;
        //exit($sess->fb_access_token .' || '. $sess->fb_access_token_expire .' <= '. time() .' - '. $sess->redirect_end);
        
        if(empty($sess->fb_access_token) || $sess->fb_access_token_expire <= time()){    
            // define all need variables
            $app_id = $this->app_id;
            $app_secret = $this->app_secret;
            
            $my_url = Zend_Registry::get('config')->site->web_uri.'account/access?conn=fb'; // $redirect
            
            // il session code serve per capire se c'e una sessione e se non ce si crea una con oAuth di FB
            $code = (isset($_REQUEST['code'])) ? $_REQUEST['code'] : null;
            if(empty($code)){
                $sess->state = md5(uniqid(rand(), TRUE)); // CSRF protection
                // automatically ask to @public_profile to
                if($sess->fb_nomail == true){
                    $dialog_url = "https://www.facebook.com/v2.0/dialog/oauth?client_id=".$app_id."&redirect_uri=".urlencode($my_url)."&state=".$sess->state.'&scope=email'."&auth_type=rerequest";
                    unset($sess->fb_nomail);
                    //,user_website,user_location,user_about_me,user_birthday - &scope=email
                } else {
                    $dialog_url = "https://www.facebook.com/v2.0/dialog/oauth?client_id=".$app_id."&redirect_uri=".urlencode($my_url)."&state=".$sess->state.'&scope=email';
                    //,user_website,user_location,user_about_me,user_birthday - ."&scope=email" | public_profile, email
                }
                
                // user_birthday are replaced by age_range that are in public_profile
                
                exit("<script>top.location.href='".$dialog_url."'</script>");
                header('Location:'.$dialog_url);
            }

            // se c'e la sessione ed e' quella della risposta da FB allora creiamo l'access token e accediamo ai dati
            if($sess->state == $_REQUEST['state']){
                //risposta che arriva:
                //access_token=USER_ACCESS_TOKEN&expires=NUMBER_OF_SECONDS_UNTIL_TOKEN_EXPIRES
                //aquesto punto bisogna memorizzare l'access token da qualche parte! per ri utilizzarlo
                $token_url = "https://graph.facebook.com/v2.0/oauth/access_token?client_id=".$app_id."&redirect_uri=".urlencode($my_url)."&client_secret=".$app_secret."&code=".$code;
                //exit($token_url);
                $response = file_get_contents($token_url);
                $params = null;
                parse_str($response, $params);

                //exit(print_r($params));

                $sess->fb_access_token = $params['access_token'];
                $sess->fb_access_token_expire = time() + $params['expires'];
                
                $redirect_end = $sess->redirect_end;
                $sess->redirect_end = null;
                exit("<script>top.location.href='".Zend_Registry::get('config')->site->web_uri.$redirect_end."'</script>");
                return;
                //header('Location: '.Zend_Registry::get('config')->site->web_uri.$redirect_end);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Facebook[method]
     * Call with CURL GET a Facebook endpoint for execute operation
     * Retrive access_token with fbAccesstoken if necessary
     * 
     * @param string $method (piece of url that go be call ex: 'me/?')
     * @param string $redirect (the url of page that begin the call, this go to be passed to a fbAccesstoken if access_token needded)
     * 
     * @return array    ([status] => false, [error] => 'ACC_TOK/CURL_ERR', [message] => 'Facebook[method] - ..desc..')
     *                  ([status] => true, [profile] => $profile ('all_data'))
     */
    public function fbMethod($method, $redirect)
    {
        $sess = Zend_Registry::get('session');
        
        if(!$this->fbAccesstoken($redirect)){
            return array('status' => false, 'error' => 'ACC_TOK', 'message' => 'Facebook[method] - False access_token');
        } else {
            $uri = "https://graph.facebook.com/v2.0/{$method}access_token=".$sess->fb_access_token;
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_NOBODY, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $profile = json_decode(curl_exec($ch));
            //exit(print_r($profile));
            if($profile === FALSE){
                curl_close($ch);
                return array('status' => false, 'error' => 'CURL_ERR', 'message' => "Facebook[method] - cURL Error: " . curl_error($ch));
            } else {
                curl_close($ch);
                return array('status' => true, 'profile' => $profile);
            }
        }
        return false;
    }
    
    /**
     * Facebook[userinfo]
     * Retrive the user data from Facebook (call fbMethod(me?))
     * And if user_mail already registered , login user
     * if user are new register himdata and login
     * 
     * @param striung       $redirect (url of page that begin the call, need to fbMethod if access_token go to be retirived)
     * @param int/string    $user_id (the id of user if call are doing by logged user for, import image and data)
     * 
     * @return array    ([status] => false, [error] => '[fbMethod[error]]/NO_MAIL/NO_REG/CANT_LOAD_REG/CANT_LOAD_CONN/NO_UPDATE', [message] => 'Facebook[userinfo] - ..desc..')
     *                  ([status] => true, [message] => 'REGISTERED/CONNECTED', 'identity' => (data from $this->creatAuthIdentity))
     */
    public function fbUserinfo($redirect, $user_id = null)
    {
        $profile = $this->fbMethod('me?', $redirect);
        if($profile['status'] === false){
            return array('status' => false, 'error' => $profile['error'], 'message' => $profile['message']);
        } else {
            $profile = $profile['profile'];
        }
        echo '<pre>';
        
        $sess = Zend_Registry::get('session');
        // user_mail important and go defined in any case
        if(isset($profile->email)){
            $this->user_mail = (string)$profile->email;
        } else {
            $this->user_mail = null;
        }
        
        /*if(strlen($this->user_mail) == 0){
            return array('status' => false, 'error' => 'NO_MAIL', 'message' => sprintf('Facebook[userinfo] - No mail retrived from "facebook...me?" | name: %s | surname: %s | fb_id: %s | atoken: %s |age: %s ||', $profile->first_name, $profile->last_name, $profile->id, $sess->fb_access_token, $profile->age_range), 'profile' => $profile);
        }*/
        
        $cdn_path = $this->image_cloudfront_endpoint.$this->bucket_image_path;
        $bucket_path = $this->bucket.'/'.$this->bucket_image_path;
        
        if(!$this->existMail($this->user_mail) && $user_id === null && $this->fbUserid($profile->id) <= 0){
            // extract data from Fb profile and register him into Db
            //$this->_newPassword = $this->password = uniqid();
            $this->password = null;

            $date = new Zend_Date();
            
            if(!empty($profile->first_name)){
                $this->profile->first_name = $profile->first_name;
            }
            if(!empty($profile->last_name)){
                $this->profile->last_name = $profile->last_name;
            }
            if(!empty($profile->id)){
                $this->profile->fb_id_conn = $profile->id;
            }
            
            // https://developers.facebook.com/docs/graph-api/reference/v2.0/user/picture
            // profile_photo based on facebook user id
            $this->profile->poster_name = 'user_fb_l_'.$this->profile->fb_id_conn.'.jpg';
            
            $this->poster = $cdn_path.$this->profile->poster_name;
            $this->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->profile->poster_name;
            $this->profile->poster_normal = $cdn_path.'normal/'.$this->profile->poster_name;            
            
            $this->getImgByUrl('https://graph.facebook.com/v2.0/'.$this->profile->fb_id_conn.'/picture?type=large', $bucket_path, $this->profile->poster_name);
            
            
            if(!empty($profile->link)){
                $this->profile->fb_profile = $profile->link;
            }
            
            // timezone of user
            if($profile->timezone !== null){
                $inSecond = $profile->timezone * 60 * 60;
                $this->time_zone = timezone_name_from_abbr("", $inSecond, 0);
            } else {
                $this->time_zone = 'Europe/Rome'; //$sess->timeZone;
            }

            // user locale and user languages
            if(!empty($profile->locale)){
                $this->user_locale = (string)$profile->locale;
                $this->language = $this->langIdByLocale($profile->locale);
                $this->profile->first_language = $this->langIdByLocale($profile->locale);
            } else {
                $this->user_locale = $sess->localeCode;
                $this->language = $this->langIdByLocale($sess->localeCode);
                $this->profile->first_language = $this->langIdByLocale($profile->locale);
            }

            // other
            if(!empty($profile->gender)){
                $this->profile->gender = (string)$profile->gender;
            }
            if(!empty($profile->updated_time)){
                $this->profile->fb_updated_time = (string)$profile->updated_time;
            }
            if(!empty($profile->middle_name)){
                $this->profile->fb_middle_name = (string)$profile->middle_name;
            }
            if(!empty($profile->name)){
                $this->profile->fb_name = (string)$profile->name;
            }
            /*if(!empty($profile->birthday)){
                $this->profile->birthday = (string)$profile->birthday;
            }
            if(!empty($profile->bio)){
                $this->profile->about = (string)$profile->bio;
            }
            if(!empty($profile->location->name)){
                $this->profile->location = (string)$profile->location->name;
            }
            if(!empty($profile->website)){
                $this->profile->web_site = (string)$profile->website;
            }*/

            if($profile->verified){
                $this->user_active = 1;
            } else {
                $this->user_active = 0; 
            }
            $this->user_type = 'user';
            
            $this->ts_created = $date->get('YYYY-MM-dd HH:mm:ss');
            $this->ts_last_online = $date->get('YYYY-MM-dd HH:mm:ss');
            $this->ts_last_login = $date->get('YYYY-MM-dd HH:mm:ss');
            
            $uidentity = 'f-b'.$date->get('B');
            $date->set('00:00:00', Zend_Date::TIMES);
            $uidentity = $date->get('U').$uidentity; 
            if($this->existIdentity($uidentity)){
                $this->logger->warn('User-fb[Registration] - Equal identity :O {identity: '.$uidentity);
                $this->identity = $uidentity.'r'.rand(0, 1000);
            } else {
                $this->identity = $uidentity;
            }

            // for future API call
            $this->profile->fb_access_token = $sess->fb_access_token;
            $this->profile->fb_access_token_expire = $sess->fb_access_token_expire;
            $acln = strlen($sess->fb_access_token);
            if($acln > 250){
                $this->logger->crit("AccessToken size to much for varchar 255 lenght: {$acln} value: ".$sess->fb_access_token);
            }
            // for facebook connected and autoposting
            $this->profile->fb_connected = '1';
            $this->profile->fb_autopost = '1';

            // @todo - can i know the exact issue? ex: no locale, no, language etc...
            if(!$this->save()){
                 return array('status' => false, 'error' => 'NO_REG', 'message' => "Facebook[userinfo] - save(new) failed for (user_mail: {$this->user_mail} , user_name: {$this->profile->first_name})");
            }

            // initialize the options
            $options = array(
                array('resize' => 'fit', 'w' => 100, 'h' => 100, 'path' => 'thumbnail', 'image' => $this->profile->poster_name),
                array('resize' => 'height', 'w' => 220, 'h' => 220, 'path' => 'normal', 'image' => $this->profile->poster_name)
            );
            $this->makeThumbs('image/jpg', $bucket_path, $options);
            
            $this->setUserLocale($this->user_locale);

            return array('status' => true, 'message' => 'REGISTERED', 'identity' => $this->createAuthIdentity());
        } 
        else {
            if($user_id !== null){
                $this->load($user_id);
            } elseif($this->fbUserid($profile->id) > 0){
                if(!$this->load($this->fbUserid($profile->id))){
                    return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Facebook[userinfo] {pid-1} - load(exist) failed for (user_mail: {$this->user_mail})");
                }
            } else {
                // get user id by mail and load user
                $u_id = $this->useridofMail($this->user_mail);
                if($u_id > 0){
                    $this->load($u_id);
                } else {
                    return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Facebook[userinfo] - load(exist) failed for (user_mail: {$this->user_mail}) fb_id: {$profile->id} , fb_fname: {$profile->first_name}");
                }
            }
            
            // user_mail important and go defined in any case
            if(isset($profile->email) && strlen($this->user_mail) <= 3){
                $this->user_mail = (string)$profile->email;
            }
            
            // if no set save new social_id in DB
            if(!isset($this->profile->fb_id_conn)){
                if(!empty($profile->id)){
                    $this->profile->fb_id_conn = $profile->id;
                }
                $this->profile->fb_autopost = '1';
            }
            
            if(!empty($profile->first_name)){
                $this->profile->first_name = $profile->first_name;
            }
            
            if(!empty($profile->last_name)){
                $this->profile->last_name = $profile->last_name;
            }
            
            // https://developers.facebook.com/docs/graph-api/reference/v2.0/user/picture
            // profile_photo based on facebook user id
            $this->profile->poster_name = 'user_fb_l_'.$this->profile->fb_id_conn.'-'.time().'.jpg';
            
            $this->poster = $cdn_path.$this->profile->poster_name;
            $this->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->profile->poster_name;
            $this->profile->poster_normal = $cdn_path.'normal/'.$this->profile->poster_name;            
            
            $this->getImgByUrl('https://graph.facebook.com/v2.0/'.$this->profile->fb_id_conn.'/picture?type=large', $bucket_path, $this->profile->poster_name);

            // save new social_profile link in DB
            if(!empty($profile->link)){
                $this->profile->fb_profile = $profile->link;
            }

            // other
            if(!empty($profile->gender)){
                $this->profile->gender = (string)$profile->gender;
            }
            
            // for future API call
            $this->profile->fb_access_token = $sess->fb_access_token;
            $this->profile->fb_access_token_expire = $sess->fb_access_token_expire;
            $acln = strlen($sess->fb_access_token);
            if($acln > 250){
                $this->logger->crit("AccessToken size to much for varchar 255 lenght: {$acln} value: ".$sess->fb_access_token);
            }
            // for facebook connected and autoposting
            $this->profile->fb_connected = '1';

            if($profile->verified){
                $this->user_active = 1;
            } else {
                $this->user_active = 0; 
            }

            if(!$this->save()){
                return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Facebook[userinfo] - save(exist) failed for (user_id: {$this->getId()}, user_mail: {$this->user_mail})");
            }
            
            // initialize the options
            $options = array(
                array('resize' => 'fit', 'w' => 100, 'h' => 100, 'path' => 'thumbnail', 'image' => $this->profile->poster_name),
                array('resize' => 'height', 'w' => 220, 'h' => 220, 'path' => 'normal', 'image' => $this->profile->poster_name)
            );
            $this->makeThumbs('image/jpg', $bucket_path, $options);

            // set the localization of user @todo put in a a part method
            $this->setUserLocale($this->user_locale);

            return array('status' => true, 'message' => 'CONNECTED', 'identity' => $this->createAuthIdentity());
        }
    }

    /**
     * Facebook[userimage]
     * Import user fb image to application
     * if user not connected with facebook call fbUserinfo and retrive-update all data
     * 
     * @param int/string $user_id (the id of user that doing the call)
     * 
     * @return array    ([status] => false, [error] => '[fbMethod[error],fbUserinfo[error]]/CANT_LOAD_CONN/NO_UPDATE', [message] => 'Facebook[userimage] - ..desc..')
     *                  ([status] => true, [message] => 'UPDATED', 'identity' => (data from $this->creatAuthIdentity))
     */
    public function fbUserimage($user_id)
    {
        if(!$this->fbUser($user_id)){
            $this->fbUserinfo('account/fbimportimage/', $user_id);
        }
        
        $profile = $this->fbMethod('me?', 'account/fbimportimage/');
        if($profile['status'] === false){
            return array('status' => false, 'error' => $profile['error'], 'message' => $profile['message']);
        } else {
            $profile = $profile['profile'];
        }
        
        if(!$this->load($user_id)){
            return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Facebook[userimage] - load(exist) failed for (user_id: {$this->user_id})"); 
        }

        // https://developers.facebook.com/docs/graph-api/reference/v2.0/user/picture
        // profile_photo based on facebook user id
        $this->poster = 'user_fb_l_'.$this->profile->fb_id_conn.'.jpg';
        $this->getImgByUrl('https://graph.facebook.com/v2.0/'.$this->profile->fb_id_conn.'/picture?type=large', '/public/files/user/image/', $this->poster);
        
        $this->profile->fb_connected = '1';
        
        // we save all in makeThumbs :)
        if(!$this->makeThumbs(null, null, null)){
            return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Facebook[userimage] - save(exist) failed for (user_id: {$this->user_id})");
        }

        return array('status' => true, 'message' => 'UPDATED', 'identity' => $this->createAuthIdentity());
    }
    
    /**
     * base64_url_decode
     * Replace string -_ with +/ and encode
     * 
     * @param type $input
     * @return type
     */
    public function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }
    
    
    /**
     * Post to facebook timeline
     * 
     * @param string $redirect If access_token need to be refresh this is the url after all call to Fb
     * 
     * @param string $message (required, the message of post)
     * 
     * @param string $user_sid (user of feed timeline, if not specified are 'me')
     * 
     * @param string $place (PAGE_ID of place from what user posts)
     * 
     * @param string $link (link in the post)
     * @param string $name (if link - name of link)
     * @param string $desc (if link - description of link)
     * @param string $pic (if link - image of link)
     * 
     * @return array (if 'status' = false have to 'error' and 'message' else staus = true , message)
     * /
    public function fbUserpost($redirect, $message, $access_token, $user_sid = null, $place = null, $link = null, $name = null, $desc = null, $pic = null){
        $post['message'] = $message;

        if($place !== null) $post['place'] = $place;

        if($link !== null) $post['link'] = $link;
        if($name !== null) $post['name'] = $name;
        if($desc !== null) $post['description'] = $desc;
        if($pic !== null) $post['picture'] = $pic;

        $user = ($user_sid === null) ? 'me' : $user_sid;

        if(!$profile = $this->fbMethodpost($user."/feed", $post, $redirect))
            return array('status' => false, 'error' => 'CURL_ERR', 'message' => $profile['message']);

        return array('status' => true, 'message' => 'POSTED');
    }*/
    
    /**
     * Facebook POST send CURL request
     * 
     * @param string $method (method and field ex: )
     * @param array $post ($post =  array(
                                'access_token' => $token,
                                'message' => $msg,
                                'name' => $title,
                                'link' => $uri,
                                'description' => $desc,
                                'picture'=>$pic,
                                'actions' => json_encode(array('name' => $action_name,'link' => $action_link))
                            );
     * 
     * 
     * 
     * @return array (if 'status' = false have to 'error' and 'message' else $result of call data)
     * /
    public function fbMethodpost($method, $post, $redirect){
        //$post['access_token'] = ;
// to set one callAction to redirect
        //if(!$this->fbAccesstoken($redirect))
            //return array('status' => false, 'error' => 'ACC_TOK', 'message' => 'False access_token');
        //elseif(!empty($sess->fb_access_token)) {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,'https://graph.facebook.com/v2.0/'.$method);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 

            $result = curl_exec($ch);

            $result = json_decode(curl_exec($ch));
            if($result === FALSE){
                curl_close($ch);
                return array('status' => false, 'error' => 'CURL_ERR', 'message' => "cURL Error: " . curl_error($ch));
            } else {
                curl_close($ch);
                return $result;
            }
        //} else
            //return false;
    }*/
}
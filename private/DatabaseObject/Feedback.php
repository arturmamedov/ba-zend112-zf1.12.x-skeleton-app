<?php
/**
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 * @version 1.0
 */
class DatabaseObject_Feedback extends DatabaseObject
{
    // db status prop
	const STATUS_DISABLED = 'DISABLED';
	const STATUS_LIVE = 'LIVE';
	const STATUS_INREVISION = 'INREVISION';
    
    // all proprieties of object
    public $id;
    public $item_id;
    public $item_type;
    public $user_id;
    public $title;
    public $feedback;
    public $vote;
    public $publich_tsk;
    public $active;

    public function __construct($db)
    {
        parent::__construct($db, DB_PREFIX.'feedback', 'id');

        $this->add('id');
        $this->add('item_id');
        $this->add('item_type');
        $this->add('user_id');
        $this->add('title');
        $this->add('feedback');
        $this->add('vote');
        $this->add('publish_ts');
        $this->add('active');

        $this->db = $db;
    }

    protected function postLoad(){
        return true;
    }

    protected function preInsert(){
        return true;
    }

    protected function postInsert(){
        return true;
    }

    protected function postUpdate(){
        return true;
    }

    protected function preDelete(){
        return true;
    }


    /**
     * Get all feedback for the passed $item(hospital, kindergarten...) $id
     * 
     * @param string $item_type (item type name)
     * @param int $id (id of item)
     * 
     * @return array array of feedback and first feedback_link for add usability
     */
    public function getFeedbacks($item_type, $id){	
        $query = "SELECT * FROM {$this->_table} WHERE item_type = '{$item_type}' AND item_id = ? ORDER BY publish_ts DESC";

        $result = $this->_db->fetchAssoc($query, $id);

        $sess = Zend_Registry::get('session');
        $date = new Zend_Date();

        $feedbacks = array();
        $feedback_link = '';
        foreach($result as $feedback){
            if(empty($feedback_link)){
                $feedbacks['feedback_link'] = $feedback_link = $feedback['id'].'-feedback';
            }
            $date->set($feedback['publish_ts']);

            $feedbacks[$feedback['id']] = array(
                'id' => $feedback['id'],
                'item_id' => $feedback['item_id'],
                'item_type' => $feedback['item_type'],
                'user_id' => $feedback['user_id'],
                'title' => $feedback['title'],
                'feedback' => $feedback['feedback'],
                'vote' => $feedback['vote'],
                'publish' => $date->get(Zend_Date::DATE_LONG, $sess->localeCode).' '.$date->get(Zend_Date::TIME_SHORT, $sess->localeCode)
            );
        }
        
        return $feedbacks;
    }

    /**
     * Has this user_id add a feedback for this item_type and item_id
     * 
     * @param string $item_type
     * @param int $item_id
     * @param int $user_id
     * 
     * @return boolean/array  arry with all data if has feedback from this user; false: not has feedback
     */
    public function hasUserFeedback($item_type, $item_id, $user_id){	
        $query = "SELECT * FROM {$this->_table} WHERE `item_type` = '{$item_type}' AND `item_id` = ? AND `user_id` = {$user_id}";

        if($result = $this->_db->fetchRow($query, $item_id)){
            return $result;
        } else {
            return false;
        }
    }

    /**
     * by Tommy Lacroix - http://www.tommylacroix.com/2008/09/10/php-design-pattern-building-a-tree/
     * Create a tree array from array with father_id and child_id
     * 
     * @param array $dataset
     * 
     * @return type
     */
    public function mapTree($dataset){
        $tree = array();

        foreach($dataset as $id=>&$node){    
            if(!$node['father_id'])
                $tree[$id] = &$node;
            else { 
                /*
                    Se ha il parent_id allora e' una risposta
                    la risposta, "il figlio", lo spostiamo nel padre
                    in questo passaggio il $node['father_id'] Array avra dentro un altro Array ['childs']['id']
                */
                $dataset[$node['father_id']]['childs'][$id] = &$node; 
            }
        }

        return $tree;
    } 

    /**
     * Add a feedback
     * 
     * @param object $item Entire item DBO
     * @param string $feedback 
     * @param int $user_id
     * @param string $title
     * @param int $vote
     * 
     * @return array
     * 
     * @throws Exception
     */
    public function addFeedback($item, $feedback, $user_id, $title, $vote){
        $feedback = trim($feedback);
        
        if(strlen($feedback) < 5){
            return array('status' => false, 'error' => 'to_short', 'message' => 'Feedback troppo corto');
        }
        
        if($this->hasUserFeedback($item->type, $item->id, $user_id)){
            return array('status' => false, 'error' => 'more_than_one', 'message' =>  'Non puoi lasciare piú di un feedback');
        }
        
        if($vote < 1){
            return array('status' => false, 'error' => 'no_vote', 'message' =>  'Non puoi inserire un feedback senza voto');
        }
        
        if($vote > 5){
            return array('status' => false, 'error' => 'no_vote', 'message' =>  'Voto non valido');
        }

        $date = new Zend_Date;

        $data = array(
            'item_id' => $item->id, 
            'item_type' => $item->type, 
            'user_id' => $user_id, 
            'title' => $title, 
            'feedback' => $feedback, 
            'publish_ts' => $date->get('YYYY-MM-dd HH:mm:ss'), 
            'vote' => $vote, 
            'status' => self::STATUS_INREVISION
        );

        if($this->_db->insert($this->_table, $data)){
            // if feedback inserted, add vote
            $item->profile->all_vote = $item->profile->all_vote + $vote;
            $item->profile->voters = $item->profile->voters + 1;
            $item->profile->true_vote = $item->profile->all_vote / $item->profile->voters;
            $item->profile->vote = round($item->profile->true_vote, 0, PHP_ROUND_HALF_UP);

            $item->save();
            
            return array('status' => true, 'id' => $this->_db->lastInsertId());
        }
        
        throw new Exception('DBO_Feedbak - addFeedback(), INSERT into a TABLE and item->profile save() faile :(');
    }
}
<?php
class DatabaseObject_Hospital extends DatabaseObject
{
    public $profile = null;
	
	const DESC_MIN_LENGHT = 100;
    const DESC_MAX_LENGHT = 1800;
    
    const IMAGES_MAX_NUM = 20;
	
	// db status prop
	const STATUS_DISABLED = 'DISABLED';
	const STATUS_LIVE = 'LIVE';
	const STATUS_INREVISION = 'INREVISION';

    public function __construct($db){
        parent::__construct($db, DB_PREFIX.'hospital', 'id');
		
		// NEW data table
        $this->add('id'); // int (univoco)
        
        $this->add('country_id'); // int 
        $this->add('region_id'); // int 
        $this->add('province_id'); // int 
        $this->add('city_id'); // int 
        
        $this->add('url'); // varchar (url per SEO) NULL
		$this->add('name'); // varchar
		$this->add('description'); // text NULL
		$this->add('poster'); // varchar NULL
        
        $this->add('user_id'); // int (id relative to user that insert)

		$this->add('status'); // bigint25
        $this->add('type'); // varchar
		
		
        $this->profile = new Profile_Hospital($db);
    }

    protected function postLoad(){
        $this->profile->setProfileId($this->getId());
        $this->profile->load();
    }

    protected function preInsert(){ 
        return true;
    }

    protected function postInsert(){
        $this->profile->setProfileId($this->getId());
        $this->profile->save(false);
		
        return true;
    }

    protected function postUpdate(){
        $this->profile->save(false);
		
        return true;
    }

    protected function preDelete(){
        $this->profile->delete();
        $this->address->delete();
		
        $this->deleteAllImages(); // delete all images from server to
        
        return true;
    }
	
	/**
	 * Transform and add field to array of arrays
	 * in example tranlate word, add data from profile in the same array
	 * its after need for be json encoded and send
	 *
	 * @param array $items array of items array for json_encode
	 * @param array $_items array of objects with profile to
	 */
	public static function jsoninHospitals($items, $_items){
		$it=1;
		foreach($items as $i => $item){
            $items[$i]['i'] = $it; $it++;
		}
		
		return json_encode($items);
    }
	
    /**
     * Get data and profile data 
     * from database in base at the passed option's array
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options  (offset, limit, order, user_id, id, active)
     * 
     * @return array of objects 
    */
    public static function GetHospitals($db, $options = array()){
        // initialize the options
        $defaults = array(
            'dataType' => 'object',
            'offset' => 0, 
            'limit'  => 0, 
            'order' => 'h.name ASC', 
            'order_field' => null, 
        );

        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        $select = self::_GetBaseQuery($db, $options);

        // set the fields to select
        $select->from(null, 'h.*');

        // set the offset, limit, and ordering of results
        if($options['limit'] > 0){
            $select->limit($options['limit'], $options['offset']);
        }
        if($options['order_field'] != null && count($options['order']) > 0){
            $select->order(new Zend_Db_Expr("FIELD({$options['order_field']}, ".join(',', $options['order']).")"));
        } else {
            $select->order($options['order']);
        }
		
        //** best debug | 
        // exit($select->assemble()); // for see a true format of query that go to db
        // fetch user data from database
        $data = $db->fetchAll($select);
		
		switch($options['dataType']){
			case'object':
				// turn data into array of DatabaseObject objects
				$items = self::BuildMultiple($db, __CLASS__, $data);

				$ids = array_keys($items);
				
				if(count($items) == 0){
					return array();
                }
                
				// load the profile data for loaded property
				$profiles = Profile::BuildMultiple($db, 'Profile_Hospital', array('id' => $ids));
				
				foreach($items as $id => $item){
					if(array_key_exists($id, $profiles) && $profiles[$id] instanceof Profile_Hospital){
						$items[$id]->profile = $profiles[$id];
					} else {
						$items[$id]->profile->setProfileId($id);
					}
				}
			break;
			case'array':
				$items = $data;
				$ids = array_keys($items);
				
				if(count($items) == 0){
					return array();
                }

				foreach($items as $id => $item){
					$items[$id]['profile'] = new Profile_Property($db);
                }
                
				// load the profile data for loaded property
				$profiles = Profile::BuildMultiple($db, 'Profile_Hospital', array('id' => $ids));
				
				foreach($items as $id => $item){
					if(array_key_exists($id, $profiles) && $profiles[$id] instanceof Profile_Hospital){
						$items[$id]['profile'] = $profiles[$id];
					} else {
						$items[$id]['profile']->setProfileId($id);
					}
				}
			break;
			case'json':
				$json_items = $data;
				// turn data into array of DatabaseObject objects
				$items = self::BuildMultiple($db, __CLASS__, $data);
				$ids = array_keys($items);
				
				if(count($items) == 0)
					return json_encode(array());
				
				// load the profile data for loaded property
				$profiles = Profile::BuildMultiple($db, 'Profile_Hospital', array('id' => $ids));
				
				foreach($items as $id => $item){
					if(array_key_exists($id, $profiles) && $profiles[$id] instanceof Profile_Hospital){
						$items[$id]->profile = $profiles[$id];
					} else {
						$items[$id]->profile->setProfileId($id);
					}
				}
				
				$items = self::jsoninPropertys($json_items, $items);
			break;
		}
        return $items;
    }

    /**
     * Get the count of hospitals that have the same otpion's
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return int
    */
    public static function GetHospitalsCount($db, $options){
        $select = self::_GetBaseQuery($db, $options);
        $select->from(null, 'count(*)');

        //exit($select->assemble()); // for see a true format of query that go to db
        
        return $db->fetchOne($select);
    }

    /**
     * This method prepare a basic DB SELECT
     * with passed array option's
     * options (user_id/s, id/s, status, type)
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return Zend_Db
    */
    private static function _GetBaseQuery($db, $options)
    {
		// lang_id - at the 2014-01-25 arent in use
        // initialize the options
        $defaults = array('user_id' => array(), 
            'id' => array(), 
            'type' => array(),
            'status' => self::STATUS_LIVE,
            
            'country_id' => null,
            'region_id' => null,
            'province_id' => null,
            'city_id' => null
        );

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        $select = $db->select();
        $select->from(array('h' => DB_PREFIX.'hospital'), array());
        
        // filter for status LIVE ...
		if(count($options['status']) > 1){
            $select->where('h.status IN (?)', $options['status']);
        } elseif(count($options['status']) > 0){
            $select->where("h.status = ?", $options['status']);
        }
        
        // filter results on specified hospitals types (if any)
        if(count($options['type']) > 1){
            $select->where('h.type IN (?)', $options['type']);
        } elseif(count($options['type']) > 0){
            $select->where("h.type = ?", $options['type']);
        }
        
        // filter results on specified hospitals ids (if any)
        if(count($options['id']) > 1){
            $select->where('h.id IN (?)', $options['id']);
        } elseif(count($options['id']) > 0){
            $select->where("h.id = ?", $options['id']);
        }
        
        // filter results on specified users ids (if any)
        if(count($options['user_id']) > 1){
            $select->where('h.user_id IN (?)', $options['user_id']);
        } elseif(count($options['user_id']) > 0){
            $select->where("h.user_id = ?", $options['user_id']);
        }
        
        return $select;
    }
	
    
    /** 
     * Control if live
     * 
     * @param int $id
     * @return boolean
    */
    public function isLive($id = null){
		if($id == null && $this->getId() < 1){
			throw new Exception('Param $id must be passed to isLive() method, or before use load() method for retrive object');
		}elseif($this->pubblicato == self::STATUS_LIVE){
			return true;
		} else {
			$query = "SELECT status FROM {$this->_table} WHERE id = '{$id}'";
			$result = $this->_db->fetchOne($query);
			if($result == self::STATUS_LIVE){
				return true;
            }
		}
		return false;
    }
	
	
	/**
     * Check if item are of user or no
     * 
     * @param int $user_id
     * 
     * @return bool
     */
    public function OfUser($user_id){
        if($user_id < 1 || $this->getId() < 1){
            return false; // 'false1'
        }
        // if master user, all are right :)
        $user = new DatabaseObject_User($this->_db);
        $select = $this->_db->select();
        $select->from($user->getTable(), 'user_type')->where('user_id = ?', $user_id);
        if($this->_db->fetchOne($select) == 'master'){
            return true;
        }
		// controll equality
		if($user_id == $this->user_id){
			return true;
        }
		return false; // 'false2'
    }
	
	/**
	 * Are this item ready to by publish
	 *
	*/
    public function ready(){
        // if yet inrevision somthing not okay
        if($this->active == self::STATUS_INREVISION){
            $logger = Zend_Registry::get('logger');
            $logger->warn('Try to publish when yet in revision status');
            return true;
        }
        
        $errors = array();
        
		//* POSTER
        //if($this->poster == null)
            //$errors[] = 'Devi inserire un immagine principale';
		
		/*
        //* DESCRIPTION
        if($this->description == null)
            $errors[] = 'Devi inserire una descrizione';
		if(strlen($this->description) < 50)
            $errors[] = 'Scrivi una descrizione più lunga perfavore';
        
        //* NAME
        if($this->name == null)
            $errors[] = 'Devi inserire un nome';
			
		// ADDRESS
		
		}
		*/
		
        
        // ERRORS OR TRUE
        if(count($errors) > 0){
            return $errors;
        }
        return true;
    }
    
    /**
     * Send property to reiwiev status for wait admin approvation
     * 
     * @return boolean
     */
    public function toRevision(){
        $logger = Zend_Registry::get('logger');
        $date = new Zend_Date();
        if($this->active == self::STATUS_INREVISION){
            $logger->warn('Try to revision when yet in revision status');
            return true;
        }
        
        $this->status = self::STATUS_INREVISION;
        $this->profile->ts_toinrev = $date->get('U');
        
        $logger->notice('Hospital toINREVISION status');
        
        $this->save();
        return true;
    }
    
    
    /**
     * Publish definitivetely the property
     * 
     * @return boolean
     */
    public function publish($admin){
        $logger = Zend_Registry::get('logger');
        if($this->status != self::STATUS_INREVISION){
            $logger->crit(sprintf('Try to publish Hospital that not are INREVISION status {id: %d , rel_id: %d , status: %s}', $this->id, $this->rel_id, $this->status));
            return false;
        }

        $this->status = self::STATUS_LIVE;
        $this->profile->count_update = (int)$this->profile->count_update + 1;
		$date = new Zend_Date();
        $this->profile->datetime_publish = $date->get('YYYY-MM-dd HH:mm:ss');
        
        $logger->notice(sprintf('Published Hospital by %s ,count update: %d ', $admin, $this->profile->count_update));
        
        $this->save();
        return true;
    }
    
    
    /**
     * Disable the property
     * 
     * @return boolean
     */
    public function disable(){
        $logger = Zend_Registry::get('logger');
        $date = new Zend_Date();
        
        $this->status = self::STATUS_DISABLED;
        $this->profile->datetime_disable = $date->get('YYYY-MM-dd HH:mm:ss');
		
        $logger->notice(sprintf('Disabled Hospital { id: %d , count update: %d}', $this->id, $this->profile->count_update));
        
        $this->save();
        return true;
    }
    
    /**
     * Prepare item for view, change readable param
     * ex: price to Zend_Locale, type A/V to readable in language
     * 
     * @param object/Zend_Translate $translate Object for translate values
     * 
     * @return boolean
     */
    public function prepareView($translare = null){
        if($this->getId()){
            // change some data before view
            $this->name = ucfirst($this->name);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * This method prepare a basic DB SELECT
     * with passed array option's
     * options (rel_id/s)
     * 
     * @param object/Zend_Db $db
     * @param array/int $options 'id' => array(1,2,3) OR id => 2
     * 
     * @return Zend_Db
    */
    public static function GetImages($db, $options)
    {
		// lang_id - at the 2014-01-25 arent in use
        // initialize the options
        $defaults = array('rel_id' => array());

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        $select = $db->select();
        $select->from(array('f' => DB_PREFIX.'files'), 'f.*');
        
        
        // filter results on specified users ids (if any)
        if(count($options['rel_id']) > 1){
            $select->where('f.rel_id = (?)', $options['rel_id']);
        } elseif($options['rel_id'] > 0){
			$select->where('f.rel_id = ?', $options['rel_id']);
        }
		$select->where("f.rel_type = 'hospital'");
		
        //** best debug | exit($select->assemble()); // for see a true format of query that go to db
        // fetch user data from database
        $data = $db->fetchAll($select);
		
		return $data;
    }
	
    /**
     * Delete poster and images of load() item
     * use this->deletePoster(); and this->deleteImage();
     * 
     * @return boolean true
     */
	public function deleteAllImages(){
		if(strlen($this->poster) > 1){
			$this->deletePoster($this->user_id, $this->getId(), $this->poster);
		}
		
		$images = self::GetImages($this->_db, array('rel_id' => $this->getId()));
		if(count($images) > 0){
			foreach($images as $image){
				$this->deleteImage($this->user_id, $this->getId(), $image['name']);
			}	
		}
		return true;
	}
    
	/**
	 * Delete property image from server and DB
	 */
	public function deleteImage($user_id, $rel_id, $file_name = NULL) {
		$path = ROOT.'/public/files/property/image/';
		$_prop = new DatabaseObject_Property($this->_db);
		$_prop->load($rel_id);
		
		if($_prop->ofUser($user_id)){
			if($file_name !== NULL){
				$file_name = $file_name;
			} else {
				return false;
			}
			// this has been customized to remove only specific images in certain user_id folders
			// you should modify that to your needs
			$file_path = $path .'/'. $file_name;
			$file_path_t = $path .'/hospital/'. $file_name;
			$success = is_file($file_path) && $file_name !== '.' && unlink($file_path);
			$success = is_file($file_path_t) && $file_name !== '.' && unlink($file_path_t);
			
			$where = "name = '{$file_name}'";
			$this->_db->delete(DB_PREFIX.'files', $where);
		}
		
		return true;
		echo json_encode($success);
	}
    
	/**
	 * Delete property poster from server and DB
	 */
	public function deletePoster($user_id, $rel_id, $file_name = NULL) {
		$path = ROOT.'/public/files/hospital/image/';
		$_prop = new DatabaseObject_Property($this->_db);
		$_prop->load($rel_id);
		
		if($_prop->ofUser($user_id)){
			if($file_name !== NULL){
				$file_name = $file_name;
			} else {
				return false;
			}
			// this has been customized to remove only specific images in certain user_id folders
			// you should modify that to your needs
			$file_path = $path .'/'. $file_name;
			$file_path_t = $path .'/thumbnail/'. $file_name;
			$success = is_file($file_path) && $file_name !== '.' && unlink($file_path);
			$success = is_file($file_path_t) && $file_name !== '.' && unlink($file_path_t);
			
			$_prop->poster = null;
			$_prop->save();
		}
		
		return true;
		echo json_encode($success);
	}
	
	/**
	 * Make thumbnails from image and save his new size to $this->profile
	 * oh oh o oh o oho oho oh ooho oho oho oho oho oho oho ho oho ho ohohoho :)
	 *
	 * [change only path: /thumbs_small || /thumbs_normal || /image && /public/file/(course.lcourse.book)/.../same_name_in.all]
	 *
	 * @param array $options array/array of options of thumbnails: w, h, path, type (default, 150, 315, small/normal, s/n)
	 *
	 * return boolean true
	 */
	public function makeThumbs($options = null){
		// initialize the options
        if($options == null){
			$options = array(	
				array('w' => 200, 'h' => 150, 'path' => 'thumbnail', 'image' => $this->poster)
			);
		} elseif(is_string($options) && !is_array($options)) {
			$options = array(	
				array('w' => 200, 'h' => 150, 'path' => 'thumbnail', 'image' => $options)
			);
		}
			
		// std opts loop, for make thumbs
		foreach($options as $opt){
			// get the current image w, h
			$arr_image_details	= GetImageSize(ROOT.DS.'public'.DS.'files'.DS.$this->type.DS.'image'.DS.$opt['image']);
			$_w	= $arr_image_details[0];
			$_h	= $arr_image_details[1];
			// aspect ratio - width more than height
			if($_w > $_h ){
				$n_w = $opt['w']; // width of new img
				$n_h = intval($_h * $n_w / $_w);
			} else {
				$n_h = $opt['h']; // height of new img
				$n_w = intval($_w * $n_h / $_h);
			}

			// i think this is for crop
			//$dest_x = intval(($opt['w'] - $n_w) / 2);
			//$dest_y = intval(($opt['h'] - $n_h) / 2);

			if($arr_image_details[2]==1){ 
				$imgt = "ImageGIF"; 
				$imgcreatefrom = "ImageCreateFromGIF";
			} elseif($arr_image_details[2]==2){
				$imgt = "ImageJPEG";
				$imgcreatefrom = "ImageCreateFromJPEG";
			} elseif($arr_image_details[2]==3){
				$imgt = "ImagePNG";
				$imgcreatefrom = "ImageCreateFromPNG";
			}
					
			if($imgt){ 
				// resize image
				$old_image = $imgcreatefrom(ROOT.DS.'public'.DS.'files'.DS.$this->type.DS.'image'.DS.$opt['image']);
				$new_image = imagecreatetruecolor($n_w, $n_h);
				imageCopyResized($new_image, $old_image, 0, 0, 0, 0, $n_w, $n_h, $_w, $_h);
				// boh
				$imgt($new_image, ROOT.DS.'public'.DS.'files'.DS.$this->type.DS.'image'.DS.$opt['path'].DS.$opt['image']);
			}
		}
	
		return true;
	}
    
    /**
     * Static function for convert text/html by passing options array
     * 
     * @param string $text
     * @param array $options (if target-blank to false): link havent the target=_blank
     *  
     * @return string The converte string
     */
    static public function convertTextToLink($text, $options = array()){
        //* initialize the default filters
        $defaults = array('target-blank' => true);
        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        ///* Url link
        if($options['target-blank']){
            // Find and replace all http/https links that are not part of an existing html anchor
            // Find and replace all naked www.links.com (without http://)
            $text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3" target="_blank">$1$3</a>', $text);//@todo pass on a mind page for analytics and more security
        } else 
            $text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3">$1$3</a>', $text); // @todo pass on a mind page for analytics and more security
        
        return $text;
    }
    
    /**
     * Static function for convert html 
     * es: a tag -in-> a tag with target=_blank
     * 
     * @param string $html
     * @param array $options (if target-blank): link receive target=_blank
     *  
     * @return string The converte string
     */
    static public function convertHtml($html, $options = array()){
        //* initialize the default filters
        $defaults = array('target-blank' => false);
        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        ///* Url link 
        if($options['target-blank']){
            $html = preg_replace("/<a(.*?)>/", "<a$1 target=\"_blank\">", $html);
            //$html = preg_replace('/(?=<a\.)(\S+)/', '<a href="http$2://$3" target="_blank">$1$3</a>', $html);//@todo pass on a site page for analytics and more security
        }
        
        return $html;
    }
}
?>
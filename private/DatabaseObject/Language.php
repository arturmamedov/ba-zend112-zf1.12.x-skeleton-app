<?php
/**
 * Model - Ragion
 * Class for work with language in database	
 */
class DatabaseObject_Language extends DatabaseObject
{	
    public function __construct($db)
    {
        parent::__construct($db, DB_PREFIX.'languages', 'id_language');
        
        $this->add('title');
        $this->add('en_title');
        $this->add('iso639code');
        $this->add('status');
    }
	
	// methods
    protected function postLoad(){
        return true;
    }
    protected function postInsert(){
        return true;
    }
    protected function postUpdate(){
        return true;
    }
    protected function preDelete(){
        return true;
    }

    /**
     * Get language data
     * from database in base at the passed option's array
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options  (offset, limit, order, country_id, id)
     * 
     * @return array/object/string array of array or object or json string (with all the fetched language)
     */
    public static function GetRegions($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'offset' => 0, 
            'limit'  => 0, 
            'order' => 'r.title ASC' 
        );

        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        $select = self::_GetBaseQuery($db, $options);

        // set the fields to select
        $select->from(null, 'r.*');
            
        // set the offset, limit, and ordering of results
        if ($options['limit'] > 0){
            $select->limit($options['limit'], $options['offset']);
        }

        $select->order($options['order']);

        // fetch user data from database
        $data = $db->fetchAll($select);		

        // turn data into array of DatabaseObject_UserCourse objects
        $items = self::BuildMultiple($db, __CLASS__, $data);

        //$ids = array_keys($items);

        if (count($items) == 0){
            return array();
        }
		
        return $items;
    }

    
    /**
     * Get the count of languages that have the same otpion's
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return int number of fetched rows
     */
    public static function GetRegionsCount($db, $options = array()){
        $select = self::_GetBaseQuery($db, $options);
        $select->from(null, 'count(*)');

        return $db->fetchOne($select);
    }

    
    /**
     * This method prepare a basic DB SELECT 
     * with passed array options
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return Zend_Db SELECT statement
     */
    private static function _GetBaseQuery($db, $options)
    {
        // initialize the options
        $defaults = array(
            'id_language' => array(),
            'title' => array()
        );

        foreach ($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        // create a query that selects from the course table
        $select = $db->select();
        $select->from(array('r' => DB_PREFIX.'languages'), array());
        
        // filter results on specified propertys id/s (if any)
        if(count($options['id_language']) > 1){
            $select->where('r.id_language IN (?)', $options['id_language']);
        } elseif(count($options['id']) > 0){
            $select->where("r.id_language = ?", $options['id_language']);
        }
        
        // filter results on specified name
        if(count($options['title']) > 1){
            $select->where('r.title IN (?)', $options['title']);
        } elseif(count($options['title']) > 0){
            $select->where("r.title = ?", $options['title']);
        }

        return $select;
    }
    
    /**
     * Get the title of language by id
     * 
     * @param int $id
     * 
     * @return string name
     */
    public static function getTitle($id){
        $db = Zend_Registry::get('db');
        $query = "SELECT title FROM `".DB_PREFIX."languages` WHERE id_language = '{$id}'";
        $result = $db->fetchOne($query);

        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    
    /**
     * Get the iso639code of language by id_language
     * 
     * @param int $id
     * 
     * @return string name
     */
    public static function getCode($id){
        $db = Zend_Registry::get('db');
        $query = "SELECT iso639code FROM `".DB_PREFIX."languages` WHERE id_language = '{$id}'";
        $result = $db->fetchOne($query);

        if($result){
            return $result;
        } else {
            return false;
        }
    }
}
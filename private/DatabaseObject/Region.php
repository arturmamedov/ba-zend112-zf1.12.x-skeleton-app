<?php
/**
 * Model - Ragion
 * Class for work with region in database	
 */
class DatabaseObject_Region extends DatabaseObject
{	
    public function __construct($db)
    {
        parent::__construct($db, DB_PREFIX.'region', 'id');

        $this->add('id');
        $this->add('country_id');
        $this->add('name');
        $this->add('description');
        $this->add('poster');
    }
	
	// methods
    protected function postLoad(){
        return true;
    }
    protected function postInsert(){
        return true;
    }
    protected function postUpdate(){
        return true;
    }
    protected function preDelete(){
        return true;
    }
	
	/**
	 * Transform and add field to array of arrays of propertys
	 * in example tranlate word, add data from profile in the same array
	 * after json encode
	 *
	 * @param array $items array of items array for json_encode
	 * @param array $_items array of objects with profile to
     * 
     * return string json endoded string
	 */
	public static function jsoninRegions($items, $_items){
		$it=1;
		
		foreach($items as $i => $item){
            $items[$i]['i'] = $it; $it++; // only add a counter to each item
		}
		
		return json_encode($items);
    }

    /**
     * Get region data
     * from database in base at the passed option's array
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options  (offset, limit, order, country_id, id)
     * 
     * @return array/object/string array of array or object or json string (with all the fetched region)
     */
    public static function GetRegions($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'dataType' => 'object', 
            'offset' => 0, 
            'limit'  => 0, 
            'order' => 'r.name ASC' 
        );

        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        $select = self::_GetBaseQuery($db, $options);

        // set the fields to select
        $select->from(null, 'r.*');
            
        // set the offset, limit, and ordering of results
        if ($options['limit'] > 0){
            $select->limit($options['limit'], $options['offset']);
        }

        $select->order($options['order']);

        // fetch user data from database
        $data = $db->fetchAll($select);		
        switch($options['dataType']){
			case'object':
				// turn data into array of DatabaseObject_UserCourse objects
				$items = self::BuildMultiple($db, __CLASS__, $data);

				$ids = array_keys($items);

				if (count($items) == 0){
					return array();
                }
			break;
			case'array':
				$items = $data;
				$ids = array_keys($items);
				
				if(count($items) == 0){
					return array();
                }
			break;
			case'json':
				$json_items = $data;
				// turn data into array of DatabaseObject objects
				$items = self::BuildMultiple($db, __CLASS__, $data);
				$ids = array_keys($items);
				
				if(count($items) == 0){
					return json_encode(array());
                }
				
				$items = self::jsoninBooks($json_items, $items);
			break;
		}
        return $items;
    }

    
    /**
     * Get the count of regions that have the same otpion's
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return int number of fetched rows
     */
    public static function GetRegionsCount($db, $options){
        $select = self::_GetBaseQuery($db, $options);
        $select->from(null, 'count(*)');

        return $db->fetchOne($select);
    }

    
    /**
     * This method prepare a basic DB SELECT 
     * with passed array options
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return Zend_Db SELECT statement
     */
    private static function _GetBaseQuery($db, $options)
    {
        // initialize the options
        $defaults = array(
            'id' => array(),
            'country_id' => array(), 
            'name' => array()
        );

        foreach ($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        // create a query that selects from the course table
        $select = $db->select();
        $select->from(array('r' => DB_PREFIX.'region'), array());
        
        // filter results on specified propertys id/s (if any)
        if(count($options['id']) > 1){
            $select->where('r.id IN (?)', $options['id']);
        } elseif(count($options['id']) > 0){
            $select->where("r.id = ?", $options['id']);
        }
        
        // filter results on specified country id/s (if any)
        if(count($options['country_id']) > 1){
            $select->where('r.country_id IN (?)', $options['country_id']);
        } elseif(count($options['country_id']) > 0){
            $select->where("r.country_id = ?", $options['country_id']);
        }
        
        // filter results on specified name
        if(count($options['name']) > 1){
            $select->where('r.name IN (?)', $options['name']);
        } elseif(count($options['name']) > 0){
            $select->where("r.name = ?", $options['name']);
        }

        return $select;
    }
}
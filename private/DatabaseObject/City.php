<?php
/**
 * Model - Ragion
 * Class for work with city in database	
 */
class DatabaseObject_City extends DatabaseObject
{	
    public function __construct($db)
    {
        parent::__construct($db, DB_PREFIX.'city', 'id');

        $this->add('id');
        $this->add('country_id');
        $this->add('region_id');
        $this->add('province_id');
        $this->add('name');
        $this->add('description');
        $this->add('poster');
    }
	
	// methods
    protected function postLoad(){
        return true;
    }
    protected function postInsert(){
        return true;
    }
    protected function postUpdate(){
        return true;
    }
    protected function preDelete(){
        return true;
    }
	
	/**
	 * Transform and add field to array of arrays of propertys
	 * in example tranlate word, add data from profile in the same array
	 * after json encode
	 *
	 * @param array $items array of items array for json_encode
	 * @param array $_items array of objects with profile to
     * 
     * return string json endoded string
	 */
	public static function jsoninCities($items, $_items){
		$it=1;
		
		foreach($items as $i => $item){
            $items[$i]['i'] = $it; $it++; // only add a counter to each item
		}
		
		return json_encode($items);
    }

    /**
     * Get city data
     * from database in base at the passed option's array
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options  (offset, limit, order, country_id, id)
     * 
     * @return array/object/string array of array or object or json string (with all the fetched city)
     */
    public static function GetCities($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'dataType' => 'object', 
            'offset' => 0, 
            'limit'  => 0, 
            'order' => 'city.name ASC' 
        );

        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        $select = self::_GetBaseQuery($db, $options);

        // set the fields to select
        $select->from(null, 'city.*');
            
        // set the offset, limit, and ordering of results
        if ($options['limit'] > 0){
            $select->limit($options['limit'], $options['offset']);
        }

        $select->order($options['order']);

        // fetch user data from database
        $data = $db->fetchAll($select);		
        switch($options['dataType']){
			case'object':
				// turn data into array of DatabaseObject_UserCourse objects
				$items = self::BuildMultiple($db, __CLASS__, $data);

				$ids = array_keys($items);

				if (count($items) == 0){
					return array();
                }
			break;
			case'array':
				$items = $data;
				$ids = array_keys($items);
				
				if(count($items) == 0){
					return array();
                }
			break;
			case'json':
				$json_items = $data;
				// turn data into array of DatabaseObject objects
				$items = self::BuildMultiple($db, __CLASS__, $data);
				$ids = array_keys($items);
				
				if(count($items) == 0){
					return json_encode(array());
                }
				
				$items = self::jsoninBooks($json_items, $items);
			break;
		}
        return $items;
    }

    
    /**
     * Get the count of citys that have the same otpion's
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return int number of fetched rows
     */
    public static function GetCitiesCount($db, $options){
        $select = self::_GetBaseQuery($db, $options);
        $select->from(null, 'count(*)');

        return $db->fetchOne($select);
    }

    
    /**
     * This method prepare a basic DB SELECT 
     * with passed array options
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return Zend_Db SELECT statement
     */
    private static function _GetBaseQuery($db, $options)
    {
        // initialize the options
        $defaults = array(
            'id' => array(),
            'country_id' => array(),
            'region_id' => array(),
            'province_id' => array(),
            'name' => array()
        );

        foreach ($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        // create a query that selects from the course table
        $select = $db->select();
        $select->from(array('city' => DB_PREFIX.'city'), array());
        
        // filter results on specified propertys id/s (if any)
        if(count($options['id']) > 1){
            $select->where('city.id IN (?)', $options['id']);
        } elseif(count($options['id']) > 0){
            $select->where("city.id = ?", $options['id']);
        }
        
        // filter results on specified country id/s (if any)
        if(count($options['country_id']) > 1){
            $select->where('city.country_id IN (?)', $options['country_id']);
        } elseif(count($options['country_id']) > 0){
            $select->where("city.country_id = ?", $options['country_id']);
        }
        
        // filter results on specified region id/s (if any)
        if(count($options['region_id']) > 1){
            $select->where('city.region_id IN (?)', $options['region_id']);
        } elseif(count($options['region_id']) > 0){
            $select->where("city.region_id = ?", $options['region_id']);
        }
        
        // filter results on specified province id/s (if any)
        if(count($options['province_id']) > 1){
            $select->where('city.province_id IN (?)', $options['province_id']);
        } elseif(count($options['province_id']) > 0){
            $select->where("city.province_id = ?", $options['province_id']);
        }
        
        // filter results on specified name
        if(count($options['name']) > 1){
            $select->where('city.name IN (?)', $options['name']);
        } elseif(count($options['name']) > 0){
            $select->where("city.name LIKE %{$options['name']}%");
        }

        return $select;
    }
}
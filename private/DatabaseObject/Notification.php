<?php
/**
 * @version 2
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 * 
 * Class with method for managing notifications
 */
class DatabaseObject_Notification extends DatabaseObject
{
    public $db;
    
    public $structure = null;
    
    const TSN = ' | HealthEye';

    public function __construct($db)
    {
        parent::__construct($db, DB_PREFIX.'notifications', 'id');
        
        $this->add('user_id');
        $this->add('structure_type');
        $this->add('structure_id');
        $this->add('url');
        $this->add('title');
        $this->add('message');
        $this->add('type');
        $this->add('start');
        $this->add('end');
        $this->add('ts_created');
        $this->add('icon');
        $this->add('active');

        $this->db = $db;
    }

    protected function postLoad(){
        return true;
    }

    protected function preInsert(){
        return true;
    }

    protected function postInsert(){
        return true;
    }

    protected function postUpdate(){
        return true;
    }

    protected function preDelete(){
        return true;
    }
    
    
    /**
     * Add a notification to user
     * note: profile[title, message] are required
     * 
     * 
     * @param int $user_id (id of user to notif)
     * @param string $url (url of notif)
     * @param string $type (type of notif normal, 'item', 'tut')
     * @param array $profile (all profile data !title, !message, !message_big, icon, item_[image/type/url/name], user_[fname/lname/identity/level], tut_[start/end/date])
     * @param array $range (range date of visibility of notif)
     * 
     * @return boolean
     * 
     * @throws Exception
     */
    public function addNotification($user_id, $url, $type, $profile, $range = null){
        $date = new Zend_Date();
        $created = $date->get('YYYY-MM-dd HH:mm:ss');
        $date->add(5);
        
        if($range == null){
            $range = array('start' => $date->get('YYYY-MM-dd HH:mm:ss'), 'end' => null);
        }
        
        if(!isset($profile['structure_type']) || !isset($profile['structure_id'])){
            $structure_type = NULL;
            $structure_id = NULL;
        } else {
            $structure_type = (string)$profile['structure_type'];
            $structure_id = (int)$profile['structure_id'];
        }
        
        // set the principal field of record
        $data = array(
            'user_id' => $user_id,
            'structure_type' => $structure_type,
            'structure_id' => $structure_id,
            'url' => $url,
            'type' => $type,
            'title' => $profile['title'],
            'message' => $profile['message'],
            'start' => $range['start'],
            'end' => $range['end'],
            'icon' => $profile['icon'],
            'active' => 1,
            'ts_created' => $created
        );
        //exit(var_dump($data));

        if(!$this->_db->insert($this->_table, $data)){
            throw new Exception(sprintf('Notification[add] - {save-fail} for: user_id: %d| mess: %s | url: %s | item_type: %d | range: %s', $user_id, $profile['message'], $url, $profile['item_type'], $range['start'].' - '.$range['end']));
        }
        
        $this->logger('notif')->notice(sprintf("Notifications-DBO [OK] - Notif added successfully! to: %s | title: (%s) | url: %s ", $user_id.': '.$profile['user_fname'], $profile['title'], $url));
        
        return true;
    }
     
    
    /**
     * Put out(toRead) a @limit last notification
     * 
     * @param int $user_id
     * 
     * @return boolean
     */
    public function outNotif($user_id){
        $where = array();

        $data = array('active' => 0);

        $where[] = 'user_id = '. $user_id;
        $where[] = "end IS NULL";
        $where[] = "active = 1";
        // @todo: disactive only seen notifications
        
        $this->_db->update($this->_table, $data, $where);
        
        $count = $this->GetNotificationsCount($this->db, array('user_id' => $user_id, 'active' => 1));
        
        if($count > 0){
            return $count;
        } else {
            return true;
        }
    }
    
    
    /**
     * According to the passed event and user options
     *  get text/html of email/notifications
     *  and if user options permit send email/nitifications
     * 
     * Receive event es: REGISTERED and control if for this event passed user wont to receive mail and notification
     *  send mail and/or notification
     *  return data for this notification and/or mail
     *  don't do anything
     * 
     * @param string $event
     * @param int/object $user if: are object work else: are int load user
     * 
     * @return array/boolean array with data of notifications/email or boolean if function send notifs to user
     */
    public function notif($event, $user, $data = array()){
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        $notifData = true;
        $translate = Zend_Registry::get('Zend_Translate');
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $range = NULL;
        $date = new Zend_Date();
		
        try{
            switch($event){
                case'OTHER-MAIL':
                    // + setUp mail
                    $sendMail = true;
                    $html = 'mail/account/new-mail.tpl';
                    $txt = 'mail/account/txt/new-mail.tpl';
                    // - generate
                    $templater = new Templater();
                    $templater->user = $user;
                    $templater->translate = $translate;
                    $templater->om_key = $data['om_key'];
                    $templater->om_id = $data['om_id'];
                    $templater->site = Zend_Registry::get('config')->site->web_uri;
                    $_txt = $templater->render($txt);
                    $_html = $templater->render($html);
                    // - mail details
                    $subject = 'Hi - are this your new mail? :)';                
                    $user_mail = $data['other_user_mail'];
                    $user_name = $user->profile->first_name .' '.$user->profile->last_name;
                    // + setUp notification
                    $sendNotif = false;
                break;
            }

            // if send_mail of user true, send him email
            if($sendMail){
                $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
			}
            // if notif of user true, send him notification
            if($sendNotif){
                $this->addNotification($user->getId(), $url, $type, $profile, $range);
			}
            return $notifData;
        } catch(Exception $e){
            $logger = Zend_Registry::get('logger');
            $logger->err(sprintf('Notifications[notif] - Something don\'t sent: {event: %s | user_id: %d | Exc: %s | ExcTrace: %s}', $event, $user->getId(), $e->getMessage(), $e->getTrace()));
        }
    }
    
    /**
     * Send a custom general mail with passed params only
     * Create Template from /mail/html_mail.tpl , /mail/txt_mail.tpl
     * 
     * @param int/DatabaseObject_User $user object of User to that we send the email if integer load him
     * @param string $subject Subject of mail
     * @param string $title Title in body of mail
     * @param string $content Text in body of mail
     * @param string $teaser
     * @param string $browser_uri uri for view email in browser
     * @param array $data additional data, not in use yet
     * 
     * @return boolean
     */
    public function customMail($user, $subject, $title, $content, $teaser, $browser_uri = null, $data = array())
    {   
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        
        $config = Zend_Registry::get('config');
        
        $info_mail = $config->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        
        
        // + setUp mail
        $html = 'mail/html_mail.tpl';
        $txt = 'mail/txt_mail.tpl';
        $site = $config->site->web_uri;
        // - generate
        $templater = new Templater();
        // header
        $templater->teaser = $teaser.$this->tsn;
        $templater->browser_url = $browser_uri;
        // must mandatory
        $templater->user = $user;
        $templater->translate = $translate;
        // content body text
        $templater->title = $title;
        $templater->content = $content;

        // footer
        $templater->_tsn = self::TSN;
        $templater->info_mail = $info_mail;
        // social 
        $templater->twitter_url = 'https://twitter.com/HealthEye_it';
        $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
        // site link
        $templater->site = $site;

        $_txt = $templater->render($txt);
        $_html = $templater->render($html);
        // - mail details
        $subject = $subject.self::TSN;
        $user_mail = $user->user_mail;
        $user_name = $user->profile->first_name .' '.$user->profile->last_name;

        $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        
        return true;
    }
    
    
    /**
     * Send a custom general mail with passed params only
     * 
     * @param int/DatabaseObject_User $user object of User to that we send the email if integer load him
     * @param string $subject Subject of mail
     * @param string $title Title in body of mail
     * @param string $content Text in body of mail
     * @param string $teaser
     * @param string $browser_uri uri for view email in browser
     * @param array $data additional data, not in use yet
     * 
     * @return boolean
     */
    public function devcustomMail($user, $subject, $title, $content, $teaser, $browser_uri = null, $data = array())
    {
        $sendMail = true; // send a email to user
        
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        
        $config = Zend_Registry::get('config');
        
        $info_mail = $config->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/html_mail.tpl';
            $txt = 'mail/txt_mail.tpl';
            $site = $config->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $teaser.$this->tsn;
            $templater->browser_url = $browser_uri;
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $title;
            $templater->content = $content;

            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = $subject.self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->devsendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    
    /**
     * Send mail with Zend_Mail
     *  setUp from details by registry and send
     * 
     * @param string $subject
     * @param string $html
     * @param string $user_mail
     * @param string $user_name
     * @param string $txt
     */
    public function sendMail($subject, $html, $user_mail, $user_name, $txt = null){
        if(strlen($user_mail) <= 3){
            return true;
        }
        
        $appconfig = Zend_Registry::get('config');
        $config = array(
            'port'      => $appconfig->smtp->port,
            'ssl'       => $appconfig->smtp->ssl,
            'auth'      => $appconfig->smtp->auth,
            'username'  => $appconfig->smtp->username,
            'password'  => $appconfig->smtp->password
        );
        $transport = new Zend_Mail_Transport_Smtp($appconfig->smtp->server, $config);
        //Zend_Mail::setDefaultTransport($transport);
        // now set up and send the e-mail
        $mail = new Zend_Mail('UTF-8');
        // get the Application from details from config
        $mail->setFrom(Zend_Registry::get('config')->email->from->noreply, Zend_Registry::get('config')->email->from->name);
        
        // set the to address and the user's full name in the 'to' line
        $mail->addTo($user_mail, $user_name);

        // set the subject and body and send the mail
        $mail->setSubject($subject);
        if($txt != null){
            $mail->setBodyText($txt, 'utf-8');
        }
        $mail->setBodyHtml($html, 'utf-8');

        // send ->
        $mail->send($transport);
        
        $this->logger('mail')->notice(sprintf("Notifications-DBO [OK] - Mail sended successfully! to: %s | subject: (%s) user: %s", $user_mail, $subject, $user_name));
        
        return true;
    }
    
    
    /**
     * Send mail with Zend_Mail
     *  setUp from details by registry and send
     * 
     * @param string $subject
     * @param string $html
     * @param string $user_mail
     * @param string $user_name
     * @param string $txt
     */
    public function devsendMail($subject, $html, $user_mail, $user_name, $txt = null){
        if(strlen($user_mail) <= 3){
            return true;
        }
        
        $appconfig = Zend_Registry::get('config');
        $config = array(
            'port'      => $appconfig->msmtp->port,
            'ssl'       => $appconfig->msmtp->ssl,
            'auth'      => $appconfig->msmtp->auth,
            'username'  => $appconfig->msmtp->username,
            'password'  => $appconfig->msmtp->password
        );
        $transport = new Zend_Mail_Transport_Smtp($appconfig->msmtp->server, $config);
        //Zend_Mail::setDefaultTransport($transport);
        // now set up and send the e-mail
        $mail = new Zend_Mail('UTF-8');
        // get the Application from details from config
        $mail->setFrom(Zend_Registry::get('config')->email->from->noreply, Zend_Registry::get('config')->email->from->name);
        
        // set the to address and the user's full name in the 'to' line
        $mail->addTo($user_mail, $user_name);

        // set the subject and body and send the mail
        $mail->setSubject($subject);
        if($txt != null){
            $mail->setBodyText($txt, 'utf-8');
        }
        $mail->setBodyHtml($html, 'utf-8');

        // send ->
        $mail->send($transport);
        
        $this->logger('mail')->notice(sprintf("Notifications-DBO [OK] - Mail sended successfully! to: %s | subject: (%s) user: %s", $user_mail, $subject, $user_name));
        
        return true;
    }
    
    /**
     * Initialize the log file in that we write errors
     * 
     * @param string $filename file name
     * 
     * @return boolean
     */
    public function logger($filename)
    {
        // Log in XML please :) http://framework.zend.com/manual/1.12/ru/zend.log.formatters.html
        $stream = new Zend_Log_Writer_Stream($this->logpath.DS.$filename.'.log');
        
        $formatter = new Zend_Log_Formatter_Xml();
        $stream->setFormatter($formatter);
        $logger = new Zend_Log();
        $logger->addWriter($stream);
        
        $this->logger = $logger;
        
        return $logger;
    }
    
    
    /**
     * Social Registered - after user registration from social account for confirm  mail
     * @from DBO_User postInsert()
     * 
     * @param object  $user Object with all feedback data and relative
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function usersocialRegistered($user, $data = array()){
        $sendMail = (strlen($user->user_mail) > 0) ? true : false; // send a email to user
        $sendNotif = true; // ever send site notification
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        $range = NULL;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('Thank you %s for choosing our resource'), $user->profile->first_name);

            $profile['title'] = $translate->translate('Registered successfully');
            $profile['icon'] = 'icon-eye-open text-primary';
            
            $profile['user_fname'] = $user->profile->first_name;
            
            $url = "/account";
            
            $type = 'user';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/social-registered.tpl';
            $txt = 'mail/account/txt/social-registered.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Registered successfully').$this->tsn;
            $templater->browser_url = $site.'mail/socialregistered';
            //$feedback_url = "<a href='{$site}write' target='_blank'>{$translate->translate('Write a review')}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Thank you for your interest');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, we wanted to thank you for registering on HealthEye'), $user->profile->first_name).'</p>'
                . '<h3>'.sprintf($translate->translate('Now you can access whenever you want with the same %s profile'), $data['social']).'</h3>';
            $templater->feedback_url = "{$site}write";
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('Registered successfully on HealthEye'));
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    
    /**
     * Social Registered - after user registration from social account for confirm  mail
     * @from DBO_User postInsert()
     * 
     * @param object  $user Object with all feedback data and relative
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function usertwitterRegistered($user, $data = array()){
        $sendMail = (strlen($user->user_mail) > 0) ? true : false; // send a email to user
        $sendNotif = true; // ever send site notification
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        $range = NULL;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('Thank you %s for choosing our resource'), $user->profile->first_name);

            $profile['title'] = $translate->translate('Registered successfully');
            $profile['icon'] = 'icon-eye-open text-primary';
            
            $profile['user_fname'] = $user->profile->first_name;
            
            $url = "/account";
            
            $type = 'user';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/user-registered.tpl';
            $txt = 'mail/account/txt/user-registered.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Registered successfully').$this->tsn;
            $templater->browser_url = $site.'mail/registered';
            //$feedback_url = "<a href='{$site}write' target='_blank'>{$translate->translate('Write a review')}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Thank you for your interest');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, we wanted to thank you for registering on HealthEye'), $user->profile->first_name).'</p>'
                . '<h3>'.sprintf($translate->translate('Now you can access whenever you want with the same %s profile or with the data below:'), $data['social']).'</h3>';
            $templater->feedback_url = "{$site}write";
            $templater->fetch_password_url = "{$site}account/fetch";
            $templater->login_url = "{$site}account/login";
            
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('Registered successfully on HealthEye'));
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    
    
    /**
     * Registered - after user registration for confirm  mail
     * @from DBO_User postInsert()
     * 
     * @param object  $user Object with all feedback data and relative
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function userRegistered($user, $data = array()){
        $sendMail = true; // send a email to user
        if(isset($data['notif']) && $data['notif'] == false){
            $sendNotif = false; // if reconfirm no send notif , only mail
        } else {
            $sendNotif = true;
        }
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        $range = NULL;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('Thank you %s for choosing our resource'), $user->profile->first_name);

            $profile['title'] = $translate->translate('Registered successfully');
            $profile['icon'] = 'icon-eye-open text-primary';
            
            $profile['user_fname'] = $user->profile->first_name;
            
            $url = "/account";
            
            $type = 'user';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/user-registered.tpl';
            $txt = 'mail/account/txt/user-registered.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Registered successfully').$this->tsn;
            $templater->browser_url = $site.'mail/registered';
            //$feedback_url = "<a href='{$site}write' target='_blank'>{$translate->translate('Write a review')}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Thank you for your interest');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, we wanted to thank you for registering on HealthEye'), $user->profile->first_name).'</p>'
                . '<h3>'.$translate->translate('Now you can access whenever you want with the data below:').'</h3>';
            $templater->feedback_url = "{$site}write";
            $templater->fetch_password_url = "{$site}account/fetch";
            $templater->login_url = "{$site}account/login";
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('Registered successfully on HealthEye'));
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    
    /**
     * Registered - after user registration for confirm  mail
     * @from DBO_User postInsert()
     * 
     * @param object  $user Object with all feedback data and relative
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function userMailconfirm($user, $data = array()){
        $sendMail = true; // send a email to user
        $sendNotif = false;
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/mail-confirm.tpl';
            $txt = 'mail/account/txt/mail-confirm.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Verify your email').$this->tsn;
            $templater->browser_url = $site.'mail/mailconfirm';
            //$feedback_url = "<a href='{$site}write' target='_blank'>{$translate->translate('Write a review')}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Verify your email');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, here is the link to confirm your email:'), $user->profile->first_name).'</p>';
            
            $templater->fetch_password_url = "{$site}account/fetch";
            $templater->login_url = "{$site}account/login";
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('Verify your email')).self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    
    /**
     * Answer Notification
     * notif user, when he receive answer
     * @from /support/answer on FP success
     * 
     * @param object/int  $user DatabaseObject_User/id user to notif
     * 
     */
    public function answerNotif($user, $translate, $quest, $data = array()){
        $sendMail = true; // send a email to user, answers are important
        $sendNotif = true; // ever send site notification
        
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $range = NULL;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('Yuor Question "%s" receive answer, check it now'), $quest->title);

            $profile['title'] = $translate->translate('We reply to your question');
            $profile['icon'] = 'icon-question text-success';
    
            $profile['user_fname'] = $user->profile->first_name;
            
            $url = "/support/question/id/{$quest->getId()}/{$quest->url}";
            $type = 'user';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/answer/receive_answer.tpl';
            $txt = 'mail/answer/txt/receive_answer.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('We reply to your question').$this->tsn;
            $templater->browser_url = $site.'/mail/watch?uid='.$user->getId().'&template='.$html;
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('We reply to your question');
            $templater->content = '<p>'.sprintf($translate->translate('Yuor Question "%s" receive answer, check it now'), $quest->title).'</p>'
                    . '<p>'
                    . "<a href='{$site}'/support/question/id/{$quest->getId()}/{$quest->url}>{$translate->translate('View answer on site')}</a>"
                    . '</p>';
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = $translate->translate('We reply to your question').self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name.' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        
        return true;
    }
    
    /**
     * Answer on Review Notification
     * notif user, when he receive answer
     * @from /support/answer on FP success
     * 
     * @param object/int  $user DatabaseObject_User/id user to notif
     * 
     */
    public function reviewanswerNotif($user, $translate, $answer, $data = array()){
        $sendMail = true; // send a email to user, answers are important
        $sendNotif = true; // ever send site notification
        
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $range = NULL;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('Your review "%s" received a response, check it now'), $data['feed_title']);

            $profile['title'] = $translate->translate('You have received a response to your review');
            $profile['icon'] = 'icon-comments text-feedback';
            
            $profile['user_fname'] = $user->profile->first_name;

            $url = "/{$data['structure_type']}/view/id/{$data['structure_id']}/{$data['structure_url']}#{$data['feed_id']}-feedback";
            $type = 'user';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/review/receive_answer.tpl';
            $txt = 'mail/review/txt/receive_answer.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('You have received a response to your review').$this->tsn;
            $templater->browser_url = $site.'/mail/watch?uid='.$user->getId().'&template='.$html;
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Response on your review');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, Your review "%s" received a response.'), $user->profile->first_name, $data['feed_title']).'</p>'
                    . '<h4>'.$translate->translate('Answer from representative').'</h4><p>'. 
                        nl2br($answer->answer)
                    . '</p>';
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('You have received a response to your review - %s'), $data['feed_title']).self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        
        return true;
    }
    
    
    /**
     * Answer on Review Notification
     * notif user, when he receive answer
     * @from /hospitalfeedbackmanage/status(managestatus) on publish LIVE status action
     * 
     * @param object  $feedback Object with all feedback data and relative
     * @param object  $translate The translate object
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function reviewapprovedNotif($feedback, $translate, $data = array()){
        $sendMail = true; // send a email to user, answers are important
        $sendNotif = true; // ever send site notification
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $range = NULL;
        
        $user = $feedback->user;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('Your review "%s" has been approved, the contribution that you give us is very important for all of us! Thank you!'), $feedback->title);

            $profile['title'] = $translate->translate('Review approved!');
            $profile['icon'] = 'icon-ok text-success';
            
            $profile['user_fname'] = $user->profile->first_name;
            
            $profile['structure_id'] = $feedback->structure->getId();
            $profile['structure_type'] = $feedback->structure->type;
            
            $url = "/{$feedback->structure->type}/view/id/{$feedback->structure->getId()}/{$feedback->structure->url}#{$feedback->getId()}-feedback";
            
            $type = 'structure';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/review/approved.tpl';
            $txt = 'mail/review/txt/approved.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Your review has been approved').$this->tsn;
            $templater->browser_url = $site.'mail/reviewapproved?uid='.$user->getId().'&fid='.$feedback->getId().'&type='.$feedback->structure->type;
            $feedback_url = "<a href='{$site}{$feedback->structure->type}/view/id/{$feedback->structure->getId()}/{$feedback->structure->url}#{$feedback->getId()}-feedback' target='_blank' title='{$feedback->structure->name} ({$feedback->publish})'>{$feedback->title}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Your review has been approved');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, we inform you that your review %s has been approved.'), $user->profile->first_name, $feedback_url).'</p>'
                . '<h3>'.$translate->translate('Thank you very much, even by the whole community HealthEye!').'</h3>';
            $templater->feedback_url = "{$feedback->structure->type}/view/id/{$feedback->structure->getId()}/{$feedback->structure->url}#{$feedback->getId()}-feedback";
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('Your review has been approved')).self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        
        return true;
    }
    
    
    /**
     * Answer on Review Notification
     * notif user, when he receive answer
     * @from /hospitalfeedbackmanage/status(managestatus) on publish LIVE status action
     * 
     * @param object  $feedback Object with all feedback data and relative
     * @param object  $translate The translate object
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function reviewnotapprovedNotif($feedback, $translate, $data = array()){
        $sendMail = true; // send a email to user, answers are important
        $sendNotif = true; // ever send site notification
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $range = NULL;
        
        $user = $feedback->user;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('We apologize but your review "%s" has not been approved'), $feedback->title);

            $profile['title'] = $translate->translate('Your review has not been approved');
            $profile['icon'] = 'icon-ban-circle text-danger';
            
            $profile['user_fname'] = $user->profile->first_name;
            
            $profile['structure_id'] = $feedback->structure->getId();
            $profile['structure_type'] = $feedback->structure->type;
            
            $profile['feedback_id'] = $feedback->getId();
            
            $url = "/{$feedback->structure->type}feedbackmanage/edit/id/{$feedback->getId()}";
            $type = 'structure';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/review/notapproved.tpl';
            $txt = 'mail/review/txt/notapproved.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Your review has not been approved').$this->tsn;
            $templater->browser_url = $site.'mail/reviewnotapproved?uid='.$user->getId().'&fid='.$feedback->getId().'&type='.$feedback->structure->type;
            $feedback_url = "<a href='{$site}{$feedback->structure->type}feedbackmanage/edit/id/{$feedback->getId()}/{$feedback->structure->getId()}' target='_blank' title='{$feedback->structure->name} ({$feedback->publish})'>{$feedback->title}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Your review has not been approved');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, we apologize but your review "%s" has not been approved, for the next reasons:'), $user->profile->first_name, $feedback_url).'</p>'
                . '<p>'.$feedback->profile->staff_message.'</p>';
            $templater->feedback_url = "{$feedback->structure->type}feedbackmanage/edit/id/{$feedback->getId()}/{$feedback->structure->getId()}";
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('We apologize. Your review has not been approved')).' '.self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        
        return true;
    }
    
    /**
     * Answer Notification
     * notif user, when he receive answer
     * @from /support/answer on FP success
     * 
     * @param object/int  $user DatabaseObject_User/id user to notif
     * 
     */
    public function fetchpassNotif($user, $translate, $data = array()){
        $sendMail = true; // send a email to user, answers are important
        $sendNotif = false; // no send site notification
        
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        
        $info_mail = Zend_Registry::get('config')->email->info;
        
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/fetchpassword.tpl';
            $txt = 'mail/account/txt/fetchpassword.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Change password').self::TSN;
            $templater->browser_url = $site.'mail/fetchpassword?uid='.$user->getId();
            $templater->letusknow_url = $site.'account/fetch?action=know&id='.$user->getId();
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Change password');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, for change your password follow the link below.'), $user->profile->first_name).'</p>';
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = $translate->translate('Change password').self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    /**
     * Answer Notification
     * notif user, when he receive answer
     * @from /support/answer on FP success
     * 
     * @param object/int  $user DatabaseObject_User/id user to notif
     * 
     */
    public function changepassNotif($user, $translate, $data = array()){
        $sendMail = true; // send a email to user, answers are important
        $sendNotif = false; // ever send site notification
        
        // if user are int load his object data
        if(is_int($user)){
            $user_id = $user;
            $user = new DatabaseObject_User($this->_db);
            $user->load($user_id);
        }
        
        $info_mail = Zend_Registry::get('config')->email->info;
        
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/changepassword.tpl';
            $txt = 'mail/account/txt/changepassword.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Password changed').self::TSN;
            $templater->browser_url = $site.'mail/changepassword?uid='.$user->getId();
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Password changed');
            $templater->content = '<p>'.sprintf($translate->translate('Hello %s, we inform you that your password was successfully changed.'), $user->profile->first_name).'</p>';
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = $translate->translate('Password changed').self::TSN;
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    /**
     * Registered - after user registration for confirm  mail
     * @from DBO_User postInsert()
     * 
     * @param object  $user Object with all feedback data and relative
     * @param array $data array with additional data
     * 
     * @return boolean
     */
    public function managerRegistered($user, $data = array()){
        $sendMail = true; // send a email to user
        $sendNotif = true;
        
        $info_mail = Zend_Registry::get('config')->email->from->email;
        $translate = Zend_Registry::get('Zend_Translate');
        $range = NULL;
        
        // if notif of user true, send him notification
        if($sendNotif){
            // + setUp notification
            $profile = $data;
            // profile data
            $profile['message'] = sprintf($translate->translate('%s your request has been taking charge, confirm your email by the link that we sent to you, we will contact you soon!'), $user->profile->first_name);

            $profile['title'] = $translate->translate('Thank you for registering');
            $profile['icon'] = 'icon-eye-open text-primary';
            
            $profile['user_fname'] = $user->profile->first_name;
            
            $url = "/pay";
            
            $type = 'user';
            
            $this->addNotification($user->getId(), $url, $type, $profile, $range);
        }
        // if send_mail of user true, send him email
        if($sendMail){
            // + setUp mail
            $html = 'mail/account/manager-registered.tpl';
            $txt = 'mail/account/txt/manager-registered.tpl';
            $site = Zend_Registry::get('config')->site->web_uri;
            // - generate
            $templater = new Templater();
            // header
            $templater->teaser = $user->profile->first_name.' '.$translate->translate('Thank you for registering').$this->tsn;
            $templater->browser_url = $site.'mail/managerregistered';
            //$feedback_url = "<a href='{$site}write' target='_blank'>{$translate->translate('Write a review')}</a>";
            // must mandatory
            $templater->user = $user;
            $templater->translate = $translate;
            // content body text
            $templater->title = $translate->translate('Thank you for registering');
            $templater->content = '<p>'.sprintf($translate->translate('%s we confirm you the correct registration and ask you to confirm your email with the link below.'), $user->profile->first_name).'</p>'
                . '<h3>'.$translate->translate('We contact you with instructions on how to manage your structure soon as possible.').'</h3>';
            $templater->feedback_url = "{$site}write";
            $templater->fetch_password_url = "{$site}account/fetch";
            $templater->login_url = "{$site}account/login";
            // footer
            $templater->_tsn = self::TSN;
            $templater->info_mail = $info_mail;
            // social 
            $templater->twitter_url = 'https://twitter.com/HealthEye_it';
            $templater->facebook_url = 'https://www.facebook.com/healtheye.it';
            // site link
            $templater->site = $site;
            
            $_txt = $templater->render($txt);
            $_html = $templater->render($html);
            // - mail details
            $subject = sprintf($translate->translate('Thank you for registering on HealthEye'));
            $user_mail = $user->user_mail;
            $user_name = $user->profile->first_name .' '.$user->profile->last_name;
            
            $this->sendMail($subject, $_html, $user_mail, $user_name, $_txt); // send Zend_Mail
        }
        return true;
    }
    
    /**
     * Get notifications for the specified options
     * 
     * @param object $db
     * @param array $options
     * 
     * @return object
     */
    public static function getNotifications($db, $options = array()){
        // initialize the options
        $defaults = array(
            'offset' => 0, 
            'limit'  => 0, 
            'order' => array('n.active DESC', 'n.ts_created DESC')
        );

        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        $select = self::_GetBaseQuery($db, $options);

        // set the fields to select
        $select->from(null, 'n.*');

        // set the offset, limit, and ordering of results
        if($options['limit'] > 0){
            $select->limit($options['limit'], $options['offset']);
        }
        
        $select->order($options['order']);
        
        // fetch user data from database
        $data = $db->fetchAll($select);
        
        // turn data into array of DatabaseObject_$notifications objects
        $notifications = self::BuildMultiple($db, __CLASS__, $data);

        if(count($notifications) == 0){
            return array();
        }
        
        foreach($notifications as $id => $item){
            if($item->type == "structure" && $item->structure_id > 0){
                switch($item->structure_type){
                    case'hospital':
                        $item->structure = new DatabaseObject_Hospital($db);
                        $item->structure->load($item->structure_id);
                    break;
                    case'kindergarten':
                        $item->structure = new DatabaseObject_Kindergarten($db);
                        $item->structure->load($item->structure_id);
                    break;
                    case'community':
                        $item->structure = new DatabaseObject_Community($db);
                        $item->structure->load($item->structure_id);
                    break;
                    case'rehab':
                        $item->structure = new DatabaseObject_Rehab($db);
                        $item->structure->load($item->structure_id);
                    break;
                    case'hospice':
                        $item->structure = new DatabaseObject_Hospice($db);
                        $item->structure->load($item->structure_id);
                    break;
                    case'medic':
                        $item->structure = new DatabaseObject_Medic($db);
                        $item->structure->load($item->structure_id);
                    break;
                }
            }
        }

        return $notifications;
    }


    public static function GetNotificationsCount($db, $options = array()){
        $select = self::_GetBaseQuery($db, $options);
        $select->from(null, 'count(*)');

        return $db->fetchOne($select);
    }


    private static function _GetBaseQuery($db, $options){
        // initialize the options
        $defaults = array(
            'user_id' => array(), 
            'ts_created' => null, 
            'visible' => 1
        );

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        // create a query that selects from the course table
        $select = $db->select();
        $select->from(array('n' => DB_PREFIX.'notifications'), array());
        
        if($options['visible'] == 1){
            $date = new Zend_Date();            
            $select->where('n.start <= ?', $date->get('YYYY-MM-dd HH:mm:ss'));
        }
            
        // select only active $notifications
        if(isset($options['active'])){
            $select->where("n.active = '{$options['active']}'");
        }
        // select only kinf of type $notifications
        if(isset($options['type'])){
            $select->where("n.type = '{$options['type']}'");
        }
        // select only of user_id/s $notifications
        if(count($options['user_id']) > 0){
            $select->where("n.user_id = ?", $options['user_id']);
        }
        // only where ts created more than ?
        if($options['ts_created'] != null){
            $select->where('n.ts_created >= ?', $options['ts_created']);
        }
        return $select;
    }
}
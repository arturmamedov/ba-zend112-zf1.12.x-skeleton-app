<?php
/**
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 * @version 1.1
 * 
 * Class of method for work with _user_ , _p-user_ and his data from Google+
 * method for register, connect, getInformation, getUsers, setData
 */
class DatabaseObject_UserGoogle extends DatabaseObject_User
{    
    // define all need variables
    var $client_id = "441749879558-qpi2abe4e6c9h0ihtralvoqu8d69qhiu.apps.googleusercontent.com";
    var $client_secret = 'b0BlrlsvM5QgvtQPcgNkvzOl';
    var $endpoint = 'https://www.googleapis.com/plus/v1/';
    
    public function __construct($db){
        parent::__construct($db);
        
        $this->social = 'Google+';
        $this->user_logger('google');
    }

    protected function preInsert(){
        parent::preInsert();
        return true;
        
    }

    protected function postLoad(){
        parent::postLoad();
        return true;
    }

    protected function postInsert(){
        parent::postInsert();
        return true;
    }

    protected function postUpdate(){
        parent::postUpdate();
        return true;
    }

    protected function preDelete(){
        parent::preDelete();
        return true;
    }
    
    /**
     * Get Google plus access token only if not set in session
     * 
     * @param string $redirect (url of the redirect, is the url of callAction, for retirive access token and then redirect)
     * 
     * @return boolean true/false
     */
    public function gpAccesstoken($redirect)
    {
        $sess = Zend_Registry::get('session');
        $sess->redirect_end = ($redirect != 'NO') ? $redirect : $sess->redirect_end;
        
        if(empty($sess->gp_access_token) || $sess->gp_access_token_expire <= time()){
            // il session code serve per capire se c'e una sessione e se non ce si crea una con oAuth di FB
            $code = (isset($_GET['code'])) ? $_GET['code'] : null;
            if(empty($code)){
                $my_url = Zend_Registry::get('config')->site->web_uri.'account/accessgp'; // $redirect
                $sess->state = md5(uniqid(rand(), TRUE)); // CSRF protection
                $dialog_url = 'https://accounts.google.com/o/oauth2/auth?redirect_uri='.$my_url.'&state='.$sess->state.'&response_type=code&client_id='.$this->client_id.'&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile';
                
                exit("<script>top.location.href='".$dialog_url."'</script>");
            }
            
            // se c'e la sessione ed e' quella della risposta da G+ allora creiamo l'access token
            if($sess->state && ($sess->state === $_REQUEST['state'])){
                $guri = "https://accounts.google.com/o/oauth2/token";

                $post_data = array('code' => $_GET['code'], 
                                'client_id' => $this->client_id,
                                'client_secret'=> $this->client_secret,
                                'redirect_uri' => Zend_Registry::get('config')->site->web_uri.'account/accessgp',
                                'grant_type' => 'authorization_code');

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $guri);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_NOBODY, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

                $params = json_decode(curl_exec($ch));
                if($params === FALSE){
                    exit( "cURL Error: " . curl_error($ch));						
                }
                
                curl_close($ch);

                $sess->gp_access_token = $params->access_token;
                $sess->gp_access_token_expire = time() + $params->expires_in;
                
                $redirect_end = $sess->redirect_end;
                $sess->redirect_end = null;
                
                exit("<script>top.location.href='".Zend_Registry::get('config')->site->web_uri.$redirect_end."'</script>");
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    /**
     * Google[methos]
     * Call with CURL GET a Google method
     * end point: $this->endpoint https://www.googleapis.com/plus/v1/
     * 
     * @param string $method (piece of url tha go be call ex: 'me/?')
     * @param string $redirect (the url of fbAccesstoken if the access_token go regenrate)
     * 
     * @return array    ([status] => false, [error] => 'ACC_TOK/CURL_ERR', [message] => 'Google[method] - ..desc..')
     *                  ([status] => true, [profile] => $profile ('all_data'))
     */
    public function gpMethod($method, $redirect)
    {
        $sess = Zend_Registry::get('session');

        if(!$this->gpAccesstoken($redirect)){
            return array('status' => false, 'error' => 'ACC_TOK', 'message' => 'Google[method] - False access_token');
        } else {
            //old $uri = 'https://www.googleapis.com/oauth2/v1/'.$method.'access_token='.$sess->gp_access_token;
            $uri = $this->endpoint.$method.'access_token='.$sess->gp_access_token;

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_NOBODY, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $profile = json_decode(curl_exec($ch));
            if($profile === FALSE){
                curl_close($ch);
                return array('status' => false, 'error' => 'CURL_ERR', 'message' => "Google[method] - cURL Error: " . curl_error($ch));
            } else {
                curl_close($ch);
                return array('status' => true, 'profile' => $profile);
            }
        }
        return false;
    }

    /**
     * Google[userinfo]
     * Retrive the user data from Google+ (call gpMethod(userinfo?))
     * And if user_mail already registered , login user
     * if user are new register himdata and login
     * 
     * @param striung       $redirect (url of page that begin the call, need to gpMethod if access_token go to be retirived)
     * @param int/string    $user_id (the id of user if call are doing by logged user for, import image and data)
     * 
     * @return array    ([status] => false, [error] => '[gpMethod[error]]/NO_MAIL/NO_REG/CANT_LOAD_REG/CANT_LOAD_CONN/NO_UPDATE', [message] => 'Google[userinfo] - ..desc..')
     *                  ([status] => true, [message] => 'REGISTERED/CONNECTED', 'identity' => (data from $this->creatAuthIdentity))
     */
    public function gpUserinfo($redirect, $user_id = null)
    {
        $profile = $this->gpMethod('people/me?', $redirect);
        
        $sess = Zend_Registry::get('session');
        if($profile['status'] === false){
            return array('status' => false, 'error' => $profile['error'], 'message' => $profile['message']);
        } else {
            $profile = $profile['profile'];
        }
        
        echo '<pre>';
        
        $cdn_path = $this->image_cloudfront_endpoint.$this->bucket_image_path;
        $bucket_path = $this->bucket.'/'.$this->bucket_image_path;
        
        $this->user_mail = (string)$profile->emails[0]->value;
        // user_mail important and go defined in any case
        /*if(strlen($this->user_mail) < 3){
            //return array('status' => false, 'error' => 'NO_MAIL', 'message' => 'Google[userinfo] - No mail retrived from google...me?');
            // not email
        }*/          

        if(!$this->existMail($this->user_mail) && $user_id === null && $this->gpUserid($profile->id) <= 0){
            // extract data from G+ profile and register him into Db
            // $this->_newPassword = $this->password = uniqid();
            $this->password = null;
            
            $date = new Zend_Date();
            
            if(!empty($profile->name->givenName)){
                $this->profile->first_name = $profile->name->givenName;
            }
            
            if(!empty($profile->name->familyName)){
                $this->profile->last_name = $profile->name->familyName;
            }
            
            if(!empty($profile->url)){
                $this->profile->gp_profile = $profile->url;
            }
            
            if(!empty($profile->id)){
                $this->profile->gp_id_conn = $profile->id;
            }
            
            if(!empty($profile->objectType)){
                $this->profile->gp_type = $profile->objectType;
            }

            if(!empty($profile->image->url)){
                $image_size = parse_url($profile->image->url, PHP_URL_QUERY);
                $image_url = str_replace('?'.$image_size, '', $profile->image->url);
                
                $this->profile->poster_name = 'user_gp_'.$this->profile->gp_id_conn.'.jpg';
            
                $this->poster = $cdn_path.$this->profile->poster_name;
                $this->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->profile->poster_name;
                $this->profile->poster_normal = $cdn_path.'normal/'.$this->profile->poster_name;            

                $this->getImgByUrl($image_url, $bucket_path, $this->profile->poster_name);
            }
            
            if(!empty($profile->tagline)){
                $this->description = $profile->tagline;
            }

            if(!empty($profile->gender)){
                $this->profile->gender = $profile->gender;
            }

            // user locale and user languages
            if(!empty($profile->language)){
                $this->user_locale = $profile->language;
                $this->language = $this->langIdByLocale($profile->language);
                $this->profile->first_language = $this->language;
                // set locale in session
                // $sess->localeCode = $this->user_locale; at the end here
            } else {
                $this->user_locale = $sess->localeCode;
                $this->language = $this->langIdByLocale($sess->localeCode);
                $this->profile->first_language = $this->language;
            }
            // timezone only from our timezone for google
            $this->time_zone = $sess->timeZone;

            $this->user_active = 1;
            $this->user_type = 'user';
            
            $this->ts_created = $date->get('YYYY-MM-dd HH:mm:ss');
            $this->ts_last_online = $date->get('YYYY-MM-dd HH:mm:ss');
            $this->ts_last_login = $date->get('YYYY-MM-dd HH:mm:ss');
           
            $uidentity = 'g-p'.$date->get('B');
            $date->set('00:00:00', Zend_Date::TIMES);
            $uidentity = $date->get('U').$uidentity; 
            if($this->existIdentity($uidentity)){
                $logger = Zend_Registry::get('logger');
                $logger->warn('Usergp[Registration] - Equal identity :O {identity: '.$uidentity);
                $this->identity = $uidentity.'r'.rand(0, 1000);
            } else {
                $this->identity = $uidentity;
            }
            
            // for google connected and autoposting
            $this->profile->gp_connected = '1';
            $this->profile->gp_autopost = '1';
            
            // for future API call
            $this->profile->gp_access_token = $sess->gp_access_token;
            $this->profile->gp_access_token_expire = $sess->gp_access_token_expire;
            $acln = strlen($sess->gp_access_token);
            if($acln > 250){
                $this->logger->crit("AccessToken size to much for varchar 255 lenght: {$acln} value: ".$sess->gp_access_token);
            }
            
            if(!$this->save()){
                 return array('status' => false, 'error' => 'NO_REG', 'message' => "Google[userinfo] - save(new) failed for (user_id: {$this->getId()}, user_mail: {$this->user_mail})");
            }

            // initialize the options
            $options = array(
                array('resize' => 'fit', 'w' => 100, 'h' => 100, 'path' => 'thumbnail', 'image' => $this->profile->poster_name),
                array('resize' => 'height', 'w' => 220, 'h' => 220, 'path' => 'normal', 'image' => $this->profile->poster_name)
            );
            $this->makeThumbs('image/jpg', $bucket_path, $options);
            
            // set the localization of user @todo put in a a part method
            $this->setUserLocale($this->user_locale);

            return array('status' => true, 'message' => 'REGISTERED', 'identity' => $this->createAuthIdentity());
        } else {
            //control if user yet logged with social and login if true or rec all data that not yet in DB
            if($user_id !== null){
                $this->load($user_id);
            } elseif($this->gpUserid($profile->id) > 0){
                if(!$this->load($this->gpUserid($profile->id))){
                    return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Google[userinfo] - load(exist) failed for (user_mail: {$this->user_mail})");
                }
            } else {
                // get user id by mail and load user
                $u_id = $this->useridofMail($this->user_mail);
                if($u_id > 0){
                    $this->load($u_id);
                } else {
                    return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Google[userinfo] - load(exist) failed for (user_mail: {$this->user_mail})");
                }
            }
            
            if($this->profile->gp_id_conn == null && !empty($profile->id)){
                $this->profile->gp_id_conn = (string)$profile->id;
                $this->profile->gp_autopost = '1';
            }
            
            if($this->profile->first_name == null && !empty($profile->name->givenName)){
                $this->profile->first_name = (string)$profile->name->givenName;
            }
            
            if($this->profile->last_name == null && !empty($profile->name->familyName)){
                $this->profile->last_name = (string)$profile->name->familyName;
            }
            
            if($this->profile->gp_profile == null && !empty($profile->url)){
                $this->profile->gp_profile = $profile->url;
            }
            
            if($this->description == null && !empty($profile->tagline)){
                $this->description = $profile->tagline;
            }
            
            if(!empty($profile->image->url)){
                $image_size = parse_url($profile->image->url, PHP_URL_QUERY);
                $image_url = str_replace('?'.$image_size, '', $profile->image->url);
                
                $this->profile->poster_name = 'user_gp_'.$this->profile->gp_id_conn.'-'.time().'.jpg';
            
                $this->poster = $cdn_path.$this->profile->poster_name;
                $this->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->profile->poster_name;
                $this->profile->poster_normal = $cdn_path.'normal/'.$this->profile->poster_name;            

                $this->getImgByUrl($image_url, $bucket_path, $this->profile->poster_name);
            }

            if($this->profile->gender == null && !empty($profile->gender)){
                $this->profile->gender = (string)$profile->gender;
            }
            
            // for google connected and autoposting
            $this->profile->gp_connected = '1';

            // for future API call
            $this->profile->gp_access_token = $sess->gp_access_token;
            $this->profile->gp_access_token_expire = $sess->gp_access_token_expire;
            $acln = strlen($sess->gp_access_token);
            if($acln > 250){
                $this->logger->crit("AccessToken size to much for varchar 255 lenght: {$acln} value: ".$sess->gp_access_token);
            }
            
            if(!$this->save()){
                return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Google[userinfo] - save(exist) failed for (user_id: {$this->user_id}, user_mail: {$this->user_mail})");
            }

            // initialize the options
            $options = array(
                array('resize' => 'fit', 'w' => 100, 'h' => 100, 'path' => 'thumbnail', 'image' => $this->profile->poster_name),
                array('resize' => 'height', 'w' => 220, 'h' => 220, 'path' => 'normal', 'image' => $this->profile->poster_name)
            );
            $this->makeThumbs('image/jpg', $bucket_path, $options);
            
            // set the localization of user @todo put in a a part method
            $this->setUserLocale($this->user_locale);

            return array('status' => true, 'message' => 'CONNECTED', 'identity' => $this->createAuthIdentity());
        }
    }

    /**
     * Google[userimage]
     * Import user G+ image to application
     * if user not connected with google call fbUserinfo and retrive-update all data
     * 
     * @param int/string $user_id (the id of user that doing the call)
     * 
     * @return array    ([status] => false, [error] => '[fbMethod[error],fbUserinfo[error]]/CANT_LOAD_CONN/NO_UPDATE', [message] => 'Google[userimage] - ..desc..')
     *                  ([status] => true, [message] => 'UPDATED', 'identity' => (data from $this->creatAuthIdentity))
     */
    public function gpUserimage($user_id)
    {
        if(!$this->gpUser($user_id)){
            $this->gpUserinfo('account/gpimportimage/', $user_id);
        }

        $profile = $this->gpMethod('people/me', 'account/gpimportimage/');
        if($profile['status'] === false){
            return array('status' => false, 'error' => $profile['error'], 'message' => $profile['message']);
        } else {
            $profile = $profile['profile'];
        }

        if(!$this->load($user_id)){
            return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Google[userimage] - load(exist) failed for (user_id: {$this->user_id})"); 
        }

        if(!empty($profile->picture)){
            $this->profile->profile_photo = 'gp_'.$this->profile->gp_id_conn.'-'.time().'.jpg';
            $this->getImgByUrl($profile->picture, '/public/file/account/image/', $this->profile->profile_photo);
            
            $image_size = parse_url($profile->image->url, PHP_URL_QUERY);
            $image_url = str_replace('?'.$image_size, '', $profile->image->url);

            $this->profile->poster_name = 'user_gp_'.$this->profile->gp_id_conn.'.jpg';

            $this->poster = $cdn_path.$this->profile->poster_name;
            $this->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->profile->poster_name;
            $this->profile->poster_normal = $cdn_path.'normal/'.$this->profile->poster_name;            

            $this->getImgByUrl($image_url, $bucket_path, $this->profile->poster_name);
        }
        
        $this->profile->gp_connected = '1';
         
        if(!$this->save()){
            return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Google[userimage] - save(exist) failed for (user_id: {$this->user_id})");
        }

        return array('status' => true, 'message' => 'UPDATED', 'identity' => $this->createAuthIdentity());
    }
}
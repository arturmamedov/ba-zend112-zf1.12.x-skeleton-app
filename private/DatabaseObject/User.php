<?php
/**
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 * @version 1.1
 * 
 * Class of method for work with _user_ , _p-user_ and his data
 * method for register, connect, getInformation, getUsers, setData, searchFor _user_/s
 */
class DatabaseObject_User extends DatabaseObject
{
    static $userTypes = array(
        'user' => 'User', 
        'puser' => 'Manager', 
        'master' => 'Administrator', 
        'reviewstaff' => 'Review Staff',
        'staffquestion' => 'Support Staff',
        'staff' => 'Staff'
    );

    public $profile = null;
    public $social = null;
    public $gmailv = 1;
    public $om_key;
    public $om_id;
    
    public $tsn = 'HealthEye';
	
	const STATUS_DISABLED = 0;
	const STATUS_LIVE = 1;

    public $logger;
    
    /**
     * S3 Bucket name
     * @var string
     */
    public $bucket = 'healtheye';
    /**
     * Path in S3 bucket to images
     * have thumbnail/ and normal/ to 
     * 
     * @var string
     */
    public $bucket_image_path = 'images/users/';
    /**
     * Default image for poster
     * @var string
     */
    public $poster_default = '/public/files/user/image/default.png';
    public $poster_default_thumbnail = '/public/files/user/image/thumbnail/default.png';
    
    public function __construct($db){
        parent::__construct($db, DB_PREFIX.'users', 'user_id');
		
        $this->add('user_id');
        
        $this->add('identity');
        $this->add('user_mail');
        $this->add('password');
        
        $this->add('user_type');
        
        $this->add('poster');
        $this->add('description');
        
        $this->add('language');
        $this->add('user_locale');
        $this->add('time_zone');
        
        $this->add('ts_created');
        $this->add('ts_last_login');
        $this->add('ts_last_online');
        
        $this->add('last_ip');
        $this->add('user_active');
		
        $this->profile = new Profile_User($db);
        
        $this->user_logger('user');
    }

    protected function preInsert(){ 
        // IP address
        //if it is a shared client
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        //elseif Is it a proxy address
        } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        $ip = ip2long($ip); // to int
        $this->last_ip = $ip;
        
        return true;
    }

    protected function postLoad(){
        $this->profile->setUserId($this->getId());
        $this->profile->load();
    }

    protected function postInsert(){
        // before all , the data of profile !!! ;)
        $this->profile->setUserId($this->getId());
        $this->profile->save(false);
        
        if($this->social == null){
            // Genera l'orario e la chiave della conferma e-mail
            $this->profile->confirm_key_ts = time();
            $this->profile->confirm_key = ($this->getId().uniqid());
            
            $this->profile->save(false);
            
            if($this->user_type == 'user'){
                // Spediamo la e-Mail per attivare l'account user_active
                $notif = new DatabaseObject_Notification($this->_db);
                $notif->userRegistered($this);
            } elseif($this->user_type == 'puser'){
                // Spediamo la e-Mail per attivare l'account al MANAGER
                $notif = new DatabaseObject_Notification($this->_db);
                $notif->managerRegistered($this);
            }
        } elseif($this->social == 'Twitter') {
            // Genera l'orario e la chiave della conferma e-mail
            $this->profile->confirm_key_ts = time();
            $this->profile->confirm_key = ($this->getId().uniqid());
            
            $this->profile->save(false);
            
            $notif = new DatabaseObject_Notification($this->_db);
            $notif->usertwitterRegistered($this, array('social' => $this->social));
        } else {
            $notif = new DatabaseObject_Notification($this->_db);
            $notif->usersocialRegistered($this, array('social' => $this->social));
        }
        

        // set default photo profile if not isset one
        /*if(!$this->poster){
            //$val = rand(0, 9);
            //if($val==0)
                $this->poster = 'default.png';
            //else
                //$this->profile->profile_photo = $val.'-default.png';
        }
        if(!$this->profile->small_photo){
            $val = rand(0, 9);
            if($val==0)
                $this->profile->small_photo = 'default30x30.png';
            else
                $this->profile->small_photo = $val.'-default30x30.png';
        }/* */
        

        return true;
    }

    protected function preUpdate(){
        $date = new Zend_Date();
        $this->ts_last_online = $date->get('YYYY-MM-dd HH:mm:ss');
                
        return true;
    }
    
    protected function postUpdate(){
        $this->profile->save(false);
        return true;
    }

    protected function preDelete(){
        $user_id = $this->getId();
        $user_name = $this->profile->first_name;
        $user_mail = $this->user_mail;

        // image delete
        $this->deleteposter();
        $this->deletAllReviews();
        
        $notifications = DatabaseObject_Notification::getNotifications($this->_db, array('user_id' => $this->getId(), 'visible' => null));
        foreach($notifications as $id => $ah){
            $notif = new DatabaseObject_Notification($this->_db);
            $notif->load($id);
            $notif->delete();
        }

        $this->profile->delete();

        $this->logger->notice(sprintf('User DELETED - user_id: %d | user: %s | user_mail: %s', $user_id, $user_name, $user_mail));
        return true;
    }

    /**
     * Special set for Users
     * 
     * @param string $name
     * @param string $value
     * 
     * @return void
     */
    public function __set($name, $value)
    {
        switch ($name) {
            case'password':
                if($value == null){
                    $value = null;
                } else {
                    $value = md5($value);
                }
                break;
            case'user_type':
                if(!array_key_exists($value, self::$userTypes))
                    $value = 'user';
                break;
            /*case'user_mail':
                    $value = strtolower($value);
                break;*/
        }
        return parent::__set($name, $value);
    }
    
    /**
     * Switch all users info to DELETE status
     * 
     * @return boolean
     */
    public function statusDelete()
    {
        $options = array('user_id' => $this->getId());
        
        $community_feedback = new DatabaseObject_CommunityFeedback($this->_db);
        $community_feedback->userDeleting();
        
        $this->logger->notice(sprintf('User DELETED - user_id: %d | user: %s | user_mail: %s', $user_id, $user_name, $user_mail));
        return true;
    }
    
    /**
     * Initialize the log file in that we write errors
     * 
     * @param string $filename file name
     * 
     * @return boolean
     */
    public function user_logger($filename)
    {
        // Log in XML please :) http://framework.zend.com/manual/1.12/ru/zend.log.formatters.html
        $stream = new Zend_Log_Writer_Stream($this->logpath.DS.$filename.'.log');
        
        $formatter = new Zend_Log_Formatter_Xml();
        $stream->setFormatter($formatter);
        $logger = new Zend_Log();
        $logger->addWriter($stream);
        
        $this->logger = $logger;
        
        return true;
    }

    /**
     * Load user by his identity(username), : url/nickname
     * 
     * @param string $identity
     * 
     * @return object
     */
    public function loadByIdentity($identity)
    {
        if(!$identity){
            return false;
        }
        $query = sprintf("SELECT %s FROM %s WHERE identity='{$identity}'", join(', ', $this->getSelectFields()),   $this->_table);

        return $this->_load($query);
    }
    
    
    /**
     * Create $identity object and set his data
     * ex: $identity->user_mail etc. 
     * (see CustomController for see usage : 
     * $this->identity in Sontrollers and $identity in Views)
     * 
     * @return \stdClass 
     */
    public function createAuthIdentity()
    {
        // It's saved data in one object, are the most used data for tis user ex: @user_id was used in a lot of method
        $identity = new stdClass;
        $identity->user_id = $this->getId();
        $identity->user_mail = $this->user_mail;
        $identity->identity = $this->identity;
        
        $identity->user_locale = $this->user_locale;
        $identity->iso_lang = $this->langIsoById($this->language);
        
        $identity->user_active = $this->user_active;
		$identity->user_type = $this->user_type; // master, user, puser
		$identity->language = $this->language;

        $identity->first_name = $this->profile->first_name;
        $identity->last_name = $this->profile->last_name; // yousure

        $identity->poster = ($this->poster != null) ? $this->poster : $this->poster_default;
        $identity->poster_thumbnail = ($this->poster != null) ? $this->profile->poster_thumbnail : $this->poster_default_thumbnail;
        
        $identity->ts_last_online = $this->ts_last_online;
        $identity->ts_last_login = $this->ts_last_login;
        
        //$identity->small_photo = $this->small_logo;
        
        $identity->structure_type = $this->profile->structure_type;
        $identity->structure_id = $this->profile->structure_id;

        $session = Zend_Registry::get('session');
        // for Facebook API call
        if(empty($session->fb_access_token)){
            $session->fb_access_token = ($this->profile->fb_access_token != null) ? $this->profile->fb_access_token : '';
        }
        if(empty($session->fb_access_token_expire)){
            $session->fb_access_token_expire = ($this->profile->fb_access_token_expire != null) ? $this->profile->fb_access_token_expire : '';
        }
        
        return $identity;
    }

    public function loginSuccess()
    {
        $message = sprintf('Login - Successful (user_mail: %s | user_id: %d | from: %s)', $this->user_mail, $this->getId(), $_SERVER['REMOTE_ADDR']);
        $this->logger->notice($message);
        
        $session = Zend_Registry::get('session');
        if(strlen($session->uniq_review) > 0){
            $nothing = false;
            switch($session->uniq_review_type){
                case'hospital':
                    $feedback = new DatabaseObject_HospitalFeedback($this->_db);
                break;
                case'kindergarten':
                    $feedback = new DatabaseObject_KindergartenFeedback($this->_db);
                break;
                case'hospice':
                    $feedback = new DatabaseObject_HospiceFeedback($this->_db);
                break;
                case'community':
                    $feedback = new DatabaseObject_CommunityFeedback($this->_db);
                break;
                case'rehab':
                    $feedback = new DatabaseObject_RehabFeedback($this->_db);
                break;
                case'medic':
                    $feedback = new DatabaseObject_MedicFeedback($this->_db);
                break;
                default:
                    $nothing = true;
                break;
            }
            if(!$nothing){
                $feedback->loadByUniq($session->uniq_review);
            
                $feedback->user_id = $this->getId();
                $feedback->language_id = $this->language;
                $feedback->status = DatabaseObject_HospitalFeedback::STATUS_INREVISION;
                $feedback->uniq = NULL;
                
                $feedback->save();
                $feedback->markImage($this->profile->first_name.' '.$this->profile->last_name);
                
                unset($session->uniq_review);
                unset($session->uniq_review_type);
            
                return array('after' => 'createfeedback', 'url' => "/u/{$this->identity}/review/inrevision");
            }
        } 
        elseif(strlen($session->uniq_addstructure) > 0){
            $nothing = false;
            switch($session->uniq_addstructure_type){
                case'hospital':
                    $structure = new DatabaseObject_Hospital($this->_db);
                break;
                case'kindergarten':
                    $structure = new DatabaseObject_Kindergarten($this->_db);
                break;
                case'hospice':
                    $structure = new DatabaseObject_Hospice($this->_db);
                break;
                case'community':
                    $structure = new DatabaseObject_Community($this->_db);
                break;
                case'rehab':
                    $structure = new DatabaseObject_Rehab($this->_db);
                break;
                case'pharmacy':
                    $structure = new DatabaseObject_Pharmacy($this->_db);
                break;
                case'medic':
                    $structure = new DatabaseObject_Medic($this->_db);
                break;
                default:
                    $nothing = true;
                break;
            }
            if(!$nothing){
                $structure->loadByUniq($session->uniq_addstructure);

                $structure->profile->add_user_id = $this->getId();
                $structure->profile->uniq = NULL;

                $structure->save();

                unset($session->uniq_addstructure);
                unset($session->uniq_addstructure_type);

                $this->profile->structure_id = $structure->getId();
                $this->profile->structure_type = $structure->type;

                // only array with actions, maybe not implemented look to controller, to welcome page for manager ... to payment page
                return array('after' => 'addstructure', 'url' => "/u/{$this->identity}");
            }
        } elseif(strlen($session->uniq_question) > 0){
            $quest = new DatabaseObject_Question($this->_db);
            $quest->loadByUniq($session->uniq_question);
            
            $quest->user_id = $this->getId();
            $quest->language = $this->language;
            $quest->status = DatabaseObject_Question::UPRIVATE;
            $quest->uniq = NULL;
    
            $quest->save();
            unset($session->uniq_question);
        }
        
        $date = new Zend_Date();
        $this->ts_last_login = $date->get('YYYY-MM-dd HH:mm:ss');
        
        $this->save();
        
        return true;
    }

    public function LoginFailure($user_mail, $code = '')
    {
        switch($code){
            case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                $reason = 'Account not exist';
                break;
            case Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS:
                $reason = 'Account ambiguos :O';
                break;
            case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                $reason = 'Password not valid';
                break;
            default:
                $reason = 'No listed issue';
        }

        $message = sprintf('Login - Failed (message: %s | user_auth: %s | from: %s)', $reason, $user_mail, $_SERVER['REMOTE_ADDR']);
        $this->logger->warn($message);
    }


    /**
     * Confirm for the registration mail
     * 
     * @todo Quanti giorni ha per attivarlo ?
     * 
     * @param string $key
     * 
     * @return boolean true/false
     */
    public function confirmMail($key)
    {
        // Controllo se sono presenti nel database
        if(!isset($this->profile->confirm_key) || !isset($this->profile->confirm_key_ts)){
            return false;
        }

        // Controllo se la chiave e corretta
        if($this->profile->confirm_key != $key){
            return false;
        }

        // Tutto va bene, attiviamo l'account
        // Aggiorniamo active e attiviamo l'utente impostandolo a 1 
        $this->user_active = 1;

        // 09-2014 teniamoci le key per un prossimo click al link per non generare errori inutili
        //unset($this->profile->confirm_key);
        //unset($this->profile->confirm_key_ts);
        
        // Infine salviamo tutto
        $this->save();
        
        $auth = Zend_Auth::getInstance();
        $auth->getStorage()->write($this->createAuthIdentity());

        return true;
    }
    
    /**
     * Confirm the add new mail 
     * 
     * @param string $key
     * @return boolean true/false
     */
    public function confirmNewMail($id, $key)
    {
        $mail = $this->otherMail(NULL, NULL, $id);
        //exit($mail[0]['om_confirm_key']);
        // Controllo se sono presenti nel database
        if(!isset($mail[0]['om_confirm_key']) || !isset($mail[0]['om_confirm_ts']))
            return 'La tua mail e stata confermata';

        if($this->user_id != $mail[0]['user_id'])
            return 'Qualcosa e andato storto :(';
        
        // Controllo se la chiave e corretta
        if($mail[0]['om_confirm_key'] != $key)
            return 'La tua conferma non e valida, riprova copiando il link';

        // Tutto va bene, attiviamo la nuova mail
        // Aggiorniamo active e attiviamo l'utente impostandolo a 1 
        $data = array(
            'active' => '1',
            'om_confirm_key' => NULL,
            'om_confirm_ts' => NULL
        );


        if(!$id = $this->_db->update(DB_PREFIX.'users_mail', $data))
            return false;

        return true;
    }
            
    public function fetchPassword()
    {
        // if user not in db false
        if(!$this->isSaved()){
            return false;
        }
        
        // generate new password properties
        $this->_newPassword = uniqid();
        $this->profile->new_password = md5($this->_newPassword);
        $this->profile->new_password_ts = time();
        $this->profile->new_password_key = md5(uniqid().$this->getId().$this->_newPassword);

        // save new password to profile and send e-mail
        $this->profile->save();

        // IP address
        //if it is a shared client
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        //elseif Is it a proxy address
        } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        $this->logger->notice(sprintf('Password change [REQUESTED] - Username: %s | user_id: %d | user: %s | user_mail: %s | IP: %s ', $this->identity, $this->getId(), $this->profile->first_name.' '.$this->profile->last_name, $this->user_mail, $ip));
        
        return true;
    }

    public function confirmNewPassword($key)
    {
        // check that valid password reset data is set
        if(!isset($this->profile->new_password) || !isset($this->profile->new_password_ts) || !isset($this->profile->new_password_key)){
            return false;
        }

        // check if the password is being confirm within a day
        if(time() - $this->profile->new_password_ts > 86400){
            return false;
        }

        // check that the key is correct
        if($this->profile->new_password_key != $key){
            return false;
        }

        // everything is valid, now update the account to use the new passwor
        // bypass the local setter as new_password is already an md5
        // change password not set generated parent::__set('password', $this->profile->new_password);

        // cut the var out for no error in future with that link
        unset($this->profile->new_password);
        unset($this->profile->new_password_ts);
        unset($this->profile->new_password_key);

        // finally, save the updated user record and the updated profile
        if($this->save()){
            // IP address
            //if it is a shared client
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                $ip=$_SERVER['HTTP_CLIENT_IP'];
            //elseif Is it a proxy address
            } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip=$_SERVER['REMOTE_ADDR'];
            }
            
            $this->logger->notice(sprintf('Password changed [Ok] - Username: %s | user_id: %d | user: %s | user_mail: %s | IP: %s ', $this->identity, $this->getId(), $this->profile->first_name.' '.$this->profile->last_name, $this->user_mail, $ip));
            
            $auth = Zend_Auth::getInstance();
            $auth->getStorage()->write($this->createAuthIdentity());
            
            return true;
        }
        
        return false;
    }
    
    /**
     * If user dontrequest a new password we delete the field for this request
     * 
     * @return boolean
     */
    public function deleteConfirmPassword()
    {
        // cut the var out for no error in future with that link
        unset($this->profile->new_password);
        unset($this->profile->new_password_ts);
        unset($this->profile->new_password_key);

        // finally, save the updated user record and the updated profile
        if($this->save()){
            // IP address
            //if it is a shared client
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                $ip=$_SERVER['HTTP_CLIENT_IP'];
            //elseif Is it a proxy address
            } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip=$_SERVER['REMOTE_ADDR'];
            }
            
            $this->logger->notice(sprintf('Password change [WRONG] - Username: %s | user_id: %d | user: %s | user_mail: %s | IP: %s ', $this->identity, $this->getId(), $this->profile->first_name.' '.$this->profile->last_name, $this->user_mail, $ip));
            
            return true;
        }
        
        return false;
    }
	
	
	/**
	 * Transform and add field to array of arrays of propertys
	 * in example tranlate word, add data from profile in the same array
	 * its after need for be json encoded and send
	 *
	 * @param array $items array of items array for json_encode
	 * @param array $_items array of objects with profile to
	 */
	public static function jsoninUsers($items, $_items)
    {
		$it=1;
		
		foreach($items as $i => $item){
            $items[$i]['i'] = $it; $it++;
			
			if($item['user_active'] == self::STATUS_LIVE)
				$items[$i]['status_lang'] = 'Attivo';
			elseif($item['user_active'] == self::STATUS_DISABLED)
				$items[$i]['status_lang'] = 'DisAttivo';
						
			$items[$i]['first_name'] = htmlentities(FormProcessor::cleanHtml($_items[$item['user_id']]->profile->first_name, 0));
			$items[$i]['last_name'] = htmlentities(FormProcessor::cleanHtml($_items[$item['user_id']]->profile->last_name, 0));
			//$items[$i]['description'] = htmlentities(FormProcessor::cleanHtml($items[$i]['description'], 0));
			$items[$i]['description'] = ''; // for more performace !important
			$items[$i]['password'] = ''; // no password please
		}
		
		return json_encode($items);
    }
	

    /**
     * Get users data and users profile data 
     * from database in base at the passed option's array
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options  (offset, limit, order, user_id, id, active)
     * 
     * @return object (with all the fetched courses)
     */
    public static function GetUsers($db, $options = array())
    {
        // initialize the options
        $defaults = array(
            'offset' => 0, 
            'limit'  => 0, 
            'order' => 'u.ts_created DESC', 
            'order_field' => null
        );

        foreach ($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        $select = self::_GetBaseQuery($db, $options);
        // set the fields to select
        $select->from(null, 'u.*');

        // set the offset, limit, and ordering of results
        if($options['limit'] > 0){
            $select->limit($options['limit'], $options['offset']);
        }
        
        if(count($options['order']) > 0 && $options['order_field'] != null){
            $select->order(new Zend_Db_Expr("FIELD({$options['order_field']}, ".join(',', $options['order']).")"));
        } else {
            $select->order($options['order']);
        }
        
        //exit($select->assemble()); // for see a true format of query that go to db
        
        // fetch user data from database
        $data = $db->fetchAll($select);
		
		
        // turn data into array of DatabaseObject_User objects
        $users = parent::BuildMultiple($db, __CLASS__, $data);

        if(count($users) == 0){
            return array();
        }
                
        $user_ids = array_keys($users);

        // load the profile data for loaded posts
        $profiles = Profile::BuildMultiple($db, 'Profile_User', array('user_id' => $user_ids));


        foreach ($users as $user_id => $user){
            if(array_key_exists($user_id, $profiles) && $profiles[$user_id] instanceof Profile_User){
                $users[$user_id]->profile = $profiles[$user_id];
            } else {
                $users[$user_id]->profile->setUserId($user_id);
            }
        }
		
        return $users;
    }

    /**
     * Get the count of users that have the same otpion's
     * _GetBaseQuery are use for database SELECT
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return int
     */
    public static function GetUsersCount($db, $options = array())
    {
        $select = self::_GetBaseQuery($db, $options);
        if(isset($options['count_param']))
            $select->from(null, "count({$options['count_param']})");
        else
            $select->from(null, 'count(*)');

        //exit($select->assemble()); // for see a true format of query that go to db
        
        return $db->fetchOne($select);
    }
    
    /**
     * This method prepare a basic DB SELECT from 
     * with passed array option's
     * 
     * @param object/Zend_Db $db
     * @param array $options
     * 
     * @return Zend_Db
     */
    private static function _GetBaseQuery($db, $options)
    {
        // initialize the options
        $defaults = array(
            'user_id' => array(), 
            'user_active' => array(), 
            'user_type' => array(),
            
            'first_name' => '',
            'last_name' => '',
            
            'from_ts' => '', 
            'to_ts' => ''
        );

        foreach ($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }

        // create a query that selects from the users table
        $select = $db->select();
        $select->from(array('u' => DB_PREFIX.'users'), array());
        
        // filter for status DRAFT, LIVE ...
        if(count($options['user_active']) > 0){
            $select->where("u.user_active = '?'", $options['user_active']);
        }
        
        // filter results on specified users types (if any)
        if(count($options['user_type']) > 1){
            $select->where('u.user_type IN (?)', $options['user_type']);
        } elseif(count($options['user_type']) > 0){
            $select->where("u.user_type = ?", $options['user_type']);
        }
        
        // filter results on specified users ids (if any)
        if(count($options['user_id']) > 1){
            $select->where('u.user_id IN (?)', $options['user_id']);
        } elseif(count($options['user_id']) > 0){
            $select->where("u.user_id = ?", $options['user_id']);
        }
        
        if(strlen($options['first_name']) > 0 || strlen($options['last_name']) > 0){
            $select->joinInner(
                array('p' => DB_PREFIX.'users_profile'), 'p.user_id = u.user_id', 
                array()
            );
            
            if(strlen($options['first_name']) > 0){
                $select->where("(p.profile_key = 'first_name' AND p.profile_value LIKE '%{$options['first_name']}%')");
            }
            if(strlen($options['last_name']) > 0){
                $select->where("(p.profile_key = 'last_name' AND p.profile_value LIKE '%{$options['last_name']}%')");
            }
        }

        // filter the records based on the start and finish dates
        if(strlen($options['from_ts']) > 0) {
            $select->where('u.ts_created >= ?', $options['from_ts']);
        }
        if(strlen($options['to_ts']) > 0) {
            $select->where('u.ts_created <= ?', $options['to_ts']);
        }

        return $select;
    }
    
    /**
     * Control for database user_mail & profile->user_mails and return the unique user->user_mail
     * or return the passed user_mail that after go to be validate
     * 
     * @param string $user_mail
     * 
     * @return string unique user->user_mail
     */
    public function getUniqueMail($user_mail)
    {
        $query = "SELECT user_mail FROM {$this->_table} WHERE user_mail = '{$user_mail}'";
        $result = $this->_db->fetchOne($query);

        if(strlen($result) > 3){
            return $result;
        } else {
            $result = $this->otherMail($user_mail, false);
            
            if(count($result) > 0 && $result[0]['user_mail'] == $user_mail){
                $user_id = $result[0]['user_id'];
            } else {
                return false;
            }
            
            $query = "SELECT user_mail FROM {$this->_table} WHERE user_id = '{$user_id}'";
            $result = $this->_db->fetchOne($query);
        }
        
        if(strlen($result) > 3){
            return $result;
        } else {
            return $user_mail;
        }
    }

    /**
     * Add a other user mail to database
     * 
     * @param int $user_id
     * @param string $user_mail
     * @param string/int $active
     * @param string $social
     * 
     * @return boolean
     */
    public function insertnewMail($user_id, $user_mail, $active, $social = null)
    {
        if($user_id < 0 || strlen($user_mail) < 4 || $active < 0 ){
            return false;
        }
        
        $this->om_key = substr($this->getId().uniqid(),0,50);
        
        $data = array('user_id' => $user_id,
                    'user_mail' => $user_mail,
                    'active' => $active,
                    'social' => $social,
                    'om_confirm_key' => $this->om_key,
                    'om_confirm_ts' => time()
                  );
                             
        if(!$this->_db->insert(DB_PREFIX.'users_mail', $data)){
            return false;
        }
        
        $this->om_id = $this->_db->lastInsertId();
        
        // Spediamo la e-Mail per attivare la nuova mail inserita
        $notifications = new DatabaseObject_Notification($this->_db);
        $notifications->notif('OTHER-MAIL', $this, array('other_user_mail' => $user_mail, 'om_key' => $this->om_key, 'om_id' => $this->om_id));
    }
    
    
    /**
     * Return all other user_mails or one other user_mail with parameters
     * 
     * @param string $user_mail     (can be null)
     * @param int $user_id          (can be null)
     * @param int $mail_id          (id of mail row)
     * 
     * @return bool/array       (with user_mail or mails)
     */
    public function otherMail($user_mail, $user_id, $mail_id = null)
    {
        $result = false;
        
        if($user_mail && !$user_id){
            $query = "SELECT * FROM `".DB_PREFIX."users_mail` WHERE user_mail = '{$user_mail}'";
            $result = $this->_db->fetchAll($query);
        } elseif($user_id){
            $query = "SELECT * FROM `".DB_PREFIX."users_mail` WHERE user_id = '{$user_id}'";
            $result = $this->_db->fetchAll($query);
        } elseif($mail_id) {
            $query = "SELECT * FROM `".DB_PREFIX."users_mail` WHERE mail_id = '{$mail_id}'";
            $result = $this->_db->fetchAll($query);
        }

        return $result;
    }
    
    /**
     * Set the user time_zone, locale and user translate locale, 
     * if the language of user is unaviable set default English 'en'
     * 
     * @param type $localeCode (ISO it_IT or en_US)
     */
    public function setUserLocale($localeCode)
    {
        $session = Zend_Registry::get('session');
        $locale = Zend_Registry::get('Zend_Locale');
        //$translate = Zend_Registry::get('Zend_Translate');

        // @todo add a suppress notice @ !!!

        $locale->setLocale($localeCode);
        $session->localeCode = $localeCode;

        /*$translate = Zend_Registry::get('Zend_Translate');

        if(!$translate->isAvailable($locale->getLanguage()))
            $translate->setLocale('en');
        else
            $translate->setLocale($locale->getLanguage());
        */
        return true;
    }
    
    /**
     * user_id of associated mail, primary or other
     * 
     * @param string $user_mail
     * 
     * @return int/boolean fasle or user_id
     */
    public function useridofMail($user_mail)
    {
        $query = "SELECT user_id FROM {$this->_table} WHERE user_mail = '{$user_mail}'";
        $user_id = $this->_db->fetchOne($query);

        if($user_id <= 0){
            $result = $this->otherMail($user_mail, false);
            
            if(count($result)>0){
                $user_id = $mail[0]['user_id'];
            } else {
                return false;
            }
        }
        
        return $user_id;
    }
    
    /**
     * True if user_mail or user_mails from profile are yet in database
     * Can be not active --- use activeMail() to control!
     * 
     * @param string $user_mail
     * @param int $user_id (The id of user that be controlled for existing mail)
     * 
     * @return bool/int 
     */
    public function existMail($user_mail, $user_id = false)
    {
        if($user_mail && !$user_id){
            $query = "SELECT count(*) AS NUM FROM {$this->_table} WHERE user_mail = '{$user_mail}'";
            $result = $this->_db->fetchOne($query);

            if($result == 0){
                $result = $this->otherMail($user_mail, false);

                if(count($result) == 0){
                    return false;
                }
            }
        } elseif($user_id) {
            $query = "SELECT count(*) AS NUM FROM {$this->_table} WHERE id = {$user_id} AND user_mail = '{$user_mail}'";
            $result = $this->_db->fetchOne($query);

            if($result == 0){
                $result = $this->otherMail($user_mail, false);

                if(count($result) > 0 && $result[0]['user_id'] == $user_id){
                    return true;
                } else { 
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }
    
    /**
     * True if identity are yet in database
     * 
     * @param string $identity
     * 
     * @return bool
     */
    public function existIdentity($identity)
    {
        $query = "SELECT count(*) AS NUM FROM {$this->_table} WHERE identity = '{$identity}'";
        $result = $this->_db->fetchOne($query);

        if($result == 0){
            return false;
        }

        return true;
    }

    /**
     * true if mail active, false another
     * 
     * @param STRING $user_mail
     * 
     * @return boolean
     */
    public function activeMail($user_mail)
    {
        // Vediamo se l'utente e attivo in caso contrario errore e richiesta di attivare tramite mail
        $query = "SELECT user_active FROM {$this->_table} WHERE user_mail = '{$user_mail}'";
        $result = $this->_db->fetchOne($query);
        
        if($result == 0){
            $res = $this->otherMail($user_mail, false);
            if(count($res)>0 && $res[0]['active'] == 0)
                return false;
        }

        return true;		
    }

    /**
     * true se l'utente del @ID passato é attivo 
     * 
     * @param STRING $user_mail
     * 
     * @return boolean
     */
    public function userActive($user_id)
    {
        // Vediamo se l'utente e attivo in caso contrario errore e richiesta di attivare tramite mail
        $query = "SELECT user_active FROM {$this->_table} WHERE user_id = '{$user_id}'";
        $result = $this->_db->fetchOne($query);
        if($result == 0)
            return false;

        return true;		
    }

    
    /**
     * Get image content with curl and write it to S3 on aws
     * 
     * @param type $url         internet url of image
     * @param type $folder      where save, AWS bucket and path
     * @param type $new_name    new name of saved image
     * 
     * @return boolean          true
     */
    public function getImgByUrl($url,$folder,$new_name)
    {
        $config = Zend_Registry::get('config');
        $s3 = new Zend_Service_Amazon_S3($config->aws->key, $config->aws->secret);
        $perms = array(Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ);

        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_POST, 0); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $file_content = curl_exec($ch);
        curl_close($ch);

        /*
        $downloaded_file = fopen(ROOT.$folder.$new_name, 'w');
        fwrite($downloaded_file, $file_content);
        fclose($downloaded_file);
        */
        
        $s3->putObject($folder.$new_name, $file_content, $perms);
        $uploaded = $s3->getInfo($folder.$new_name);
        
        $this->profile->poster_size = $uploaded['size'];
        $this->profile->poster_type = $uploaded['type'];
        
        return true;
    }
    
    public function camparePassword($password, $user_id)
    {
        $password = md5($password);
        
        $query = "SELECT user_id FROM {$this->_table} WHERE  `user_id` = '{$user_id}' AND `password` = '{$password}'";
        $result = $this->_db->fetchOne($query);
        if($result){
            return true;
        }
            
        return false;
    }

    /**
     * Get the id of languages by locale
     * 
     * @param string $locale (uk_UA)
     * 
     * @return int (id_language)
     */
    public function langIdByLocale($locale)
    {
        $lang = substr($locale,0,2);

        $query = "SELECT id_language FROM `".DB_PREFIX."languages` WHERE iso639code = '{$lang}'";
        $result = $this->_db->fetchOne($query);
        
        if($result > 0){
            return $result;
        } else {
            return 11;
        }
    }

    /**
     * Get the title of languages by id
     * 
     * @param int $id_language 
     * 
     * @return title (Italiana)
     */
    public function langTitleById($id_language)
    {
        $query = "SELECT title FROM `".DB_PREFIX."languages` WHERE id_language = '{$id_language}'";
        $result = $this->_db->fetchOne($query);

        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    /**
     * Get the id of language , if possible, by title
     * 
     * @param string $language 
     * 
     * @return title (Italiana)
     */
    public function langIdByTitle($language)
    {
        $query = "SELECT id_language FROM `".DB_PREFIX."languages` WHERE title LIKE '%{$language}%'";
        $result = $this->_db->fetchOne($query);

        if($result){
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Get the iso_639_code of languages by id
     * 
     * @param int $id_language 
     * 
     * @return title (it)
     */
    public function langIsoById($id_language)
    {
        $query = "SELECT iso639code FROM `".DB_PREFIX."languages` WHERE id_language = '{$id_language}'";
        $result = $this->_db->fetchOne($query);

        return $result;
    }
    
    /**
     * Get the id from iso_639_code of languages
     * 
     * @param int $iso
     * 
     * @return title (it)
     */
    public function langIdByIso($iso)
    {
        $query = "SELECT id_language FROM `".DB_PREFIX."languages` WHERE iso639code = '{$iso}'";
        $result = $this->_db->fetchOne($query);

        return $result;
    }


    /**
     * Get the timezone full name from city name
     * 
     * @param string $capital
     * 
     * @return title (it)
     */
    public function timezoneByCity($capital)
    {
        $timezone = $this->generateTimezone();

        foreach($timezone as $key => $val){
            foreach($val as $k => $v){
                if($v == $capital){
                   $need = $k;
                }
            }
        }
        if(isset($need)){
            return $need;
        } else {
            return false;
        }
    }
    
    /**
     * Generate array that contain all timezone sorted by Region
     */
    public function generateTimezone()
    {
        $zones = timezone_identifiers_list();
        $date = new Zend_Date();
        
        foreach ($zones as $zone)
        {
            $zoneExploded = explode('/', $zone); // 0 => Continent, 1 => City
            // Only use "friendly" continent names
            if($zoneExploded[0] == 'Africa' || $zoneExploded[0] == 'America' || $zoneExploded[0] == 'Antarctica' || $zoneExploded[0] == 'Arctic' || $zoneExploded[0] == 'Asia' || $zoneExploded[0] == 'Atlantic' || $zoneExploded[0] == 'Australia' || $zoneExploded[0] == 'Europe' || $zoneExploded[0] == 'Indian' || $zoneExploded[0] == 'Pacific'){
                if (isset($zoneExploded[1]) != ''){
                    $area = str_replace('_', ' ', $zoneExploded[1]);
                    if (!empty($zoneExploded[2])){
                        $area = $area . ' (' . str_replace('_', ' ', $zoneExploded[2]) . ')';
                    }

                    $date->setTimezone($zone);
                    $tzFriendlyOffset = $date->get(Zend_Date::GMT_DIFF_SEP);
                    
                    $locations[$zoneExploded[0]][$zone] = $area.' ('. $tzFriendlyOffset .')'; // Creates array(DateTimeZone => 'Friendly name')
                    /*
                    $tzfo = '+00:00';
                    $areas = '';
                    if($tzFriendlyOffset == $tzfo){
                        $areas .= $area.', '; 
                    } else {
                        $locations[$zoneExploded[0]][$zone] = '('. $tzFriendlyOffset .') '. $areas;
                        $tzfo = $date->get(Zend_Date::GMT_DIFF_SEP);
                        $areas = '';
                    }*/
                }
            }
        }

        return $locations;
    }

    /**
     * Get array that contain all languages contained in database
     * 
     * @param string/int $what Active = 1, Disactive = 0 or ALL languages (default)
     */
    public function getLanguages($what = 'ALL')
    {
        if($what == 'ALL'){
            $query = "SELECT * FROM `".DB_PREFIX."languages`";
        } elseif($what == 1){
            $query = "SELECT * FROM `".DB_PREFIX."languages` WHERE status = 1";
        } elseif($what == 0){
            $query = "SELECT * FROM `".DB_PREFIX."languages` WHERE status = 0";
        }
        
        $languages = $this->_db->fetchAll($query);

        return $languages;
    }
    
    /**
     * True if user has already connected with fb
     * 
     * @param int $user_id
     * 
     * @return boolean
     */
    public function fbUser($user_id)
    {
        $query = "SELECT profile_value FROM `".DB_PREFIX."users_profile` WHERE  `user_id` = '{$user_id}' AND `profile_key` = 'fb_id_conn'";
        
        $result = $this->_db->fetchOne($query);
                
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    /**
     * Return user_id from user fb_conn_id
     * 
     * @param int $fb_id_conn
     * 
     * @return boolean
     */
    public function fbUserid($fb_id_conn)
    {
        $query = "SELECT user_id FROM `".DB_PREFIX."users_profile` WHERE  `profile_key` = 'fb_id_conn' AND `profile_value` = '{$fb_id_conn}'";
        
        $result = $this->_db->fetchOne($query);
                
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    /**
     * Is this a user that connected with Google+
     * 
     * @param int $user_id
     * @return boolean/string false or id of google user
     */
    public function gpUser($user_id)
    {
        $query = "SELECT profile_value FROM `{$this->profile->getTable()}` WHERE  `user_id` = '{$user_id}' AND `profile_key` = 'gp_id_conn'";
        
        if($result = $this->_db->fetchOne($query)){
            return $result;
        } else {
            return false;
        }
    }
    
    /**
     * Return user_id from google plus user_id
     * 
     * @param string $gp_id_conn (117274939285089676744)
     * 
     * @return boolean/int
     */
    public function gpUserid($gp_id_conn)
    {
        $query = "SELECT user_id FROM `{$this->profile->getTable()}` WHERE `profile_key` = 'gp_id_conn' AND `profile_value` = '{$gp_id_conn}'";
        if($result = $this->_db->fetchOne($query)){
            return $result;
        } else {
            return false;
        }
    }
    
    /**
     * True if user already connect with twitter
     * 
     * @param int $user_id
     * 
     * @return boolean
     */
    public function twUser($user_id){
        $query = "SELECT profile_value FROM `{$this->profile->getTable()}` WHERE  `user_id` = '{$user_id}' AND `profile_key` = 'tw_id_conn'";
        if($result = $this->_db->fetchOne($query)){
            return $result;
        } else {
            return false;
        }
    }
    
    /**
     * Return user_id from twitter user id
     * 
     * @param string $tw_id_conn (117274939285089676744)
     * 
     * @return boolean/int
     */
    public function twUserid($tw_id_conn){
        $query = "SELECT user_id FROM `{$this->profile->getTable()}` WHERE `profile_key` = 'tw_id_conn' AND `profile_value` = '{$tw_id_conn}'";
        if($result = $this->_db->fetchOne($query))
            return $result;
        else
            return false;
    }
    
    /**
     * Delete image from server and DB
     * 
     * @param object $identity
     * @param int $id
     * 
     * @return array with translated message array(type => message)
     */
    public function deletePoster()
    {
        $config = Zend_Registry::get('config');
        $s3 = new Zend_Service_Amazon_S3($config->aws->key, $config->aws->secret);
        
        $translate = Zend_Registry::get('Zend_Translate');
            
        $object = $this->bucket.'/'.$this->bucket_image_path . $this->profile->poster_name;
        $object_t = $this->bucket.'/'.$this->bucket_image_path .'thumbnail/'. $this->profile->poster_name;
        $object_n = $this->bucket.'/'.$this->bucket_image_path .'normal/'. $this->profile->poster_name;
            
        $s3->removeObject($object);
        $s3->removeObject($object_t);
        $s3->removeObject($object_n);
        
        $this->poster = null;
        unset($this->profile->poster_normal);
        unset($this->profile->poster_thumbnail);
        unset($this->profile->poster_name);
        unset($this->profile->poster_size);
        unset($this->profile->poster_type);

        $this->save();
        
        return array('success' => $translate->translate('Picture deleted'));
    }
    
    public function deletAllReviews(){
        if($this->getId() <= 0){
            return false;
        }
        
        $comm_feedbacks = DatabaseObject_CommunityFeedback::GetFeedbacks($this->_db, array('user_id' => $this->getId(), 'status' => null));
        foreach($comm_feedbacks as $id => $ah){
            $feedback = new DatabaseObject_CommunityFeedback($this->_db);
            $feedback->load($id);
            $feedback->delete();
        }
        
        $hospice_feedbacks = DatabaseObject_HospiceFeedback::GetFeedbacks($this->_db, array('user_id' => $this->getId(), 'status' => null));
        foreach($hospice_feedbacks as $id => $ah){
            $feedback = new DatabaseObject_HospiceFeedback($this->_db);
            $feedback->load($id);
            $feedback->delete();
        }
        
        $hospital_feedbacks = DatabaseObject_HospitalFeedback::GetFeedbacks($this->_db, array('user_id' => $this->getId(), 'status' => null));
        foreach($hospital_feedbacks as $id => $ah){
            $feedback = new DatabaseObject_HospitalFeedback($this->_db);
            $feedback->load($id);
            $feedback->delete();
        }
        
        $kinder_feedbacks = DatabaseObject_KindergartenFeedback::GetFeedbacks($this->_db, array('user_id' => $this->getId(), 'status' => null));
        foreach($kinder_feedbacks as $id => $ah){
            $feedback = new DatabaseObject_KindergartenFeedback($this->_db);
            $feedback->load($id);
            $feedback->delete();
        }
        
        $rehab_feedbacks = DatabaseObject_RehabFeedback::GetFeedbacks($this->_db, array('user_id' => $this->getId(), 'status' => null));
        foreach($rehab_feedbacks as $id => $ah){
            $feedback = new DatabaseObject_RehabFeedback($this->_db);
            $feedback->load($id);
            $feedback->delete();
        }
        
        return true;
    }
}
<?php
/**
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 * @version 1.1
 * Twitter
 * Class of method for work with _user_ , _p-user_ and his data
 * method for register, connect, getInformation, getUsers, setData, searchFor _user_/s
 */
class DatabaseObject_UserTwitter extends DatabaseObject_User
{    
    // define all need variables
    var $owner_id = "254468423"; // ArturMamedov
    var $app_key = "Y28P75WCGshSv0SwYZ8wDIGWv";
    var $app_secret = "CPcTHfGYCCfAtTPqrGLVKznQPVkMcFl2pJCAvUH80m0ZJeTVTQ";
    /*
    App-only authentication https://api.twitter.com/oauth2/token
    Request token URL https://api.twitter.com/oauth/request_token
    Authorize URL https://api.twitter.com/oauth/authorize
    Access token URL https://api.twitter.com/oauth/access_token 
    */
    
    public function __construct($db){
        parent::__construct($db);
        
        $this->social = 'Twitter';
        $this->user_logger('twitter');
    }

    protected function preInsert(){
        parent::preInsert();
        return true;
        
    }

    protected function postLoad(){
        parent::postLoad();
        return true;
    }

    protected function postInsert(){
        parent::postInsert();
        return true;
    }

    protected function postUpdate(){
        parent::postUpdate();
        return true;
    }

    protected function preDelete(){
        parent::preDelete();
        return true;
    }   
    
    /**
     * Twitter
     * Retrive twitter autorization link or true if is already set in session 
     * and get the ouath_token
     * 
     * @todo - now i use a twitterouth SDK by @abraham, wont to change to use only API CURL call
     * 
     * @param string $redirect (url of page that begin the call, or NO if it's call only for obtain access_token)
     * 
     * @return boolean true/false
     */
    public function twAccesstoken($redirect){
        $sess = Zend_Registry::get('session');
        $sess->redirect_end = ($redirect != 'NO') ? $redirect : $sess->redirect_end; // oo = only object (last else)

        if(!isset($sess->tw_access_token) || $sess->tw_access_token == ''){            
            require_once ROOT.DS.'private'.DS.'libraries'.DS.'twitteroauth'.DS.'twitteroauth.php';

            if(!$sess->tw_request_token || !$sess->tw_request_token_secret){
                $twitter = new TwitterOAuth($this->app_key, $this->app_secret);

                $request_token = $twitter->getRequestToken(Zend_Registry::get('config')->site->web_uri.'account/access?conn=tw');
                $request_link = $twitter->getAuthorizeURL($request_token);

                // Save tokens for later  - we need these on the callback page to ask for the access tokens
                $sess->tw_request_token = $request_token['oauth_token'];
                $sess->tw_request_token_secret = $request_token['oauth_token_secret'];

                unset($sess->tw_access_token);
                unset($sess->tw_access_token_secret);
                unset($sess->tw_screen_name);
                
                exit("<script>top.location.href='".$request_link."'</script>");
                return;
            }

            if(!isset($sess->tw_access_token) || $sess->tw_access_token == ''){
                if(isset($_GET['oauth_verifier'])){
                    $twitter = new TwitterOAuth($this->app_key, $this->app_secret, $sess->tw_request_token, $sess->tw_request_token_secret);

                    $tw_request = $twitter->getAccessToken($_GET['oauth_verifier']);

                    // return access token for save in controller
                    $sess->tw_access_token = $tw_request['oauth_token'];
                    $sess->tw_access_token_secret = $tw_request['oauth_token_secret'];
                    $sess->tw_screen_name = $tw_request['screen_name'];
                }
                unset($sess->tw_request_token);
                unset($sess->tw_request_token_secret);
                
                $redirect_end = $sess->redirect_end;
                $sess->redirect_end = null;
                
                exit("<script>top.location.href='".Zend_Registry::get('config')->site->web_uri.$redirect_end."'</script>");
                return;
            }
        } else {
            require_once ROOT.DS.'private'.DS.'libraries'.DS.'twitteroauth'.DS.'twitteroauth.php';
        
            $twitter = new TwitterOAuth($this->app_key, $this->app_secret, $sess->tw_access_token, $sess->tw_access_token_secret);
            return $twitter;
        }
    }
    
    /**
     * Twitter[method]
     * Use Twiter API with cUrl to do call and operation ex: retrive user information
     * 
     * @todo - now i using this function only to retrive the user information i need to implement more operations 
     * 
     * @param string $redirect (url of oage that call the API, passed to teAccesstoken)
     * 
     * @return array    ([status] => false, [error] => 'ACC_TOK/CURL_ERR', [message] => 'Facebook[method] - ..desc..')
     *                  ([status] => true, [profile] => $profile ('all_data'))
     */
    public function twMethod($redirect = null){
        $sess = Zend_Registry::get('session');
        
        if(!$twitter = $this->twAccesstoken($redirect)){
            return array('status' => false, 'error' => 'ACC_TOK', 'message' => 'Twitter[method] - False access_token');
        } else {
            $profile = $twitter->get('account/verify_credentials');
            //exit(var_dump($profile->errors));
            if($profile == FALSE){
                return array('status' => false, 'error' => 'TWconn_ERR', 'message' => 'Twitter[method] - '.$profile->error);				
            } elseif(isset($profile->errors)){
                return array('status' => false, 'error' => 'TW_API', 'message' => 'Twitter[method] - Error: '.$profile->error);
            }

            return array('status' => true, 'profile' => $profile);
        }
        return false;
    }
    
    /**
     * Twitter[userinfo]
     * Retrive user information by calling twMethod 
     * and if user newest in site store all data in session for FormProcessor and classic registration flow
     * another update twitter_profile and twitter_id and data that nor store in DB and login user
     * @param string $mail [optional if call provent from already loged user]
     * @param string $redirect [url of page that begin the call if was necessary retrive access_token]
     * 
     * @return array    ([status] => false, [error] => '[fbMethod[error]]/CANT_LOAD_REG/NO_UPDATE', [message] => 'Twitter[userinfo] - ..desc..')
     *                  ([status] => true, [message] => 'RETRIVED/CONNECTED', 'identity' => (data from $this->creatAuthIdentity, 'data' => 'for FormProcessor'))
     */
    public function twUserinfo($mail, $redirect = null){
        $session = new Zend_Session_Namespace('login');
        $sess = Zend_Registry::get('session');
        
        $profile = $this->twMethod($redirect);
        if($profile['status'] === false){
            return array('status' => false, 'error' => $profile['error'], 'message' => $profile['message']);
        } else {
            $profile = $profile['profile'];
        }
        
        if($mail == null){
            // se l'utente non si e' ancora mai connesso con Twitter recupero dentro la sessione tutti i suoi bei dati
            $u_id = $this->twUserid($profile->id_str);
            if($u_id <= 0){
                
                $tmp_name = explode(' ', $profile->name);

                $session->tw_id_conn = $profile->id_str;
                $session->profile_photo = $profile->profile_image_url; // 48x48
                $session->tw_profile = 'https://twitter.com/'.$profile->screen_name;
                $session->screen_name = $profile->screen_name;
                $session->description = $profile->description;
                $session->location = $profile->location;
                $session->web_site = $profile->url;
                // @poster
                $poster = $profile->profile_image_url;
                $ext = pathinfo($poster);
                $cut = (8 + strlen($ext['extension']));
                $or_poster = substr($poster, 0, strlen($poster) - $cut) .'.'.$ext['extension'];
                $session->poster = $or_poster;
                
                // token for future API call
                if(isset($sess->tw_access_token)){
                    $session->tw_access_token = $sess->tw_access_token;
                }
                if(isset($sess->tw_access_token_secret)){
                    $session->tw_access_token_secret = $sess->tw_access_token_secret;
                }

                $session->time_zone = $profile->time_zone;
                $session->lang = $profile->lang;
                
                return array('status' => true, 'message' => 'RETRIVED', 'data' => array('first_name' => $tmp_name[0], 'last_name' => $tmp_name[1]));
            } else {
                // se l'utente gia' in precedenza si e' loggato con twitter lo autorizzo ed e' fatta :)
                if(!$this->load($u_id)){
                    return array('status' => false, 'error' => 'CANT_LOAD_REG', 'message' => "Twitter[userinfo] - load(exist) failed for (user_id: {$u_id}, user_mail: {$mail})");
                }
                 
                // twitter u @id
                if(!empty($profile->id_str)){
                    $this->profile->tw_id_conn = $profile->id_str;
                }

                // @profile_twitter
                if(!empty($profile->screen_name)){
                    $this->profile->tw_profile = 'https://twitter.com/'.$profile->screen_name;
                }
                // @about
                if(!isset($this->profile->about) && !empty($profile->description)){
                    $this->profile->about = $profile->description;
                }
                // @location
                if(!isset($this->profile->location) && !empty($profile->location)){
                    $this->profile->location = $profile->location;
                }
                // @website
                if(!isset($this->profile->web_site) && !empty($profile->url)){
                    $this->profile->web_site = $profile->url;
                }
                if(!isset($this->profile->tw_name) && !empty($profile->screen_name)){
                    $this->profile->tw_name = $profile->screen_name;
                    $this->profile->tw_autopost = 1;
                }
                // @poster
                $cdn_path = $this->image_cloudfront_endpoint.$this->bucket_image_path;
                $bucket_path = $this->bucket.'/'.$this->bucket_image_path;

                // @poster
                $poster = $profile->profile_image_url;
                $ext = pathinfo($poster);
                $cut = (8 + strlen($ext['extension']));
                $or_poster = substr($poster, 0, strlen($poster) - $cut) .'.'.$ext['extension'];
                
                $this->profile->poster_name = 'user_tw_l_'.$this->profile->tw_id_conn.'_'.time().'_'.uniqid().'.jpg';

                $this->poster = $cdn_path.$this->profile->poster_name;
                $this->profile->poster_thumbnail = $cdn_path.'thumbnail/'.$this->profile->poster_name;
                $this->profile->poster_normal = $cdn_path.'normal/'.$this->profile->poster_name;            

                $this->getImgByUrl($or_poster, $bucket_path, $this->profile->poster_name);

                $this->profile->tw_connected = 1;
                // token for future API call
                if(isset($sess->tw_access_token)){
                    $this->profile->tw_access_token = $sess->tw_access_token;
                }
                if(isset($sess->tw_access_token_secret)){
                    $this->profile->tw_access_token_secret = $sess->tw_access_token_secret;
                }
                
                if(!$this->save()){
                    return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Twitter[userinfo] - save(exist) failed for (user_id: {$this->user_id}, user_mail: {$this->user_mail})");
                }
                
                // record login attempt
                $this->loginSuccess();

                // set the localization of user @todo put in a a part method
                $this->setUserLocale($this->user_locale);

                // initialize the options
                $options = array(
                    array('resize' => 'fit', 'w' => 100, 'h' => 100, 'path' => 'thumbnail', 'image' => $this->profile->poster_name),
                    array('resize' => 'height', 'w' => 220, 'h' => 220, 'path' => 'normal', 'image' => $this->profile->poster_name)
                );
                $this->makeThumbs('image/jpg', $bucket_path, $options);
                
                return array('status' => true, 'message' => 'CONNECTED', 'identity' => $this->createAuthIdentity());
            }
        } elseif(strlen($mail) > 4) {
            // get user id by mail and load user
            $u_id = $this->useridofMail($mail);
            if($u_id > 0){
                $this->load($u_id);
            } else {
                return array('status' => false, 'error' => 'CANT_LOAD_REG', 'message' => "Twitter[userinfo] - load(exist) failed for (user_id: {$u_id}, user_mail: {$mail})");
            }

            // twitter u @id
            if(!isset($this->profile->tw_id_conn) && !empty($profile->id_str)){
                $this->profile->tw_id_conn = $profile->id_str;
            }
            // @photo
            /*if(!isset($this->profile->profile_photo) && !empty($profile->profile_image_url)){	
                //$this->profile->profile_photo = $session->profile_photo;
                $this->profile->profile_photo = 'tw_'.$this->profile->tw_id_conn.'.jpg';
                $this->getImgByUrl($profile->profile_image_url, '/public/file/account/image/', $this->profile->profile_photo);
            }*/
            // @profile_twitter
            if(!isset($this->profile->tw_profile) && !empty($profile->screen_name)){
                $this->profile->tw_profile = 'https://twitter.com/'.$profile->screen_name;
            }
            // @about
            if(!isset($this->profile->about) && !empty($profile->description)){
                $this->profile->about = $profile->description;
            }
            // @location
            if(!isset($this->profile->location) && !empty($profile->location)){
                $this->profile->location = $profile->location;
            }
            // @website
            if(!isset($this->profile->web_site) && !empty($profile->url)){
                $this->profile->web_site = $profile->url;
            }
            if(!isset($this->profile->tw_name) && !empty($profile->screen_name)){
                $this->profile->tw_name = $profile->screen_name;
                $this->profile->tw_autopost = 1;
            }
            $this->profile->tw_connected = 1;
            // token for future API call
            if(isset($sess->tw_access_token)){
                $this->profile->tw_access_token = $sess->tw_access_token;
            }
            if(isset($sess->tw_access_token_secret)){
                $this->profile->tw_access_token_secret = $sess->tw_access_token_secret;
            }
                
            if(!$this->save()){
                return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Twitter[userinfo] - load(exist) failed for (user_id: {$this->user_id}, user_mail: {$this->user_mail})");
            }
               
            // record login attempt
            $this->loginSuccess();

            return array('status' => true, 'message' => 'CONNECTED', 'identity' => $this->createAuthIdentity());
        }
    }
    
    /**
     * Import twitter profile image
     * if user not connected with twitter previuosly, connect him by calling twUserinfo
     * 
     * @param int $user_id
     * @param string $user_mail
     * 
     * @return array    ([status] => false, [error] => '[fbMethod[error],fbUserinfo[error]]/CANT_LOAD_CONN/NO_UPDATE', [message] => 'Twitter[userimage] - ..desc..')
     *                  ([status] => true, [message] => 'UPDATED', 'identity' => (data from $this->creatAuthIdentity))
     */
    public function twUserimage($user_id, $user_mail){
        if(!$this->twUser($user_id)){
            $this->twUserinfo($user_mail, 'account/twimportimage');
        }
        
       
        $profile = $this->twMethod('account/twimportimage');
        if($profile['status'] !== true)
            return array('status' => false, 'error' => $profile['error'], 'message' => $profile['message']);
        else
            $profile = $profile['profile'];

        if(!$this->load($user_id))
            return array('status' => false, 'error' => 'CANT_LOAD_CONN', 'message' => "Twitter[userimage] - load(exist) failed for (user_id: {$user_id}, user_mail: {$user_mail})");
        
        // profile_photo based on facebook user id and in the end i attach the type of image returned by method that save image on server
        $this->profile->profile_photo = 'tw_'.$this->profile->tw_id_conn.'-'.time().'.jpg';
        $this->getImgByUrl($profile->profile_image_url, '/public/file/account/image/', $this->profile->profile_photo);

        $this->profile->tw_connected = '1';
        
        if(!$this->save())
            return array('status' => false, 'error' => 'NO_UPDATE', 'message' => "Twitter[userimage] - save(exist) failed for (user_id: {$this->user_id}, user_mail: {$this->user_mail})");

        return array('status' => true, 'message' => 'UPDATED', 'identity' => $this->createAuthIdentity());
    }
}
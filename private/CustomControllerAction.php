<?php
class CustomControllerAction extends Zend_Controller_Action
{
    /**
     * Object of Database Connection
     * @var object 
     */
    public $db;
    /**
     * Breadcrumbs object
     * ->addStep(strName, strUrl[$this->getUrl(strAction, strCntrlr)], strTitle)
     * 
     * @var object 
     */
    public $breadcrumbs;
    /**
     * Messenges for Error/Warning/Notice etc...
     * @var object
     */
    public $messages;
    /**
     * Object of logged user with him identity
     * @var object 
     */
    public $identity;
    /**
     * For check if user are or no logged to system, true yes, false none
     * @var boolean
     */
    public $authenticated;
    /**
     * All action need to translate portion of text, view to, and this is for that purpose
     * @var object
     */
    public $translate;
    /**
     * I dont know ...
     * @var type 
     */
    private $_sanitizeChain = null;
    /**
     * Site Name for Title
     * @var string
     */
    public $tsn = ' | Zend1 Skeleton';
	
    
    /**
     * That method was called for first 
     * and here i set the db connection on $this->db from Register
     */
    public function init()
    {
        $this->db = Zend_Registry::get('db');
        
        $this->breadcrumbs = new Breadcrumbs();
        $this->breadcrumbs->addStep('Home', $this->getUrl(null, 'index'));
        $this->messages = new Messages;
    }
    
    /**
     * Create url by action and controller
     * ex: index, hospital => ..../hospital/index
     * 
     * @param string $action
     * @param string $controller
     * 
     * @return string
     */
    public function getUrl($action = null, $controller = null)
    {
        $url  = rtrim($this->getRequest()->getBaseUrl(), '/');
        $url .= $this->_helper->url->simple($action, $controller);

        return $url;
    }

    /**
     * Create custom url
     * 
     * @param array $options
     * @param string $route
     * 
     * @return string
     */
    public function getCustomUrl($options, $route = null)
    {
        return $this->_helper->url->url($options, $route);
    }
    
    /**
     * This method was call before dispatch any other Action in other Controller
     * and here i set the global session var, global object, logged user identity and more
     */
    public function preDispatch()
    {
        // identity - authenticated
        $auth = Zend_Auth::getInstance();
        $session = Zend_Registry::get('session');
        
        if($auth->hasIdentity()){
            $this->view->authenticated = $this->authenticated = true;
            $this->view->identity = $this->identity = $auth->getIdentity();
            
            if(empty($session->localeCode)){
                $session->localeCode = $this->identity->user_locale;
            }
            if(empty($session->siteLang)){
                $session->siteLang = $this->identity->iso_lang;
            }
            // new notifications?
            $this->view->notif_count = DatabaseObject_Notification::GetNotificationsCount($this->db, array('user_id' => $this->identity->user_id, 'active' => '1'));
        } else {
            $this->view->authenticated = $this->authenticated = false;
        }
        
        // Locale
        $locale = new Zend_Locale('auto');
        if(isset($session->localeCode)){
            $locale->setLocale($session->localeCode);
        } 
        /* elseif(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
            foreach($locale->getBrowser() as $loc => $val){
                $localeCode = $session->localeCode = $loc;
                break;
            }
            $locale->setLocale($localeCode);
            Zend_Registry::set('Zend_Locale', $locale);
        } */
        elseif(strlen($locale->toString()) < 2) {
            // default
            $locale->setLocale("it_IT");
        }
        Zend_Registry::set('Zend_Locale', $locale);
        $localeCode = $session->localeCode = $locale->toString();
        
        // TimezoneToTerritory, TimezoneToCity, TerritoryToPhone, TerritoryToLanguage, RegionToCurrency, Language
        // if (Zend_Locale::isLocale($input)) {
        // exit(print_r($locale->getTranslationList('TimezoneToTerritory', $localeCode)));
        
        $config = Zend_Registry::get('config');
        
        // Wring up Zend_Translate
        $translate = new Zend_Translate('gettext', ROOT.DS.'private'.DS.'libraries'.DS.'Languages'.DS, 'it', array('scan' => Zend_Translate::LOCALE_FILENAME));
        // here we read URL for language, and if isset we add this language to session to
        $lang = $this->_request->getUserParam('lang');
        if(strlen($lang) == 2 && $translate->isAvailable($lang)){
            $session->siteLang = $lang;
        }
        // set the language of site
        if(isset($session->siteLang)){
            $translate->setLocale($session->siteLang); // set translation
            $this->view->iso_lang = $session->siteLang; // put site lang on html tag
        } elseif($translate->isAvailable($locale->getLanguage())) {
            $translate->setLocale($locale->getLanguage());
            $this->view->iso_lang = $localeCode;
        } else {
            $translate->setLocale('it');
            $this->view->iso_lang = 'it_IT';
        }
        $this->view->lang = $lang = substr($translate->getLocale(), 0, 2);
        $translate_uri = $this->getRequest()->getRequestUri();
        if($lang == 'it'){
            $translate_uri = str_replace('/it', '', $translate_uri);
            $this->view->translate_uri = '/en'.$translate_uri;
            // for Disqus, Facebook in example
            $this->view->no_translate_uri = $config->site->host . $translate_uri;
        } else {
            $translate_uri = str_replace('/en', '', $translate_uri);
            $this->view->translate_uri = '/it'.$translate_uri;
            // for Disqus, Facebook in example
            $this->view->no_translate_uri = $config->site->host . $translate_uri;
        }
        Zend_Registry::set('Zend_Translate', $translate);

        // set the timezone of user, only if javascript not performed yet
        $request = new Zend_Controller_Request_Http();
        $cookie_tz = $request->getCookie('timezone');
        if($session->timeZone_cookie == NULL || $cookie_tz != $session->timeZone){
            if(strlen($cookie_tz) > 3){
                $session->timeZone = $cookie_tz;
                $session->timeZone_cookie = true;
            }
            else {
                $timezones = $locale->getTranslationList('TimezoneToTerritory');
                if(isset($timezones[strtoupper($lang)])){
                    $session->timeZone = $timezones[strtoupper($lang)];
                } else {
                    $session->timeZone = 'UTC';
                }
            }
        }
        
        date_default_timezone_set($config->date->timezone->default); // cosi ho il timeZoneOffset
        
        // set general Zend_Date
        $date = new Zend_Date();
        if(isset($session->timeZone)){
            $date->setTimezone($session->timeZone);
        } else {
            $date->setTimezone($config->date->timezone->default);
            $session->timeZone = $config->date->timezone->default;
        }
        Zend_Registry::set('Zend_Date', $date);
        
        $this->view->tsn = $this->tsn;
        $this->view->ga = true;
        $this->view->adversting_image = '/public/img/site/adv225trnsp.png';
        // Translate to view and in the object for use anywherw
        $this->view->translate = $this->translate = $translate;
    }

	
	public function postDispatch()
    {
        //$this->analytics();
                
        $this->view->breadcrumbs = $this->breadcrumbs;
        $session = Zend_Registry::get('session');
        //$this->view->messages = $this->messenger->getMessages(); // @todo?
        if($session->message != NULL){
            $this->view->message = $session->message[1];
            $this->view->msg_type = $session->message[0];
            $session->message = NULL;
        }

        $this->view->isXmlHttpRequest = $this->getRequest()->isXmlHttpRequest();
    }
	
	/**
     * Not in use
     * record all visited page by check every request
     */
	public function analytics()
    {
        $request = $this->getRequest();
        
        $description = '';
        if(strlen($request->getParam('status')) > 0){
            $description = 'STATUS = '.$request->getParam('status');
        }
        if(strlen($request->getParam('cat')) > 0){
            $description = 'CAT_ID = '.$request->getParam('status');
        }
        if($this->authenticated){
            $user_id = $this->identity->user_id;
        } else {
            $user_id = 0;
        }
        
        $analytics = new DatabaseObject_Analytics($this->db);
        $analytics->insert($request->getParam('controller'), $request->getParam('action'), $request->getRequestUri(), 'Hmmm', $description, $user_id, $request->getParam('id'), $request->getParam('controller'));
        //'medic', 'view', $this->identity->user_id, $this->request->get()
    }
	
	
	
	/**
     * Get the name of context
     * get the object of context item
     * ex: /index/ - 'index'
     * 
     * @param string $context (Url from detect context)
     * @param int $dbo 0/1 (default 1 , retrive)
     * @param int $ctx 0/1 (default 1 , retrive)
     * @param int $name 0/1 (default 1 , retrive)
     * 
     * @return boolean/array
     */
    public function getContext($context, $dbo = 1, $ctx = 1, $name = 1)
    {
        return false;
//        $response = array();
        
//        if(strpos($context, '/index/') > 0){
//            $response['item'] = ($dbo == 1) ? new DatabaseObject_Hospital($this->db) : null;
//            $response['context'] = ($ctx == 1) ? 'hospital' : null;
//            $response['name'] = ($name == 1) ? $this->translate->translate('Hospital') : null;
//        } elseif(strpos($context, '/medic/') > 0){
//            $response['item'] = ($dbo == 1) ? new DatabaseObject_Medic($this->db) : null;
//            $response['context'] = ($ctx == 1) ? 'medic' : null;
//            $response['name'] = ($name == 1) ? $this->translate->translate('Medic') : null;
//        } else {
//            return false;
//        }
        
//        return $response;
    }
	
	
	
	
	
    /**
     * disable the render view and send json data with correct headers
     * 
     * @param type $data
     */
    public function sendJson($data)
    {
        $this->_helper->viewRenderer->setNoRender();

        $this->getResponse()->setHeader('content-type', 'application/json');
        echo Zend_Json::encode($data);
    }

    /**
     * Sanitize the passed value, stripTags, StringTrim
     * and eliminate a new line
     * 
     * @param string $value
     * 
     * @return string
     */
    public function sanitize($value)
    {
        if (!$this->_sanitizeChain instanceof Zend_Filter) {
            $this->_sanitizeChain = new Zend_Filter();
            $this->_sanitizeChain->addFilter(new Zend_Filter_StringTrim())->addFilter(new Zend_Filter_StripTags());
        }

        // filter out any line feeds / carriage returns
        $ret = preg_replace('/[\r\n]+/', ' ', $value);

        // filter using the above chain
        return $this->_sanitizeChain->filter($ret);
    }
    
    /**
     * Sanitize the passed value, stripTags, StringTrim
     * but keep new line
     * 
     * @param string $value
     * 
     * @return string
     */
    public function textsanitize($value)
    {
		$value = trim($value);
        if (!$this->_sanitizeChain instanceof Zend_Filter) {
            $this->_sanitizeChain = new Zend_Filter();
            $this->_sanitizeChain->addFilter(new Zend_Filter_StringTrim())
                                 ->addFilter(new Zend_Filter_StripTags());
        }

        // filter using the above chain
        return $this->_sanitizeChain->filter($value);
    }
}
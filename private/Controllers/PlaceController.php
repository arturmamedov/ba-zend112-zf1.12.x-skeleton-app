<?php

class PlaceController extends CustomControllerAction {
  
    public function init()
    {
        parent::init();
        $this->breadcrumbs->addStep('Regioni', $this->getUrl('index', 'place'));
    }
    
    public function indexAction()
    {
        $regions = DatabaseObject_Region::GetRegions($this->db);
        
        $this->view->regions = $regions;
        
        $this->view->addthis = 0;
        
		$this->view->title = 'Regioni Italiane ..';
        $this->view->description = 'Regioni Italiane ...';
    }
    
    public function regionAction()
    {
        $id = $this->sanitize($this->_request->getParam('id'));
        
        $region = new DatabaseObject_Region($this->db);
        if(!$region->load($id)){
            $this->redirect('/place/region');
        }
        
        $cities = DatabaseObject_City::GetCities($this->db, array('region_id' => $region->getId()));
        $provinces = DatabaseObject_Province::GetProvinces($this->db, array('region_id' => $region->getId()));
        
        $this->view->cities = $cities;
        $this->view->provinces = $provinces;
        $this->view->region = $region;
        
        $this->view->addthis = 0;
        $this->breadcrumbs->addStep($region->name);
        
		$this->view->title = $region->name.', Italia';
        $this->view->description = $region->name.', Regione Italiana';
    }
    
    public function provinceAction()
    {
        $id = $this->sanitize($this->_request->getParam('id'));
        
        $province = new DatabaseObject_Province($this->db);
        if(!$province->load($id)){
            $this->redirect('/place/region');
        }
        $region = new DatabaseObject_Region($this->db);
        $region->load($province->region_id);

        $cities = DatabaseObject_City::GetCities($this->db, array('province_id' => $province->getId()));

        $this->view->cities = $cities;
        $this->view->province = $province;
        $this->view->region = $region;

        $this->view->addthis = 0;
        
        $this->breadcrumbs->addStep(
            $region->name, 
            $this->getUrl('region', 'place'). '/id/'.$region->getId()
        );
        $this->breadcrumbs->addStep($province->name ."({$province->code_2})");        
        
		$this->view->title = 'Provincia di '.$province->name;
        $this->view->description = 'Provincia di '.$province->name.' '.$region->name;
    }
    
    public function cityAction(){	
		
        $id = $this->sanitize($this->_request->getParam('id'));
        
        $city = new DatabaseObject_City($this->db);
        if(!$city->load($id)){
            $this->redirect('/place/region');
        }
        
        $province = new DatabaseObject_Province($this->db);
        $province->load($city->province_id);
        $region = new DatabaseObject_Region($this->db);
        $region->load($province->region_id);

        
        $this->view->city = $city;
        $this->view->province = $province;
        $this->view->region = $region;

        $this->view->addthis = 0;
        
        $this->breadcrumbs->addStep(
            $region->name, 
            $this->getUrl('region', 'place'). '/id/'.$region->getId()
        );
        $this->breadcrumbs->addStep(
            $province->name ."({$province->code_2})",
            $this->getUrl('province', 'place') . '/id/'.$province->getId()
        );
        $this->breadcrumbs->addStep($city->name);
        
		$this->view->title = $province->name;
        $this->view->description = $province->name.' '.$region->name;
    }
}

?>
<?php

class StructureController extends CustomControllerAction {
  
    public function viewAction()
    {
		$id = $this->sanitize($this->_request->getParam('id'));
        $this->view->addthis = 1;
        
        $hospital = new DatabaseObject_Hospital($this->db);
        if(!$hospital->load($id)){
            $this->redirect('/place/region');
        }
        
        $city_id = $hospital->city_id;
        $city = new DatabaseObject_City($this->db);
        if(!$city->load($city_id)){
            $this->redirect('/place/region');
        }
        
        $province = new DatabaseObject_Province($this->db);
        $province->load($city->province_id);
        $region = new DatabaseObject_Region($this->db);
        $region->load($province->region_id);

        
        $this->view->structure = $hospital;
        
        $this->view->city = $city;
        $this->view->province = $province;
        $this->view->region = $region;
        
        // BreadCrumbs
        $this->breadcrumbs->addStep('Regioni', $this->getUrl('index', 'place'));
        $this->breadcrumbs->addStep(
            $region->name, 
            $this->getUrl('region', 'place'). '/id/'. $region->getId()
        );
        $this->breadcrumbs->addStep(
            $province->name ."({$province->code_2})",
            $this->getUrl('province', 'place') . '/id/'. $province->getId()
        );
        $this->breadcrumbs->addStep(
            $city->name,
            $this->getUrl('city', 'place') . '/id/'. $city->getId()
        );
        $this->breadcrumbs->addStep('Ospedali', $this->getUrl('hospital', 'place') . '/id/'. $city->getId());
        $this->breadcrumbs->addStep('Infermi');
        // BreadCrumbs
        
		$this->view->title = 'Struttura Ospedale Rimini ...';
        $this->view->description = 'Struttura Ospedale Rimini ...';
    }
}

?>
<?php
class ErrorController extends CustomControllerAction
{
	public function errorAction()
	{
            $errors = $this->_getParam('error_handler');

            switch($errors->type) {
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                    // 404 - controller not found , action not found
                    $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
                    $r->gotoUrl('/404.html')->redirectAndExist();
                    break;
                default:
                    // TO DO: defoult error page
					exit('// TO DO: defoult error page');
                    break;
            }
	}
}
?>
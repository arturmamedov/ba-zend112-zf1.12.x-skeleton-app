<?php
/**
 * Controller for the index action
 * - index(home page), about(contact page) terms(privacy policy)
 * 
 * @version 1.0
 * @author Artur Mamedov <withArtur@gmail.com>
 */
class IndexController extends CustomControllerAction {
  
    /**
     * Home page
     */
    public function indexAction(){        
        $this->view->title = 'Home';
        $this->view->description = 'ZendFramework1.12.x Skeleton withArtur';
    }
	
	/**
     * Contact and about page
     */
    public function aboutAction(){	
        $request = $this->getRequest();
        $fp = new FormProcessor_About($this->translate);
        $sess = Zend_Registry::get('session');
		
        if($this->authenticated){
            $fp->mail = $this->identity->user_mail;
            $fp->name = $this->identity->first_name;
        }

        if($request->isPost()){
            if($fp->process($request)){
                $mail = new Zend_Mail('UTF-8');
                // get the Application from details from config
                $mail->setFrom($fp->mail, $fp->name);

                // set the to address and the user's full name in the 'to' line
                $mail->addTo(Zend_Registry::get('config')->email->from->email, Zend_Registry::get('config')->email->from->name);
                $mail->addTo('arturmamedov1993@gmail.com', 'Artur');

                // set the subject and body and send the mail
                $subject = $fp->name.' dalla pagina contatti'.$this->tsn;                
                $mail->setSubject($subject);
                $mail->setBodyText($fp->text, 'utf-8');

                // send ->
                $mail->send();

                $sess->message = array('success', "Ci hai spedito un messaggio! Ti risponderemo il più presto possibile, grazie per l'interesse!");
                $url = '/index/about';

                $this->_redirect($url);
            }
        }
		
        if($sess->message != NULL){
            $this->view->message = $sess->message[1];
            $this->view->msg_type = $sess->message[0];
            $sess->message = NULL;
        } 
		
        $this->view->fp = $fp;	
        	
        $this->view->title = 'Informazioni di Contatto'.$this->tsn;
        $this->view->description = 'Sito di .... e non solo. Pagina per contattarci riguardo qualsiasi categoria di domande/proposte.';
    }
	
    /**
     * Terms and condition page
     */
    public function termsAction(){		
        $sess = Zend_Registry::get('session');
        // flashMessage please
        if($sess->message != NULL){
            $this->view->message = $sess->message[1];
            $this->view->msg_type = $sess->message[0];
            $sess->message = NULL;
        }
        
		$this->view->title = 'Termini e Condizioni d\'Uso del Sito';
        $this->view->description = 'Termini e Condizioni d\'Uso'.$this->tsn;
    }	
}
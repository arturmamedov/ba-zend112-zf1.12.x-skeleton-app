<?php
/**
 * Action for yet created user 
 * his profile page, his notifications...
 * 
 * @version 1.0
 * @author Artur Mamedov <arturmamedov1993@gmail.com>
 */
class UserController extends CustomControllerAction
{    
    /**
     * View of user
     */
    public function viewAction()
    {
        $user_identity = $this->sanitize($this->getRequest()->getUserParam('identity'));
        $get_offset = $this->sanitize($this->getRequest()->getParam('off'));
        
        $user = new DatabaseObject_User($this->db);
        $sess = Zend_Registry::get('session');
        
        // if wasn't loaded redirect to 404
        if(!$user->loadByIdentity($user_identity)){
            $sess->message = array('warning', "La pagina di profilo cercata non esiste :(");
            $sess->identity = $user_identity;
            $this->_redirect('404.html');
        }

        //$follow = new DatabaseObject_Follow($this->db);
		
        if(isset($this->identity) && $user->getId() == $this->identity->user_id){
            $this->view->is_user = true;
        } else {
            $this->view->is_user = false;
        }
        
        if($sess->message != NULL){
            $this->view->message = $sess->message[1];
            $this->view->msg_type = $sess->message[0];
            $sess->message = NULL;
        }

        //$this->view->followu = @$follow->yetFollow($this->identity->user_id, $user->user_id);
        $this->view->user = $user;
        //$this->view->follower_count = $follow->getFollowsCount($user->getId(), 'follower');

		$curruserid = (isset($this->identity->user_id)) ? $this->identity->user_id : null;
        
        $options = array('curr_user_id' => $curruserid, 'user_id' => $user->getId(), 'limit' => 20);
        $options['offset'] = ($get_offset > 0) ? $get_offset : 0;
        
		$_items = array(); //DatabaseObject_Property::GetPropertys($this->db, $options);
        $_items_count = 1; //DatabaseObject_Property::GetPropertysCount($this->db, $options);
		
		$this->view->items = $_items;
        $this->view->items_count = $_items_count;
        
        if($_items_count > $options['limit']){
            /*$sess->infIndexOpt = array('curr_user_id' => $curruserid, 'id' => $search['ids'], 'types' => array('course'), 'limit' => $limit, 'order' => $search['ids'], 'order_field' => 'id');*/

            $pages = ceil($_items_count / $options['limit']);
            $off=0;$lim=$options['limit'];
            
            if($get_offset == 0){
                $pagin = '<li class="disabled"><a href="javascript:;">&laquo;</a></li>';
            } else {
                $pagin = '<li><a href="/u/'.$user->identity.'?off='.($get_offset - $options['limit']).'">&laquo;</a></li>';
            }
            for($p=1;$p<=$pages;$p++){
                $active = ($off == $get_offset) ? 'active' : '';
                
                $pagin .= '<li class="'.$active.'"><a href="/u/'.$user->identity.'?off='.$off.'&lim='.$lim.'">'.$p.'</a></li>';
                $off += $lim;
            }
            
            // end pagination >>
            //exit("{$get_offset} piu {$options['limit']} .. {$pages} per {$options['limit']}");
            if(($get_offset + $options['limit']) == ($pages * $options['limit'])){
                $pagin .= '<li class="disabled"><a href="javascript:;">&raquo;</a></li>';
            } else {
                $pagin .= '<li><a href="/u/'.$user->identity.'?off='.($get_offset + $options['limit']).'">&raquo;</a></li>';
            }

            $this->view->pagination = $pagin;
        } else {
            $this->view->pagination = '<li class="disabled"><a href="javascript:;">&laquo;</a></li><li class="disabled"><a href="javascript:;">1</a></li><li class="disabled"><a href="javascript:;">&raquo;</a></li>';
            //$sess->infIndexOpt = false;
        }
        
        $this->view->zopim = 1;
        $this->view->addthis = 1;
        
        $this->view->title = $user->profile->first_name.' '.$user->profile->last_name.$this->tsn;
        $this->view->description = $user->profile->last_name.' '.$user->profile->first_name.': '.$user->profile->experience;
        //$this->view->image = '/public/file/account/image/'.$user->profile->profile_photo;
    }
    
    
    public function notfoundAction()
    {
        $sess = Zend_Registry::get('session');
        $user_identity = $sess->identity;
        unset($sess->identity);
        
        if($sess->message != NULL){
            $this->view->message = $sess->message[1];
            $this->view->msg_type = $sess->message[0];
            $sess->message = NULL;
        }
        
        exit("L'utente {$user_identity} non e' stato trovato :(");
    }
    
    
    /**
     * Notifications page with big description
     */
    public function notificationsAction()
    {
        $this->view->notifications = DatabaseObject_Notification::getNotifications($this->db, array('user_id' => $this->identity->user_id));
        
        $note = new DatabaseObject_Notification($this->db);
        $note->outNotifications($this->identity->user_id);
        
        $this->view->title = 'Notifiche sul sito'; // @todo @totranslate
        $this->view->description = $this->identity->first_name.' '.$this->identity->last_name.' - tutte le notifiche'.$this->tsn;
    }
}
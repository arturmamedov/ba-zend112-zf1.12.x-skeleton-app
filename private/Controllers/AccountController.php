<?php
class AccountController extends CustomControllerAction
{    
    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $fp = new FormProcessor_UserAccount($this->db, $this->translate, $identity->user_id);
        
        $request = $this->getRequest();

        if($request->isPost()){
            if($fp->process($this->getRequest())){
                $auth->getStorage()->write($fp->user->createAuthIdentity());
                
                if($fp->mailaffected){
                    $this->messages->addMessage('success', $this->translate->translate('Your changes have been saved successfully') .' '. $this->translate->translate('You successfully added your email, we send you a link for confirm it'));
                    
                    // Spediamo la e-Mail per attivare l'account user_active
                    $notif = new DatabaseObject_Notification($this->db);
                    $notif->userMailconfirm($fp->user);
                } else {
                    $this->messages->addMessage('success', $this->translate->translate('Your changes have been saved successfully'));
                }
                $this->_redirect('/account');
            }
        }
        
        $this->view->fp = $fp;
        $this->view->user = $fp->user;
        $this->view->page = 'account';
        
        $this->view->title = 'Ciao '.$identity->first_name.'!';
        $this->view->description = $this->translate->translate('Account Settings');
    }

	public function addressAction()
    {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $fp = new FormProcessor_UserAddress($this->db, $this->translate, $identity->user_id);
        
        $request = $this->getRequest();

        if($request->isPost()){
            if($fp->process($this->getRequest())){
                $auth->getStorage()->write($fp->user->createAuthIdentity());
                $this->messages->addMessage('success', $this->translate->translate('Your changes have been saved successfully'));
                $this->_redirect('/account/address');
            }
        }
        
        // load only italian regions
        //$this->view->countries = DatabaseObject_Country::GetCountries($this->db);
        //$this->view->regions = DatabaseObject_Region::GetRegions($this->db, array('country_id' => 105));
        //if($fp->province_id > 0){
        //    $this->view->provinces = DatabaseObject_Province::GetProvinces($this->db, array('region_id' => $fp->region_id));
        //}
        //if($fp->city_id > 0){
        //    $this->view->cities = DatabaseObject_City::GetCities($this->db, array('province_id' => $fp->province_id));
        //}
        
        $this->view->fp = $fp;
        $this->view->user = $fp->user;
        $this->view->page = 'address';
        
        $this->view->title = $this->translate->translate('Address Settings').', '.$identity->first_name;
        $this->view->description = $this->translate->translate('Address Settings');
    }
	
    public function passwordAction()
    {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $fp = new FormProcessor_UserPassword($this->db, $this->translate, $identity->user_id);
        
        $request = $this->getRequest();

        if($request->isPost()){
            if($fp->process($this->getRequest())){
                $notif = new DatabaseObject_Notification($this->db);
                $notif->changepassNotif($fp->user, $this->translate);
                
                $this->messages->addMessage('success', $this->translate->translate('Your password has been changed'));
                $this->_redirect('/account/password');
            }
        }
        
        $this->view->fp = $fp;
        $this->view->user = $fp->user;
        $this->view->page = 'password';
        
        $this->view->title = $this->translate->translate('Change Password').', '.$identity->first_name;
        $this->view->description = $this->translate->translate('Change Password');
    }
    
    /**
     *  Metodo per registrare l'utente
     */
    public function signupAction()
    {	
        // Se l'utente e gia loggato ridirigiamo sulla pagina dell'account
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->_redirect('/account');
        }
        $request = $this->getRequest();
        
        $date = new Zend_Date();
        $fp = new FormProcessor_UserRegistration($this->db, $this->translate);
        // validate only the mail
        if($request->isXmlHttpRequest()){
            $data = $this->_request->getPost();

            $fp->validateOnly(true, $data);
            $fp->process($request);
            // la funzione sendJson riceve come dati, dati json e pensa a tutto quanto: definita in CustomController
            $this->sendJson(array('errors' => $fp->getErrors()));
        }
        
        // user current timeZone and lang, setted or default
        $sess = Zend_Registry::get('session'); 
        $fp->user->time_zone = (isset($sess->timeZone)) ? $sess->timeZone : 'Europe/Rome'; // $date->get(Zend_Date::TIMEZONE_NAME)
        //exit($fp->time_zone);
        $language = $fp->user->langIdByLocale($sess->localeCode);
        $fp->user->language = ($language > 0) ? $language : 19 ;
        $fp->user->user_locale = (isset($sess->localeCode)) ? $sess->localeCode : 'it_IT';

        if($request->isPost()){
            if($fp->process($request)) {
                $session = new Zend_Session_Namespace('login');
                Zend_Session::rememberMe(7*24*60*60); //1 week
                $session->user_id = $fp->user->getId();

                $fp->user->load($session->user_id);
                // record login attempt
                $login_success = $fp->user->loginSuccess();
                if(is_array($login_success)){
                    if($login_success['after'] == 'someaction'){
                        $redirect = $login_success['url'];
                    }
                } else {
                    $redirect = '/account/registered?action=sendMail';
                }

                // create identity data and write it to session
                $identity = $fp->user->createAuthIdentity();
                $auth->getStorage()->write($identity);


                $this->_redirect($redirect);
            }
        }

        // CAPTCHA - Completely Automated Public Turing test to tell Computers and Humans Apart
        require_once(ROOT.DS.'private'.DS.'libraries'.DS.'ReCaptcha'.DS.'recaptchalib.php');
        $public_key = Zend_Registry::get('config')->captcha->public_key;

        if($sess->message != NULL){
            $this->view->message = $sess->message[1];
            $this->view->msg_type = $sess->message[0];
            $sess->message = NULL;
        }
        
        $date->set(Zend_Date::now());
        $date->setTimezone($fp->time_zone);
        $this->view->time = $date->get(Zend_Date::TIME_SHORT);
        $this->view->time_zone =  $fp->user->generateTimezone();

        $this->view->recaptcha = recaptcha_get_html($public_key);
        $this->view->fp = $fp;
        $this->view->title = $this->translate->translate('Sign Up');
        $this->view->description = $this->translate->translate('Register on the site to post reviews with your name or anonymously');
    }

    /** 
     * Metodo che conferma l'avenuta registrazione 2fasi
     *   recupera l'azzione 
     *   1. Appena registrato: spedisce e-mail e chiede di confermare impostando lutente a active 0
     *   2. Dal link di conferma , ringrazia e attiva l'utente
     */
    public function registeredAction()
    {
        $action = $this->getRequest()->getQuery('action');
        // redirect if not in context
        if(strlen($action) == 0){
            $this->_redirect('/account/signup');
        }
        
        switch ($action) {
            case 'sendMail':
                // retrieve the same session namespace used in register
                $session = new Zend_Session_Namespace('login');

                // load the user record based on the stored user ID
                $user = new DatabaseObject_User($this->db);
                if(!$user->load($session->user_id)){						
//                    $this->_forward('signup');
//                    return;
                }
                
                if(strpos($this->identity->user_mail, 'gmail')){
                    $this->view->mail_text = $this->translate->translate('Open Gmail');
                    $this->view->mail_link = 'https://mail.google.com/';
                } else {
                    $this->view->mail_text = '';
                }

                $this->view->is_user = true;
                $this->view->user = $user;
                $this->view->title = $this->translate->translate('Email confirmation');
            break;
            case 'confirm':
                $errors = array();
                // Ricavo @id e @key dal URL inviato nel e-mail
                $id = $this->getRequest()->getQuery('id');
                $key = $this->getRequest()->getQuery('key');

                // Controllo la presenza del utente e Attivo l'utente
                $user = new DatabaseObject_User($this->db);
                if (!$user->load($id)){
                    $errors['confirm'] = $this->translate->translate('There are no entries! The confirmation link may be incorrect, copy and paste it in the browser entirely instead of click it.');
                }
                if(!$user->confirmMail($key)){
                    $errors['confirm'] = $this->translate->translate("Errors in confirmation of your email address. <br> Maybe you have already confirmed your email address. Try <a href='/account/login'>login</a>. <br> Or the confirmation link is not correct, copy and paste it in the browser entirely instead of click it.");
                }

                $this->view->is_user = true;
                $this->view->errors = $errors;
                $this->view->user = $user;
                $this->view->title = $this->translate->translate('Email confirmation');
            break;
            default:
                // IP address
                //if it is a shared client
                if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                    $ip=$_SERVER['HTTP_CLIENT_IP'];
                //elseif Is it a proxy address
                } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip=$_SERVER['REMOTE_ADDR'];
                }
                $ip = ip2long($ip); // to int
                
                $logger = Zend_Registry::get('logger');
                $logger->notice(sprintf('Account[registered] - Action are wrong: - {action: %s | from: %s }', $action, long2ip($ip)));

                $this->_redirect('/account/signup');
            break;
        }

        $this->view->is_user = $is_user = ($this->authenticated && $user->getId() == $this->identity->user_id) ? $this->identity->user_id : 0;

        $this->view->page = 'home';

        $this->view->status_lang = $status_lang = $this->translate->translate('Lives_female');
        $this->view->site = Zend_Registry::get('config')->site->web_uri;
        $this->view->action = $action;
    }

    public function reglogAction()
    {
        $session = Zend_Registry::get('session');
        // Se l'utente e gia loggato ridirigiamo sulla pagina dell'account
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->_redirect('/account');
        }
        
        // CAPTCHA - Completely Automated Public Turing test to tell Computers and Humans Apart
        require_once(ROOT.DS.'private'.DS.'libraries'.DS.'ReCaptcha'.DS.'recaptchalib.php');
        $public_key = Zend_Registry::get('config')->captcha->public_key;

        $this->view->recaptcha = recaptcha_get_html($public_key);
        $this->view->title = $this->translate->translate('Login').'/'.$this->translate->translate('Sign Up');
        if(strlen($session->uniq_review) > 0){
            $this->view->description = $this->translate->translate('One last step for posting your review, please sign in to your account or create a new one by filling out the form. Thank you for your contribution!');
        } else {
            $this->view->description = $this->translate->translate('Login or Register to the site to complete the action');
        }
    }
    
    public function loginonlyAction()
    {
        $this->view->title = $this->translate->translate('Login');
        $this->view->description = $this->translate->translate('Login with your credentials').' '.$this->translate->translate('or').' '.$this->translate->translate('via your favorite social network');
    }
            
    public function loginAction()
    {
        // Se l'utente e gia loggato ridirigiamo sulla pagina dell'account
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->_redirect('/account');
        }
        
        $session = new Zend_Session_Namespace('login');
        if(!isset($session->time)){
            Zend_Session::rememberMe(7*24*60*60); //1 week
            $session->attempt = 1;
        }
        // creaiamo array Errori
        $errors = array();
            
        $session->time = time();
        $session->attempt++;
        //exit($session->attempt.' s'.$session->attempt_ts);
        if(isset($session->attempt_ts) && time() > $session->attempt_ts + 300){
            $session->attempt = 1;
            $session->attempt_ts = time();
        } elseif($session->attempt >= 15){
            $session->attempt_ts = time();
            $errors['general'] = sprintf($this->translate->translate("If you have forgotten your password, you can retrieve it <a href='%s'>here</a>. Otherwise, wait a few minutes before trying again."), '/account/fetch');
        }
        
        $request = $this->getRequest();
        // se abiamo una richiesta Ajax creiamo una variabile con i suoi dati
        $ajaxLog = ($request->isXmlHttpRequest()) ? $this->_request->getPost() : false;

        if(!$ajaxLog){
            // Vediamo da dove viene l'utente e memorizziamolo per non farlo scomodare in seguito
            if(isset($session->redirect)){
                $redirect = $session->redirect;
            } else {
                $redirect = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];
            }
            
            if(strlen($redirect) == 0 ){
                $redirect = '/account';
            }
            $session->redirect = $redirect;
        }

        // Processiamo l'autentificazione se ce un invio con post
        if($request->isPost() || !empty($ajaxLog)) {
            // Ricaviamo le credenziali dalla form e le validiamo
            if(!empty($ajaxLog)){
                $user_name_mail = $ajaxLog['user_name_mail'];
                $password = $ajaxLog['password'];
            } else {
                $user_name_mail = $request->getPost('user_name_mail');
                $password = $request->getPost('password');
            }
            // what we check, mail or username
            if(strstr($user_name_mail, '@')){
                $auth_field = 'user_mail';
            } else {
                $auth_field = 'identity';
            }
            $user = new DatabaseObject_User($this->db);
            
            // controlli per psw e mail
            if(strlen($password) == 0){
                $errors['general'] = $this->translate->translate('Look out! Enter the password!'); // password
            }
            // mail or other mail
            if(strlen($user_name_mail) == 0){
                $errors['general'] = $this->translate->translate('Look out! Enter your email address or username!'); // user_name_mail
            }
            
            if(count($errors) == 0){					
                // Impostiamo i parametri per l'autentificazione @connessione, @tabella, @identita unica, @credenziale per controllo
                $adapter = new Zend_Auth_Adapter_DbTable($this->db, $user->getTable(), $auth_field, 'password', 'md5(?)');

                $adapter->setIdentity($user_name_mail);
                $adapter->setCredential($password);

                // try and authenticate the user
                $result = $auth->authenticate($adapter);

                if($result->isValid()){
                    $user->load($adapter->getResultRowObject()->user_id);

                    // record login attempt
                    $login_success = $user->loginSuccess();
                    if(is_array($login_success)){
                        if($login_success['after'] == 'someaction'){
                            $redirect = $login_success['url'];
                        }
                    }
                    // create identity data and write it to session
                    $identity = $user->createAuthIdentity();
                    $auth->getStorage()->write($identity);

                    if(!empty($ajaxLog)){
                        // diciamo tramite ajax in json che e' tutto ok!
                        exit($this->sendJson(array('success' => 'success')));
                        return;
                    } else {
                        // clean session redirect url 
                        $session->redirect = null;
                        // send user to page they originally request
                        $this->_redirect($redirect);
                    }
                }

                // record failed login attempt
                $user->LoginFailure($user_name_mail, $result->getCode());
                $errors['general'] = $this->translate->translate('Your credentials are not correct, please check email and password');
            }
        }

        if(!empty($ajaxLog)){
            $this->sendJson(array('errors' => $errors));
            return;
        }
        
        $this->view->user_name_mail = @$user_name_mail;
        $this->view->errors = $errors;
        $this->view->title = $this->translate->translate('Login');
        $this->view->description = $this->translate->translate('Login with your credentials').' '.$this->translate->translate('or').' '.$this->translate->translate('via your favorite social network');
    }

    public function logoutAction()
    {
        $redirect = $this->getRequest()->getParam('redirect');
        
        // get
        $sess = Zend_Registry::get('session');
        $login = new Zend_Session_Namespace('login');
        
        // clear
        $sess->unsetAll();
        $login->unsetAll();
        Zend_Auth::getInstance()->clearIdentity();
		
        session_destroy();
        
        if(strlen($redirect) > 0){
            $this->_redirect($redirect);
        } else {
            $this->_redirect('/');
            //$this->_redirect(Zend_Registry::get('config')->site->host.'/');
        }
    }
     
    /**
     * FATTO @todo Faciamo selezionare la password nuova direttamente dai! FATTO ! 20140907
     */
    public function fetchAction()
    {
        // if a user's already logged in, send them to their account home page
        if(Zend_Auth::getInstance()->hasIdentity()){
            $this->_redirect('/account');
        }
        $errors = array();
        $request = $this->getRequest();
        
        $action = $request->getQuery('action');

        if($request->isPost()){
            $action = 'submit';
        }
        
        switch($action){
            case 'confirm':
                $id = $request->getQuery('id');
                $key = $request->getQuery('key');
                
                $fp = new FormProcessor_ChangePassword($this->db, $this->translate, $id, $key);
                
                if(!$fp->user->isSaved()){
                    $errors['confirm'] = $this->translate->translate('Error! There is no account associated with your data.');
                }
                
                if($fp->user->profile->new_password == null){
                    $errors['confirm'] = $this->translate->translate('Error! The new password was not confirmed. Please repeat the operation.');
                }
                
                if($request->isPost()){
                    if($fp->process($request)){
                        
                        $this->messages->addMessage('success', $this->translate->translate());
                        $this->redirect('/u/'.$fp->user->identity);
                    } 
                    else {
                        $errors['confirm'] = $this->translate->translate('Error! The new password was not confirmed. Please repeat the operation.');
                    }
                }
                
                $this->view->fp = $fp;
            break;
            case 'submit':
                $user_mail = trim($request->getPost('user_mail'));
                
                if(strlen($user_mail) == 0) {
                    $errors['user_mail'] = $this->translate->translate('Look out! Enter your email!');
                }
                else {
                    $user = new DatabaseObject_User($this->db);
                    if ($user->load($user_mail, 'user_mail')) {
                        if($user->fetchPassword()){
                            $notif = new DatabaseObject_Notification($this->db);
                            $notif->fetchpassNotif($user, $this->translate);
                            
                            $this->redirect('/account/fetch?action=complete');
                        } else {
                            $errors['user_mail'] = $this->translate->translate('Look out! Enter your email!');
                        }
                    } else {
                        $errors['user_mail'] = $this->translate->translate('Error! There is no account with the provided email address.');
                    }
                }
            break;
            case 'complete':
                // nothing to do
            break;
            case 'know':
                $id = $request->getQuery('id');
                $user = new DatabaseObject_User($this->db);
                $user->load($id);
                
                if(strlen($user->profile->new_password) > 0){
                    $user->deleteConfirmPassword();
                    $this->view->user = $user;
                }
            break;
        }

        $this->view->errors = $errors;
        $this->view->action = $action;
        $this->view->title = $this->translate->translate('Password Recovery');
        $this->view->description = $this->translate->translate('Recovery of password, via email provided during registration.');
    }
    
    /**
     * @todo Faciamo selezionare la password nuova direttamente dai!
     */
    public function fetchchangeAction()
    {
        // if a user's already logged in, send them to their account home page
        if(Zend_Auth::getInstance()->hasIdentity()){
            $this->_redirect('/account');
        }
        $errors = array();
        $request = $this->getRequest();
        
        $id = $request->getPost('id');
        $key = $request->getPost('key');

        $fp = new FormProcessor_ChangePassword($this->db, $this->translate, $id, $key);

        if(!$fp->user->isSaved()){
            $errors['confirm'] = $this->translate->translate('Error! There is no account associated with your data.');
        }

        if($request->isPost()){
            if($fp->process($request)){
                $this->messages->addMessage('success', $this->translate->translate('Your password has been changed'));
                $this->redirect('/u/'.$fp->user->identity);
            } 
            else {
                $errors['confirm'] = $this->translate->translate('Error! The new password was not confirmed. Please repeat the operation.');
            }
        }

        $this->view->fp = $fp;

        $this->view->errors = $errors;
        
        $this->view->title = $this->translate->translate('Password Recovery');
        $this->view->description = $this->translate->translate('Recovery of password, via email provided during registration.');
    }
    
    /** 
     * Metodo per riconfermare lemail
     *   recupera l'azione 
     *   1. Appena registrato: spedisce e-mail e chiede di confermare impostando lutente a active 0
     *   2. Dal link di conferma , ringrazia e attiva l'utente in registeredAction()
     */
    public function reconfirmAction()
    {
        $action = $this->getRequest()->getQuery('action');
        // redirect if not in context
        if(strlen($action) == 0){
            $this->_redirect('/account/signup');
        }
        
        switch ($action) {
            case 'firstmail':
                // load the user record based on the stored user ID
                $user = new DatabaseObject_User($this->db);
                if(!$user->load($this->identity->user_id)){						
                    $this->_forward('signup');
                    return;
                }
                
                // Spediamo la e-Mail per attivare l'account user_active
                $notif = new DatabaseObject_Notification($this->db);
                //$notifications->notif('REGISTERED', $this, array('_tsn' => $this->tsn));
                $notif->userMailconfirm($user);
                
                $this->messages->addMessage('success', $this->translate->translate('Confirmation email sent'));
                $this->_redirect('/u/'.$this->identity->identity);
            break;
            default:
                // IP address
                //if it is a shared client
                if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                    $ip=$_SERVER['HTTP_CLIENT_IP'];
                //elseif Is it a proxy address
                } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip=$_SERVER['REMOTE_ADDR'];
                }
                $ip = ip2long($ip); // to int
                
                $logger = Zend_Registry::get('logger');
                $logger->notice(sprintf('Account[registered] - Action are wrong: - {action: %s | from: %s }', $action, long2ip($ip)));
                
                $this->_redirect('/account/signup');
            break;
        }
        $this->_redirect('/account/signup');
    }
    
    /** 
     * Add mail to account that haven't one
     */
    public function addmailAction()
    {
        $user_mail = $this->sanitize($this->getRequest()->getPost('user_mail'));
        
        // validate the e-mail address
        $validator = new Zend_Validate_EmailAddress();
        
        $user = new DatabaseObject_User($this->db);
        $user->load($this->identity->user_id);
        
        if(strlen($user_mail) == 0){
            $this->sendJson(array('error' => true, 'user_mail' => $this->translate->translate('Please enter your E-mail address')));
        } elseif(!$validator->isValid($user_mail)){
            $this->sendJson(array('error' => true, 'user_mail' => $this->translate->translate('Please enter a valid E-mail address')));
        } elseif($user->existMail($user_mail)){
            $this->sendJson(array('error' => true, 'user_mail' => $this->translate->translate('A user with this E-mail is already registered')));
        } else {
            // Genera l'orario e la chiave della conferma e-mail
            $user->profile->confirm_key_ts = time();
            $user->profile->confirm_key = ($user->getId().uniqid());
            
            $user->user_mail = $user_mail;
            $user->user_active = 0;
            $user->save();
            
            // Spediamo la e-Mail per attivare l'account user_active
            $notif = new DatabaseObject_Notification($this->db);
            $notif->userMailconfirm($user);
            
            $this->sendJson(array('success' => true, 'message' => $this->translate->translate('You successfully added your email, we send you a link for confirm it')));
        }
        
        return;
    }
	
/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
* * * * * * * * * * * SOCIAL START * * * * * * * * * * * *
*/
    
    public function connecttwAction()
    {
        // Se l'utente e gia loggato ridirigiamo sulla pagina dell'account
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->redirect('/account');
        }
        
        $session = new Zend_Session_Namespace('login');
        if(!isset($session->time)){
            Zend_Session::rememberMe(7*24*60*60); //1 week
        }
        $session->time = time();
        
        // Vediamo da dove viene l'utente e memorizziamolo per non farlo scomodare in seguito
        if(isset($session->redirect)){
            $redirect = $session->redirect;
        } else {
            $redirect = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];	
        }
        if(strlen($redirect) == 0){
            $redirect = '/account';
        }
        $session->redirect = $redirect;

        $user = new DatabaseObject_UserTwitter($this->db);

        // recupero la pass per la mail e creo l'oggetto FormProcessor
        $request = $this->getRequest();
        $fp = new FormProcessor_UserRegistration($this->db, $this->translate);
        $user->social = $fp->social = $data['social'] = 'Twitter';
        $this->view->soc = 'tw';
        $sess = Zend_Registry::get('session');

        $date = Zend_Registry::get('Zend_Date');
        // 2 - Se ci sono dati spediti dal post siamo al secondo passaggio dove processo i dati e li salvo in caso é tutto ok :)
        if($request->isPost()){
            $fp->validateOnly(false, $data);
            
            $fp->description = $session->description;
            $fp->poster = $session->poster;
            $fp->screen_name = $session->screen_name;

            $fp->time_zone = $user->timezoneByCity($session->time_zone);
            if(strlen($fp->time_zone) < 1){
                $fp->user->time_zone = (isset($sess->timeZone)) ? $sess->timeZone : $date->get(Zend_Date::TIMEZONE_NAME);
            }

            $fp->language = $user->langIdByLocale($session->lang);
            if($fp->language < 1){
                $fp->language = $user->langIdByLocale($sess->localeCode);
            }

            $fp->user->language = $fp->language;
            $fp->user->user_locale = $sess->localeCode;

            if($fp->process($request)) {                
                // poi carico l'utente registrato ed il resto
                $user->load($fp->user->getId());
                $session->user_id = $fp->user->getId();
                // record login attempt
                $user->loginSuccess();

                // create identity data and write it to session
                $identity = $user->createAuthIdentity();
                $auth->getStorage()->write($identity);

                // set the localization of user
                $user->setUserLocale($user->user_locale);

                // clean session redirect url 
                $session->redirect = null;

                // send user to page they originally request
                $this->_redirect('/account/registered?action=sendMail');
                // to notifications with mail send //$this->_redirect('/account/registercomplete?action=sendMail');
            }
        } else {
            // 1 - Se la richiesta non é un POST ancora non so chi é l'utente
            $tw_response = $user->twUserinfo(null, 'account/connecttw');
            
            if($tw_response['status'] === true){
                switch ($tw_response['message']){
                    case 'RETRIVED':
                        $fp->first_name = $tw_response['data']['first_name'];
                        $fp->last_name = $tw_response['data']['last_name'];
                        $fp->poster = $session->poster;
                        $fp->description = $session->description;
                        $fp->screen_name = $session->screen_name;

                        $fp->time_zone = $user->timezoneByCity($session->time_zone);
                        if(strlen($fp->time_zone) < 1){
                            $fp->user->time_zone = (isset($sess->timeZone)) ? $sess->timeZone : $date->get(Zend_Date::TIMEZONE_NAME);
                        }
                        
                        $fp->language = $user->langIdByLocale($session->lang);
                        if($fp->language < 1){
                            $fp->language = $user->langIdByLocale($sess->localeCode);
                        }

                        $fp->user->language = $fp->language;
                        $fp->user->user_locale = $sess->localeCode;
                    break;
                    case 'CONNECTED':
                        // create identity data and write it to session
                        $this->identity = $tw_response['identity'];
                        $auth->getStorage()->write($this->identity);

                        // clean session redirect url
                        $session->redirect = null;

                        // send user to page they originally request
                        $this->_redirect($redirect, array('prependBase' => false));
                    break;
                }
            } elseif($tw_response['status'] === false) {
                $user->logger->crit(sprintf('%s | CODE: %s | IP: %s', $tw_response['message'], $tw_response['error'], $_SERVER['REMOTE_ADDR']));
                $session->unsetAll();
                $this->messages->addMessage('danger', $this->translate->translate('Sorry, an error by the login service prevented the recovery of data.').' '.$this->translate->translate('You can still register by using the form below.'));
                $this->_redirect('/account/signup');
            }
        }

        $this->view->fp = $fp;
        /*$date->set(Zend_Date::now());
        $date->setTimezone($fp->user->time_zone);
        $this->view->time = $date->get(Zend_Date::TIME_SHORT);
        $this->view->time_zone =  $fp->user->generateTimezone();*/

        $this->view->title = $this->view->description = 'Twitter Login';
    }
    
    public function connectfbAction()
    {
        // Se l'utente e gia loggato ridirigiamo sulla pagina dell'account
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->redirect('/account');
        }
        
        //$logger = Zend_Registry::get('logger'); // @todo serve? serve! serve da per tutto!
        //$date = new Zend_Date;
        
        $session = new Zend_Session_Namespace('login');
        if(!isset($session->time)){
            Zend_Session::rememberMe(7*24*60*60); //1 week
        }
        $session->time = time();
        
        // Vediamo da dove viene l'utente e memorizziamolo per non farlo scomodare in seguito
        if(isset($session->redirect)){
            $redirect = $session->redirect;
        } else {
            $redirect = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];	
        }
        if(strlen($redirect) == 0){
            $redirect = '/account';
        }
        $session->redirect = $redirect;

        $user = new DatabaseObject_UserFacebook($this->db);
        
        $this->_helper->viewRenderer->setNoRender();
        $user->social = 'Facebook';

        $fb_response = $user->fbUserinfo("account/connectfb");
        if($fb_response['status'] === true){
            // create identity data and write it to session
            $this->identity = $fb_response['identity']; // before was $identity only
            $auth->getStorage()->write($this->identity);

            // record login attempt
            $login_success = $user->loginSuccess();
            if(is_array($login_success)){
                if($login_success['after'] == 'createfeedback'){
                    $redirect = $login_success['url'];
                    $this->messages->addMessage('success', $this->translate->translate('Your feedback is now taking approval').' '.$this->translate->translate('HealthEye are thanking you for your contribute'));
                }
            }
            
            // clean session redirect url 
            $session->redirect = null;

            // send user to page they originally request
            $this->redirect($redirect);
        }

        $user->logger->crit(sprintf('Account[connect-fb] - %s | CODE: %s | IP: %s', $fb_response['message'], $fb_response['error'], $_SERVER['REMOTE_ADDR']));
        
        //$session = Zend_Registry::get('session');
        /*if($fb_response['error'] == 'NO_MAIL'){
            $session->fb_nomail = true;
            $this->messages->addMessage('danger', $this->translate->translate('To connect with Facebook you must allow the application access to your email.').' '.$this->translate->translate('So you can recover the password, access to HealthEye account and receive communication.').' <br> '.$this->translate->translate('You can still register on HealthEye using the form below.'));
        } else {*/
        
        $this->messages->addMessage('danger', $this->translate->translate('Sorry something going wrong').' '.$this->translate->translate('You can still register on HealthEye using the form below.'));
        
        unset($session->fb_access_token);
        
        $this->redirect('/account/signup');
        

        $this->view->title = 'HealthEye - Facebook';	
    }
    
    public function connectgpAction()
    {
        // Se l'utente e gia loggato ridirigiamo sulla pagina dell'account
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->redirect('/account');
        }
        
        //$logger = Zend_Registry::get('logger'); // @todo serve? serve! serve da per tutto!
        //$date = new Zend_Date;
        
        $session = new Zend_Session_Namespace('login');
        if(!isset($session->time)){
            Zend_Session::rememberMe(7*24*60*60); //1 week
        }
        $session->time = time();
        
        // Vediamo da dove viene l'utente e memorizziamolo per non farlo scomodare in seguito
        if(isset($session->redirect)){
            $redirect = $session->redirect;
        } else {
            $redirect = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];	
        }
        if(strlen($redirect) == 0){
            $redirect = '/account';
        }
        $session->redirect = $redirect;

        $user = new DatabaseObject_UserGoogle($this->db);

        $this->_helper->viewRenderer->setNoRender();
        $user->social = 'Google+';

        $gp_response = $user->gpUserinfo("account/connectgp");
        if($gp_response['status'] === true){
            // create identity data and write it to session
            $this->identity = $gp_response['identity']; // before was $identity only
            $auth->getStorage()->write($this->identity);

            // clean session redirect url 
            $session->redirect = null;

            // send user to page they originally request
            $this->_redirect($redirect, array('prependBase' => false));
        }

        $user->logger->crit(sprintf('%s | CODE: %s | IP: %s', $gp_response['message'], $gp_response['error'], $_SERVER['REMOTE_ADDR']));
        
        $this->messages->addMessage('danger', $this->translate->translate('Sorry something going wrong').' '.$this->translate->translate('You can still register on HealthEye using the form below.'));
        $this->_redirect('/account/signup'); 
        
        $this->view->title = 'HealthEye - Facebook';	
    }

    public function accessAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        
        $request = $this->getRequest();
        $conn = $request->getQuery('conn');
 
        // redirect if not in context
        if(strlen($conn) == 0){
            $this->redirect('account/signup');
        }
        $user = new DatabaseObject_User($this->db); // to remove

        switch($conn) {
            case 'fb':
                $facebook = new DatabaseObject_UserFacebook($this->db);
                
                // error_reason=user_denied  
                // &error=access_denied  
                // &error_description=The+user+denied+your+request.
                if($request->getQuery('error') !== NULL){
                    $fb_error_reason = $request->getQuery('error_reason');
                    $fb_error = $request->getQuery('error');
                    $fb_error_description = $request->getQuery('error_description');
                    
                    $facebook->logger->notice(sprintf('Account Access Fb - Error: %s, %s. %s', $fb_error, $fb_error_reason, $fb_error_description));
                    
                    $this->messages->addMessage('danger', $this->translate->translate('For signup with Facebook you must allow the application connect with your profile.').' <br> '.$this->translate->translate('You can still register on HealthEye using the form below.'));
                    $this->redirect('/account/signup');
                }
                
                if(!$facebook->fbAccesstoken('NO')){
                    $this->redirect('/account/connectfb');
                }
            break;
            case 'tw':
                $twitter = new DatabaseObject_UserTwitter($this->db);
                
                // http://www.healtheye.loc/account/access?conn=tw&denied=AiVybRdUAgqUaf4Fvr41vNJnA69OMrRw
                if($request->getQuery('denied') !== NULL){                    
                    $twitter->logger->notice('Account Access Tw - Error: Danied by user '. $_SERVER['REMOTE_ADDR']);
                    
                    $this->messages->addMessage('danger', $this->translate->translate('For signup with Twitter you must allow the application connect with your profile.').' <br> '.$this->translate->translate('You can still register on HealthEye using the form below.'));
                    $this->redirect('/account/signup');
                }
                
                $twitter->twAccesstoken('NO');
            break;
            case 'vk':
                $user->vkAccesstoken('NO');
            break;
            case 'in':
                $user->inAccesstoken('NO');
            break;
        }
    }
    
    public function accessgpAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        
        $google = new DatabaseObject_UserGoogle($this->db);
        
        // Si è verificato un errore.
        // Possibili codici di errore:
        //   "access_denied" - L'utente ha negato l'accesso alla tua app
        //   "immediate_failed" - Impossibile eseguire l'accesso automatico dell'utente
        if($request->getQuery('error') !== NULL){
            $gp_error = $request->getQuery('error');

            $google->logger->notice('Account Access Gp - Error: '. $gp_error);

            $this->messages->addMessage('danger', $this->translate->translate('For signup with Google+ you must allow the application connect with your profile.').' <br> '.$this->translate->translate('You can still register on HealthEye using the form below.'));
            $this->redirect('/account/signup');
        }
        
        if(!$google->gpAccesstoken('NO')){
            $this->redirect('/account/connectgp');
        }
    }
    
    public function revokegpAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        
        $google = new DatabaseObject_UserGoogle($this->db);
        $google->load($this->identity->user_id);
        
        if($google->profile->gp_connected != 1){
            $this->redirect('/account');
        }
        
        $uri = 'https://accounts.google.com/o/oauth2/revoke?token='.$google->profile->gp_access_token;
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $profile = json_decode(curl_exec($ch));

        $google->logger->notice('Account Access Gp - Revoked: '. $gp_error);

        $this->messages->addMessage('success', $this->translate->translate('You successful disconnect from Google+ you can still use HealthEye, we send you email with credentials.'));
        
        
        unset($google->profile->gp_connected);
        unset($google->profile->gp_autopost);
        unset($google->profile->gp_access_token);
        unset($google->profile->gp_access_token_expire);
        unset($google->profile->gp_id_conn);
        
        $google->save();
        
        $this->redirect('/account');
    }
    
    public function mustlogoutAction()
    {
        $request = $this->getRequest();
        
        $redirect = $request->getParam('url');
        
        $this->view->logout_url = '/account/logout?redirect='.$redirect;
        
        $this->view->title = 'Logout';
        $this->view->description = $this->translate->translate('To continue, you must log out');
    }
}
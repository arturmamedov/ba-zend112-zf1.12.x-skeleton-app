<?php /* Smarty version Smarty-3.1.12, created on 2014-05-17 01:35:26
         compiled from "D:\ServerPath\work\socialtrip\www\private\Templates\account\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:210535376b09ceadd03-85460624%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'daede59fa79e6c9ff782c035fd8862dc4d12fe99' => 
    array (
      0 => 'D:\\ServerPath\\work\\socialtrip\\www\\private\\Templates\\account\\login.tpl',
      1 => 1400290525,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210535376b09ceadd03-85460624',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5376b09d31ce00_90488375',
  'variables' => 
  array (
    'tsn' => 0,
    'errors' => 0,
    'user_name_mail' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5376b09d31ce00_90488375')) {function content_5376b09d31ce00_90488375($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('layout/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>
<body id="home">
    <?php echo $_smarty_tpl->getSubTemplate ('layout/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="container">
	<div class="row">
		<div class="col-md-5 center-block">
			<a href="/" title="Torna alla pagina principale del sito <?php echo $_smarty_tpl->tpl_vars['tsn']->value;?>
" class="center-block col-sm-2">
				<img src="/public/img/site/logo.jpg" class="img-responsive"/>
			</a>
			<h1 class="text-center">Login</h1>
			<p class="text-center">Esegui il login per accedere</p>
				
			<?php if (count($_smarty_tpl->tpl_vars['errors']->value)>0){?><?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['errors']->value['general']), 0);?>
<?php }?>
				
			<form action="/account/login" method="POST" class="form-horizontal" role="form">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="username">Username</label>
					<div class="col-sm-9">
						<input type="text" id="username" class="form-control" name="user_name_mail" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['user_name_mail']->value)===null||$tmp==='' ? '' : $tmp);?>
" autofocus="autofocus" placeholder="username / @email" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3  control-label" for="password">Password</label>
					<div class="col-sm-9">
						<input type="password" id="password"  class="form-control" name="password"  placeholder="password"/><br />
					</div>	
				</div>
				
				<div class="form-group">
					<div class="text-center">
						<input type="submit" class="btn btn-primary btn-lg" value="Log in">
					</div>
				</div>
			</form>

			<a href="/account/fetch" class="pull-right text-muted">Recupero password</a>
			<a href="/account/signup" class="pull-left text-info">Registrazione</a>
		</div> <!-- .cont -->
	</div> <!-- row -->
</div> <!-- container -->  
        
<?php echo $_smarty_tpl->getSubTemplate ('layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ('layout/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</body>
</html><?php }} ?>
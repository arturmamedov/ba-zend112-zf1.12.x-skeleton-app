<?php /* Smarty version Smarty-3.1.12, created on 2014-05-17 01:25:02
         compiled from "D:\ServerPath\work\socialtrip\www\private\Templates\account\signup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:62755376ba6e5cc601-37440842%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd15afbd0cb5fe49e235b53a93964de9a2cc9a354' => 
    array (
      0 => 'D:\\ServerPath\\work\\socialtrip\\www\\private\\Templates\\account\\signup.tpl',
      1 => 1400287294,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '62755376ba6e5cc601-37440842',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'tsn' => 0,
    'fp' => 0,
    'recaptcha' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5376ba6e829d88_69990007',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5376ba6e829d88_69990007')) {function content_5376ba6e829d88_69990007($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('layout/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script type="text/javascript">
    var RecaptchaOptions = {theme:'clean'};
</script>
</head>

<body id="signup">
	<?php echo $_smarty_tpl->getSubTemplate ('layout/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="container">
	
	<?php echo $_smarty_tpl->getSubTemplate ('lib/pop_message.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


        <div class="page-header text-center">
            <h1 class=""><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
<small><?php echo $_smarty_tpl->tpl_vars['tsn']->value;?>
</small></h1>
        </div>
        
<div class="row">
    <form class="col-md-8 center-block form-horizontal" method="POST" action="/account/signup" id="regForm" autocomplete="on" role="form">
        
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="firstName">Nome</label>
            <div class="col-md-8">
                <input class="form-control" id="firstName" autofocus="autofocus" name="first_name" required="required" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fp']->value->first_name, ENT_QUOTES, 'UTF-8', true);?>
" type="text" placeholder="Nome (persona/agenzia)" />
                <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('first_name')), 0);?>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="last_name">Cognome</label>
            <div class="col-md-8">
                <input class="form-control" id="lastName" name="last_name" required="required" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fp']->value->last_name, ENT_QUOTES, 'UTF-8', true);?>
" type="text" placeholder="Cognome (persona)" />
                <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('last_name')), 0);?>

            </div>
        </div>
            
        <div class="form-group">
            <label class="col-md-4 control-label" for="formEmail">Mail</label>
            <div class="col-md-8">
                <input type="email" class="form-control" id="formEmail" name="user_mail" required="required" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fp']->value->user_mail, ENT_QUOTES, 'UTF-8', true);?>
" placeholder="Mail per confermare identità" />
                <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('user_mail')), 0);?>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-8">
                <input class="form-control" id="password" name="password" required="required" type="password" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fp']->value->password, ENT_QUOTES, 'UTF-8', true);?>
" placeholder="Password">
                <span class="help-block">La password deve contenere 6 o piú caratteri, AVERE un numero, un simbolo speciale o una maiuscola</span>
                <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('password')), 0);?>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="formPasswordConfirm">Conferma password</label>
            <div class="col-md-8">
                <input class="form-control" id="formPasswordConfirm" name="password_confirm" required="required" type="password" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fp']->value->password_confirm, ENT_QUOTES, 'UTF-8', true);?>
" placeholder="Ripeti la password" />
                <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('password_confirm')), 0);?>

            </div>
        </div>
        
        <div class="form-group text-center">
            <label class="col-md-4 control-label">Codice di sicurezza</label>
            <div class="col-md-8"><?php echo $_smarty_tpl->tpl_vars['recaptcha']->value;?>

                <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('captcha')), 0);?>

            </div>
        </div>
        
        <div class="col-md-10 center-block text-center">
            <input class="btn btn-lg btn-success" type="submit" value="Registrati"/>
            <p><small>Cliccando REGISTRATI si accettano i <a href="/index/terms" target="_blank"> Termini e Condizioni d'Uso</a></small></p>
        </div>
        
        
        <a class="pull-right" href="/account/login" title="Hai già un account? Esegui il login :)">Login</a>
    </form>
</div><!-- row -->  

	
</div><!-- container -->
<?php echo $_smarty_tpl->getSubTemplate ('layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ('layout/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('hide'=>true), 0);?>


</body>
</html><?php }} ?>
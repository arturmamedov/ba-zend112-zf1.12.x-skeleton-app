<?php /* Smarty version Smarty-3.1.12, created on 2014-05-17 00:52:34
         compiled from "D:\ServerPath\work\socialtrip\www\private\Templates\index\about.tpl" */ ?>
<?php /*%%SmartyHeaderCode:183435376b09baa6904-04629581%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7f1fb297b89fad4e4f69663a387fbe3b63e933af' => 
    array (
      0 => 'D:\\ServerPath\\work\\socialtrip\\www\\private\\Templates\\index\\about.tpl',
      1 => 1400287951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183435376b09baa6904-04629581',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5376b09bc35001_45635702',
  'variables' => 
  array (
    'tsn' => 0,
    'fp' => 0,
    'authenticated' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5376b09bc35001_45635702')) {function content_5376b09bc35001_45635702($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('layout/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</head>

<body id="about">
	<?php echo $_smarty_tpl->getSubTemplate ('layout/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="container" id="container">
	
	<?php echo $_smarty_tpl->getSubTemplate ('lib/pop_message.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

	
	

	<div class="page-header text-center">
		<h1>Informazioni di contatto <small><?php echo $_smarty_tpl->tpl_vars['tsn']->value;?>
</small></h1>
	</div>
	
	<div class="row">
		<figure class="col-md-3">
			<img src="/public/img/site/logo.jpg" alt="Logo <?php echo $_smarty_tpl->tpl_vars['tsn']->value;?>
" class="img-responsive">
		</figure>
		
		<div class="col-md-4">
			<address>
				<strong><?php echo $_smarty_tpl->tpl_vars['tsn']->value;?>
</strong>,<br>
				P.iva: .......<br>
				<label>emailexample@gmail.com</label><br>
				<abbr title="FAX">fax:</abbr> (+39) 123 123 123
			</address>
		</div>
		
		<div class="col-md-4">
			<p>
				a e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenev
			</p>
			<p>
				a e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenev
			</p>
			<p>
				a e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenev
			</p>
		</div>
	</div>
	
	<div class="row">
		<?php if (isset($_smarty_tpl->tpl_vars['fp']->value->text)&&!$_smarty_tpl->tpl_vars['fp']->value->hasError()){?>
			<h2>Ti risponderemo il più presto possibile</h2>
			<p><?php echo $_smarty_tpl->tpl_vars['fp']->value->name;?>
 ci hai scritto:</p> 
			<p><?php echo $_smarty_tpl->tpl_vars['fp']->value->text;?>
</p>
			<p>Ti ricontatteremo su <?php echo $_smarty_tpl->tpl_vars['fp']->value->mail;?>
</p>
			
		<?php }else{ ?>
		<div class="col-md-6">
			<h2>Contattaci</h2>
			
			
			<form method="POST" action="/index/about">
				<div class="form-group">
					<label for="mail">Vostra e-Mail</label>
					<?php if ($_smarty_tpl->tpl_vars['authenticated']->value){?>
						<p class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['fp']->value->mail;?>
</p>
					<?php }else{ ?>
						<input type="text" id="mail" name='mail' class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['fp']->value->mail;?>
" required placeholder="email@example.com" required>
					<?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('mail')), 0);?>

				</div>
				
				<div class="form-group">
					<label for="name">Nome</label>
					<?php if ($_smarty_tpl->tpl_vars['authenticated']->value){?>
						<p class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['fp']->value->name;?>
</p>
					<?php }else{ ?>
						<input type="text" name='name' id="name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['fp']->value->name;?>
" required placeholder="Emanuele" required>
					<?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('name')), 0);?>

				</div>
				
				<div class="form-group">
					<label for="text">Cosa volete comunicarci</label>
					<textarea id="text" name='text' class="form-control" required><?php echo $_smarty_tpl->tpl_vars['fp']->value->text;?>
</textarea>
                    <?php echo $_smarty_tpl->getSubTemplate ('lib/error.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('error'=>$_smarty_tpl->tpl_vars['fp']->value->getError('text')), 0);?>

				</div>
				
				<div class="row center-text">
					<input type="submit" class="btn btn-info btn-lg" value="Invia" title="Iviaci il tuo messaggio"> 
				</div>
			</form>
		</div>
		<?php }?>
		
		
	</div>


	<?php echo $_smarty_tpl->getSubTemplate ('layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>

<?php echo $_smarty_tpl->getSubTemplate ('layout/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('hide'=>true), 0);?>


</body>
</html><?php }} ?>
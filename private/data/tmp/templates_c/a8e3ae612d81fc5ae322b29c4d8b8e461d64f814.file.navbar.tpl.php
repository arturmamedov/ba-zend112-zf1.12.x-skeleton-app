<?php /* Smarty version Smarty-3.1.12, created on 2014-05-17 01:39:40
         compiled from "D:\ServerPath\work\socialtrip\www\private\Templates\layout\navbar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:257335376a8b4520800-80784949%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a8e3ae612d81fc5ae322b29c4d8b8e461d64f814' => 
    array (
      0 => 'D:\\ServerPath\\work\\socialtrip\\www\\private\\Templates\\layout\\navbar.tpl',
      1 => 1400290777,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '257335376a8b4520800-80784949',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5376a8b459d806_08704824',
  'variables' => 
  array (
    'tsn' => 0,
    'authenticated' => 0,
    'identity' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5376a8b459d806_08704824')) {function content_5376a8b459d806_08704824($_smarty_tpl) {?><div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header pull-left col-sm-2">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-logo col-sm-1" href="/">
				<img src="/public/img/site/logo.jpg" alt="Logo <?php echo $_smarty_tpl->tpl_vars['tsn']->value;?>
" class="img-responsive">
			</a>
		</div>
		
		<div class="navbar-collapse collapse pull-left">
			<ul class="nav navbar-nav">
				<li><a href="/">Home</a></li>
			
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="/search/property?sell_type=V&estate_type=1">1</a></li>
							<li><a href="/search/property?sell_type=V&estate_type=2">2</a></li>
							<li><a href="/search/property?sell_type=V&estate_type= 3">3</a></li>
							<li><a href="/search/property?sell_type=V&estate_type=4">4</a></li>
						</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Drop_down <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="/search/property?sell_type=A&estate_type=Residenziale">Residenziale</a></li>
							<li><a href="/search/property?sell_type=A&estate_type=Commerciale">Commerciale</a></li>
							<li><a href="/search/property?sell_type=A&estate_type=Terreno">Terreno</a></li>
						</ul>
				</li>
				
				<li><a href="/support" title="Supporto per le funzionalità del sito e altro">F.A.Q.</a></li>
				<li><a href="/index/about" title="Informazioni">Info</a></li>
                <?php if ($_smarty_tpl->tpl_vars['authenticated']->value&&$_smarty_tpl->tpl_vars['identity']->value->user_type=='master'){?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Amministrazione <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="/property/manage">Annunci Immobiliari</a></li>
							<li><a href="/user/manage">Utenti</a></li>
						</ul>
                    </li>
                <?php }?>
			</ul>
			
			
			<?php if ($_smarty_tpl->tpl_vars['authenticated']->value){?>
				<ul class="nav navbar-nav navbar-right">
					<li class="img-profile">
						<a href="/u/<?php echo $_smarty_tpl->tpl_vars['identity']->value->identity;?>
">
							<img src="/public/files/user/image/thumbnail/<?php echo (($tmp = @$_smarty_tpl->tpl_vars['identity']->value->poster)===null||$tmp==='' ? 'default.png' : $tmp);?>
" class="img-thumbnail">
						</a>
					</li>
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->tpl_vars['identity']->value->first_name;?>
 <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/u/<?php echo $_smarty_tpl->tpl_vars['identity']->value->identity;?>
">Profilo</b></a></li>
                                <li><a href="/dashboard">Gestionale</a></li>
								<li><a href="/account">Impostazioni</a></li>
								<li><a href="/account/logout">Logout</a></li>
							</ul>
					</li>
				</ul>
			<?php }else{ ?>	
				<div class="nav navbar-nav navbar-right col-md-5 visible-lg" style="margin-top: 20px;">
					<form action="javascript:;" type="POST" class="loginForm form-inline">
						<div class="form-group col-md-3">
							<label class="sr-only" for="tl_mail">Email o Username</label>
							<input type="text" value="" id="tl_mail" placeholder="username/@mail" class="loginMail form-control" required>
						</div>
						<div class="form-group col-md-3">
							<label class="sr-only" for="tl_mail">Password</label>
							<input type="password" value="" placeholder="password" class="loginPass form-control" required>
						</div>
						<input type="submit" value="Login" style="font-weight: bold;" class="btn btn-success" data-loading-text="Login...">
						<a href="/account/signup" style="font-weight: bold;" class="btn btn-info">Registrati</a>
					</form>
				</div>
			<?php }?>
		</div>
	</div>
</div><?php }} ?>
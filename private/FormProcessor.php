<?php
abstract class FormProcessor
{
    protected $_errors = array();
    protected $_vals = array();
    private $_sanitizeChain = null;
    public $translate;
    
    /**
     * Allowed tags in text [es: in item description text]
     * no allowed tag at 13.20.2013 because now we do not accept html  
     * 
     * @var type 
     */
    static $tags = array(
        //'a'      => array('href', 'title'),
        //'img'    => array('src', 'alt'),
        //'b'      => array(),
        //'strong' => array(),
        //'em'     => array(),
        //'i'      => array(),
        //'ul'     => array(),
        //'li'     => array(),
        //'ol'     => array(),
        //'p'      => array(),
        //'br'     => array()
     );

    public function __construct()
    {
        // ... :)
    }

    abstract function process(Zend_Controller_Request_Abstract $request);

    /**
     * Sanitize the passed value, stripTags, StringTrim
     * and eliminate a new line
     * 
     * @param string $value
     * 
     * @return string
     */
    public function sanitize($value)
    {
		$value = trim($value);
        if (!$this->_sanitizeChain instanceof Zend_Filter) {
            $this->_sanitizeChain = new Zend_Filter();
            $this->_sanitizeChain->addFilter(new Zend_Filter_StringTrim())
                                 ->addFilter(new Zend_Filter_StripTags());
        }

        // filter out any line feeds / carriage returns
        $ret = preg_replace('/[\r\n]+/', ' ', $value);

        // filter using the above chain
        return $this->_sanitizeChain->filter($ret);
    }

    public function addError($key, $val)
    {
        if (array_key_exists($key, $this->_errors)) {
            if (!is_array($this->_errors[$key]))
                $this->_errors[$key] = array($this->_errors[$key]);

            $this->_errors[$key][] = $val;
        }
        else
            $this->_errors[$key] = $val;
    }

    public function getError($key)
    {
        if ($this->hasError($key))
            return $this->_errors[$key];

        return null;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function hasError($key = null)
    {
        if (strlen($key) == 0)
            return count($this->_errors) > 0;

        return array_key_exists($key, $this->_errors);
    }

    public function __set($name, $value)
    {
        $this->_vals[$name] = $value;
    }

    public function __get($name)
    {
        return array_key_exists($name, $this->_vals) ? $this->_vals[$name] : null;
    }

    /**
     * Extract a video token from passed YouTube or Vimeo URL
     * 
     * @param string $video_url
     * 
     * @return string
     */
    public function get_videoToken($video_url){
        // cut vimeo/youtube url and extract only a token for embed

        if(strlen(strstr($video_url, 'youtu')) > 0){
            $query_string = array();
            parse_str(parse_url($video_url, PHP_URL_QUERY), $query_string);
            if(isset($query_string["v"])){
                $token = $query_string["v"];
            } else
                $token = substr($video_url, strrpos($video_url, '/') + 1);                
        }elseif(strlen(strstr($video_url, 'vimeo')) > 0){
            $result = preg_match('/(\d+)/', $video_url, $matches);
            $token = $matches[0];
        } else
            return false;

        if(strlen($token) > 3)
            return $token;
        else
            return false;
    }
    
    
    /**
     * Extract and return a id video token from passed YouTube or Vimeo URL
     * 
     * @param string $video_url
     * 
     * @return string id, numeric for vimeo, alphadecimal for youtube
     */
    static public function vitubeToken($video_url){
        // cut vimeo/youtube url and extract only a token for embed

        if(strlen(strstr($video_url, 'youtu')) > 0){
            $query_string = array();
            parse_str(parse_url($video_url, PHP_URL_QUERY), $query_string);
            if(isset($query_string["v"])){
                $token = $query_string["v"];
            } else {
                if(($strs = strrpos($video_url, '/')) > 3)
                    $token = substr($video_url, $strs + 1);
                else
                    return false;
            }
        } elseif(strlen(strstr($video_url, 'vimeo')) > 0) {
            $result = preg_match('/(\d+)$/', $video_url, $matches);
            $token = $matches[0];
        } else
            return false;

        if(strlen($token) > 3)
            return $token;
        else
            return false;
    }
    
    static public function cleanHtml($html, $tags = null){
        if($tags === null)
            $tags = self::$tags;
        elseif($tags === 0)
            $tags = array();
        
        $chain = new Zend_Filter();
        $chain->addFilter(new Zend_Filter_StripTags($tags));
        $chain->addFilter(new Zend_Filter_StringTrim());

        $html = $chain->filter($html);

        $tmp = $html;
        while(1) {
            // Try and replace an occurrence of javascript:
            $html = preg_replace('/(<[^>]*)javascript:([^>]*>)/i', '$1$2', $html);

            // If nothing changed this iteration then break the loop
            if ($html == $tmp)
                break;

            $tmp = $html;
        }

        return $html;
    }
}
?>
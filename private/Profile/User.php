<?php
    class Profile_User extends Profile
    {
        public function __construct($db, $id = null)
        {
            parent::__construct($db, DB_PREFIX.'users_profile');

            if($id > 0)
                $this->setUserId($id);
        }

        public function setUserId($id)
        {
            $filters = array('user_id' => (int) $id);
            $this->_filters = $filters;
        }
    }
?>
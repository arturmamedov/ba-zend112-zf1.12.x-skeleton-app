<?php
class Profile_Notification extends Profile
{
    public function __construct($db, $id = null)
    {
        parent::__construct($db, DB_PREFIX.'notifications_profile');

        if ($id > 0)
            $this->setNotificationId($id);
    }

    public function setNotificationId($id)
    {
        $filters = array('id' => (int) $id);
        $this->_filters = $filters;
    }
}
?>
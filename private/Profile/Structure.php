<?php
class Profile_Structure extends Profile
{
    public function __construct($db, $id = null){
        parent::__construct($db, DB_PREFIX.'structure_profile');

        if($id > 0){
			$this->setProfileId($id);
        }
    }

    public function setProfileId($id){
        $filters = array('id' => (int) $id);
        $this->_filters = $filters;
    }
}
?>


<?php

class Templater extends Zend_View_Abstract
{
    protected $_path;
    protected $_engine;

    public function __construct()
    {
        parent::__construct();

        $config = Zend_Registry::get('config');

        require_once(ROOT.DS.'private'.DS.'libraries'.DS.'Smarty'.DS.'Smarty.class.php');

        $this->_engine = new Smarty();
        $this->_engine->template_dir = ROOT.DS.'private'.DS.'Templates';
        $this->_engine->compile_dir = ROOT.DS.'private'.DS.'data'.DS.'tmp'.DS.'templates_c';
        $this->_engine->cache_dir = ROOT.DS.'private'.DS.'data'.DS.'cache';
        $this->_engine->plugins_dir = array(ROOT.DS.'private'.DS.'libraries'.DS.'Smarty'.DS.'plugins','plugins');
        //$this->_engine->config_dir = (ROOT.DS.'private'.DS.'libraries'.DS.'Smarty'.DS.'configs');
    }

    public function getEngine()
    {
        return $this->_engine;
    }

    public function __set($key, $val)
    {
        $this->_engine->assign($key, $val);
    }

    public function __get($key)
    {
        return $this->_engine->get_template_vars($key);
    }

    public function __isset($key)
    {
        return $this->_engine->get_template_vars($key) !== null;
    }

    public function __unset($key)
    {
        $this->_engine->clear_assign($key);
    }

    public function assign($spec, $value = null)
    {
        if (is_array($spec)) {
            $this->_engine->assign($spec);
            return;
        }

        $this->_engine->assign($spec, $value);
    }

    public function clearVars()
    {
        $this->_engine->clear_all_assign();
    }

    public function render($name)
    {
        return $this->_engine->fetch(strtolower($name));
    }

    public function _run()
    { }
}
?>